export const toEnglishDigit = replaceString => {
	var find = ['۰', '۱', '۲', '۳', '۴', '۵', '۶', '۷', '۸', '۹']
	var replace = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9']
	var regex
	for (var i = 0; i < find.length; i++) {
		regex = new RegExp(find[i], 'g')
		replaceString = replaceString.replace(regex, replace[i])
	}
	return replaceString
}

export const separateNumbers = number => {
	return (number && Number(number).toLocaleString('en-US')) || ''
}

export const fromSeparetedNumber = number => {
	let newValue = number.replace(/,/g, '')
	newValue = toEnglishDigit(newValue)
	return newValue
}

export const emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/

export const phoneRegex = /^0[0-9]{3}[0-9]{7}$/

export const postalCodeRegex = /^\d{10}$/

export const mobileRegx = /^(\+98|0)?9\d{9}$/

export const vmsNationalCode = input => {
	if (
		!/^\d{10}$/.test(input) ||
		input === '0000000000' ||
		input === '1111111111' ||
		input === '2222222222' ||
		input === '3333333333' ||
		input === '4444444444' ||
		input === '5555555555' ||
		input === '6666666666' ||
		input === '7777777777' ||
		input === '8888888888' ||
		input === '9999999999'
	)
		return false
	var check = parseInt(input[9])
	var sum = 0
	var i
	for (i = 0; i < 9; ++i) {
		sum += parseInt(input[i]) * (10 - i)
	}
	sum %= 11
	return (sum < 2 && check === sum) || (sum >= 2 && check + sum === 11)
}
