import { useState } from 'react'
import classes from './EnquiryLayout.module.scss'
import {
	Button,
	Collapse,
	CircularProgress,
	makeStyles,
	Dialog,
	DialogTitle,
	DialogContent,
	IconButton
} from '@material-ui/core'
import Badge from '../../components/UI/Badge/Badge'
import OrderSteps from '../../components/order-steps/OrderSteps'
import InsuranceDetails from '../../components/insurance-details/InsuranceDetails'
import OrderDialog from '../../components/order-dialog/OrderDialog'
import { useRouter } from 'next/router'
import { useDispatch, useSelector } from 'react-redux'
import { setProduct } from '../../store/actions/productActions'
import DiscountCode from '../../components/discount-code/DiscountCode'
import CloseIcon from '@material-ui/icons/Close'

const useStyle = makeStyles({
	root: {
		'& .MuiDialog-paperWidthSm': {
			width: '80%'
		}
	}
})

const EnquiryLayout = ({
	title,
	children,
	isValid,
	onSubmit,
	badges,
	filterWrapper,
	onReturn,
	data,
	loading,
	properties,
	orderPath,
	renderDetails,
	getStickyLabel,
	infoTitle,
	insuranceParams = []
}) => {
	const state = useSelector(state => state.product.discountCode)
	const styles = useStyle()

	const [collapse, setCollapse] = React.useState(true)
	const router = useRouter()
	const dispatch = useDispatch()

	const onCollapse = () => {
		setCollapse(c => !c)
	}

	const [sort, setSort] = useState({ param: '', highest: false })

	const onChangeSort = param => {
		if (sort.param === param)
			setSort(sort => ({ ...sort, highest: !sort.highest }))
		else setSort({ param, highest: false })
	}

	const comparator = (a, b) => {
		const order = sort.highest ? 1 : -1
		if (sort.param === 'price') return (a.price - b.price) * order
		else return (a.insurance[sort.param] - b.insurance[sort.param]) * order
	}

	const renderElements = () => {
		if (data) {
			if (!data.length)
				return (
					<div className="mt-4 text-center">
						بیمه ای متناسب با اطلاعات داده شده وجود ندارد.
					</div>
				)
			const newData = [...data]
			newData.sort(comparator)
			return newData.map(item => (
				<InsuranceDetails
					price={item.price}
					key={item.id}
					insurance={item.insurance}
					onClick={() => onOrder(item)}
					details={renderDetails && renderDetails(item)}
					sticky={getStickyLabel && getStickyLabel(item)}
					hasInstallments
				/>
			))
		}
	}

	const onOrder = item => {
		const values = [
			{
				header: 'مشخصات بیمه',
				body: [
					{ key: 'نوع بیمه', value: title },
					{ key: 'بیمه انتخابی', value: item.insurance.title },
					{
						key: 'قیمت',
						value:
							item.price &&
							Number(+item.price.toFixed()).toLocaleString() +
								' تومان'
					},
					...insuranceParams
				]
			},
			{ header: infoTitle, body: properties(item) }
		]
		if (state)
			values[0].body.push(
				{
					key: 'تخفیف',
					value: Number(state.discount).toLocaleString()
				},
				{
					key: 'قیمت نهایی',
					value:
						(item.price &&
						Number(+item.price.toFixed()) -
							Number(state.discount)).toLocaleString()
							
				}
			)
		setOrderSummery({ show: true, values, product: item })
	}

	const [orderSummery, setOrderSummery] = useState({
		show: false,
		values: [],
		product: null
	})

	const onOrderInsurance = paymentMethod => {
		const summary = [...orderSummery.values]
		summary.shift()
		summary.push({
			header: 'جزئیات پرداخت',
			body: [
				{
					key: 'قیمت بیمه نامه',
					value:
						orderSummery.product.price &&
						Number(
							+orderSummery.product.price.toFixed()
						).toLocaleString() + ' تومان'
				},
				{
					key: 'جریمه دیرکرد',
					value:
						orderSummery.product.price &&
						Number(
							+orderSummery.product.price.toFixed()
						).toLocaleString() + ' تومان'
				}
			]
		})
		if (paymentMethod !== 'cash')
			summary[1].body.push({
				key: 'پرداختی اول',
				value:
					orderSummery.product.price &&
					Number(
						+orderSummery.product.price.toFixed()
					).toLocaleString() + ' تومان'
			})
		const params = insuranceParams.map(param => ({
			name: param.name,
			value: param.data
		}))
		dispatch(
			setProduct({
				data: orderSummery.product,
				insuranceType: title,
				summary,
				paymentMethod,
				params
			})
		)
		setOrderFormLoading(true)
		router.push(orderPath).finally(() => {
			setOrderFormLoading(false)
		})
	}

	const [orderFormLoading, setOrderFormLoading] = useState(false)

	const [open, setOpen] = React.useState(false)

	const handleClickOpen = () => {
		setOpen(true)
	}
	const handleClose = () => {
		setOpen(false)
	}

	return (
		<div>
			<div className="container my-2 px-md-5">
				<OrderSteps step={0} />
			</div>
			<OrderDialog
				open={orderSummery.show}
				setOpen={() =>
					setOrderSummery({ show: false, values: [], product: null })
				}
				values={orderSummery.values}
				onOrder={onOrderInsurance}
				hasInstallments={true}
				loading={orderFormLoading}
			/>
			<div className="container container-fluid mb-5">
				<Collapse in={!collapse}>
					<div className={classes.enquiryUpdate}>
						<div className={classes.enquiryUpdate__header}>
							<div className={classes.enquiryUpdate__title}>
								تغییر اطلاعات
							</div>
						</div>
						<div className={classes.enquiryUpdate__content}>
							<div className={classes.enquiryUpdate__wrapper}>
								{children}
								<div className="row mt-5 px-5">
									<div className="col-12 col-md-6 mb-3 mb-md-0">
										<Button
											color="primary"
											variant="contained"
											fullWidth
											type="submit"
											disabled={!isValid}
											onClick={() => {
												onSubmit()
												setCollapse(true)
											}}
										>
											تایید
										</Button>
									</div>
									<div className="col-12 col-md-6 mb-3 mb-md-0">
										<Button
											onClick={() => {
												onReturn()
												setCollapse(true)
											}}
											color="primary"
											variant="outlined"
											fullWidth
											type="button"
										>
											بازگشت
										</Button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</Collapse>
				<Collapse in={collapse}>
					<div className={classes.contentCard}>
						<div className={classes.contentCard__wrapper}>
							<div className={classes.contentCard__header}>
								<div className="d-flex align-items-center">
									<h1
										className={classes.contentCard__title}
										style={{
											borderLeft: badges.length
												? '1px solid #b3b8bc'
												: '0'
										}}
									>
										{title}
									</h1>
									<div
										className={
											classes.contentCard__badges +
											' d-none d-md-flex'
										}
									>
										{badges.map(badge => (
											<Badge key={badge}>{badge}</Badge>
										))}
									</div>
								</div>
								<Button
									color="secondary"
									variant="contained"
									onClick={onCollapse}
								>
									تغییر اطلاعات
								</Button>
							</div>
							<div className="row">
								<div className="col-xl-3 d-xl-block sidebar">
									<div className={classes.filterEnquiry}>
										<div
											className={
												'd-none d-xl-block ' +
												classes.cover
											}
										>
											{filterWrapper}
										</div>
										<div
											className={
												'd-xl-none ' +
												classes.cover__mobile
											}
										>
											{filterWrapper}
										</div>
									</div>
									<div className="mt-4 d-none d-xl-block">
										<DiscountCode />
									</div>
								</div>
								<div className="col-12 col-xl-9 pr-2 pr-md-3 pr-xl-0 pl-2 pl-md-3">
									<div
										className={
											classes.sort + ' d-none d-md-block'
										}
									>
										<div className={classes.sort__body}>
											<div className={classes.sort__item}>
												<span>شرکت بیمه</span>
											</div>
											{sortItems.map(sortItem => (
												<div
													key={sortItem.value}
													className={
														classes.sort__item
													}
												>
													<span
														className={
															classes.sort__button
														}
														onClick={() =>
															onChangeSort(
																sortItem.value
															)
														}
													>
														{sortItem.value ===
															sort.param && (
															<img
																src="/imgs/svg/sort-ic.svg"
																alt="sort_icon"
																className={[
																	classes.sort__icon,
																	sort.highest
																		? classes.sort__ascend
																		: ''
																].join(' ')}
															/>
														)}
														{sortItem.name}
													</span>
												</div>
											))}
											<div className={classes.sort__item}>
												<span>خرید</span>
											</div>
										</div>
									</div>
									{loading ? (
										<div className="text-center mt-4">
											<CircularProgress
												color="primary"
												size={50}
											/>
										</div>
									) : (
										renderElements()
									)}
								</div>
							</div>
						</div>
					</div>
				</Collapse>
			</div>
			{collapse && (
				<div className={classes.bottomNav + ' d-xl-none'}>
					<Button onClick={onCollapse}>تغییر اطلاعات</Button>
					<Button onClick={handleClickOpen}>کد تخفیف</Button>
				</div>
			)}
			<div className="d-xl-none">
				<Dialog
					onClose={handleClose}
					open={open}
					className={styles.root}
				>
					<DialogTitle
						style={{ textAlign: 'left' }}
						onClose={handleClose}
					>
						<IconButton
							aria-label="close"
							className={classes.closeButton}
							onClick={handleClose}
						>
							<CloseIcon />
						</IconButton>
					</DialogTitle>
					<DialogContent>
						<div className="mb-4">
							<DiscountCode />
						</div>
					</DialogContent>
				</Dialog>
			</div>
		</div>
	)
}

const sortItems = [
	{ name: 'قیمت', value: 'price' },
	{ name: 'سرعت پاسخگویی', value: 'delay' },
	{ name: 'توانگری', value: 'power' },
	{ name: 'رضایت', value: 'customer_rate' },
	{ name: 'تعداد شعب', value: 'branches' }
]

export default EnquiryLayout
