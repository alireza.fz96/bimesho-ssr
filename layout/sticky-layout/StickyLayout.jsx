import React from 'react'
import Backdrop from '@material-ui/core/Backdrop'
import { makeStyles } from '@material-ui/core/styles'
import classes from './StickyLayout.module.scss'

const useStyles = makeStyles(theme => ({
	backdrop: {
		zIndex: theme.zIndex.drawer + 1
	}
}))

export default function StickyLayout({ title, children }) {
	const styles = useStyles()
	const [open, setOpen] = React.useState(false)
	const handleClose = () => {
		setOpen(false)
	}

	const handleToggle = () => {
		setOpen(!open)
	}

	return (
		<>
			<div className={classes.stick} onClick={handleToggle}>
				{title}
			</div>
			<Backdrop
				className={styles.backdrop}
				open={open}
				onClick={handleClose}
			></Backdrop>
			{open && <div className={classes.cover}>{children}</div>}
		</>
	)
}
