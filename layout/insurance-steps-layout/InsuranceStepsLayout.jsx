import React from 'react'
import classes from './InsuranceStepsLayout.module.scss'
import { Button, makeStyles } from '@material-ui/core'
import Badge from '../../components/UI/Badge/Badge'

const useStyle = makeStyles({
	btn: {
		color: 'white'
	}
})

const InsuranceStepsLayout = ({
	children,
	title,
	sectionTitle,
	header,
	img,
	next,
	prev,
	canReturn,
	isValid,
	badges = []
}) => {
	const styles = useStyle()
	return (
		<div className="container-lg container-fluid">
			<div className={classes.landing}>
				<div className={classes.landing__header}>
					<h1 className={classes.landing__title}>{title}</h1>
					<div className={classes.landing__badges}>
						<div className={classes.badges}>
							{badges.map(badge => (
								<Badge key={badge}>{badge}</Badge>
							))}
						</div>
					</div>
				</div>
				<div className={classes.landing__content}>
					<div className={classes.content__header}>
						<div className={classes.header__border}></div>
						<h2 className={classes.content__title}>
							{sectionTitle}
						</h2>
						<div className={classes.header__border}></div>
					</div>
					<div className={classes.content__main}>
						<div className={classes.content__wrapper}>
							<div className={classes.content__nav}>
								<div className={classes.nav__header}>
									<span className={classes.nav__title}>
										{header}
									</span>
								</div>
								<div className={classes.nav__controller}>
									<Button
										className={
											classes.nav__btn +
											' ' +
											classes.nav__prev +
											' ' +
											styles.btn
										}
										onClick={prev}
										disabled={!canReturn}
									>
										{canReturn ? 'مرحله قبل' : '...'}
									</Button>
									<div className={classes.nav__line}></div>
									<Button
										className={
											classes.nav__btn +
											' ' +
											classes.nav__next +
											' ' +
											styles.btn
										}
										onClick={next}
										disabled={!isValid}
									>
										مرحله بعد
									</Button>
								</div>
								<div className={classes.nav__content}>
									<img
										src={img}
										alt=""
										className={classes.nav__icon}
									/>
								</div>
							</div>
							<div className={classes.content__steps}>
								<div dir="ltr">{children}</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	)
}

export default InsuranceStepsLayout
