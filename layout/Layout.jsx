import Toolbar from '../components/toolbar/Toolbar.jsx'
import Footer from '../components/footer/Footer.jsx'
import { useRouter } from 'next/router'

const Layout = ({ children }) => {
	const router = useRouter()

	return (
		<div
			style={{
				backgroundColor:
					router.pathname === '/' ? 'unset' : 'rgb(250, 250, 250)',
				paddingBottom:
					router.pathname.split('-').pop() === 'enquiry' ? 20 : 0,
				minHeight: '100vh'
			}}
		>
			<header>
				<Toolbar />
			</header>
			<main>{children}</main>
			{router.pathname.split('-').pop() !== 'enquiry' && (
				<footer>
					<Footer />
				</footer>
			)}
		</div>
	)
}

export default Layout
