import { useState, useEffect } from 'react'
import EnquiryLayout from '../../layout/enquiry-layout/EnquiryLayout'
import { useSelector, useDispatch } from 'react-redux'
import Select from '../../components/UI/Select/Select'
import { setForm as travelSetForm } from '../../store/actions/healthInsuranceActions'
import Actions from '../../components/actions/Actions'
import { fetchInsurances, makeRequestBody } from '../../api/healthInsurance'
import classes from '../../styles/HealthEnquiry.module.scss'
import withEnquiryGuard from '../../hoc/withEnquiryGuard'

const HealthEnquiry = () => {
	const state = useSelector(state => {
		const { baseInsurances, applicantAges } = state.coreData
		return {
			baseInsurances,
			applicantAges,
			form: state.healthInsurance.form
		}
	})
	const dispatch = useDispatch()

	const [form, setForm] = useState(state.form)

	const onSubmit = () => {
		dispatch(
			travelSetForm({
				...form
			})
		)
	}

	const onSetField = (field, value) => {
		setForm({ ...form, [field]: value })
	}

	const onReturn = () => {
		setForm(state.form)
	}

	const onAddApplicant = key => {
		const newApplicatns = { ...form.applicants }
		newApplicatns[key]++
		onSetField('applicants', newApplicatns)
	}

	const onSubtractApplicant = key => {
		const newApplicatns = { ...form.applicants }
		if (newApplicatns[key] > 0) {
			newApplicatns[key]--
			onSetField('applicants', newApplicatns)
		}
	}

	const canCheck = () => {
		for (let age in form.applicants) {
			if (form.applicants[age] > 0) return true
		}
		return false
	}

	const [insurances, setInsurances] = useState({ data: [], loading: false })

	useEffect(() => {
		const getInsurances = async () => {
			const body = makeRequestBody(state.form)
			setInsurances({ data: [], loading: true })
			fetchInsurances(body)
				.then(data => {
					setInsurances({ data: data.data, loading: false })
				})
				.catch(error => setInsurances({ data: [], loading: false }))
		}
		getInsurances()
	}, [state.form])

	const filterWrapper = (
		<div className="image-cover">
			<img src="/imgs/svg/health-icon.svg" alt="health_image" />
		</div>
	)

	const getApplicants = () => {
		const applicants = []
		for (let age of state.applicantAges) {
			const count = state.form.applicants[age.value]
			if (count > 0) applicants.push(age.name + ': ' + count)
		}

		return applicants.join(', ')
	}

	const properties = item => {
		const props = [
			{ key: 'بیمه گر پایه', value: state.form.baseInsurance.name },
			{ key: 'سن متقاضی (های) بیمه', value: getApplicants() }
		]
		const tarh = getStickyLabel(item)
		if (tarh) props.push({ key: 'طرح', value: tarh })
		return props
	}

	const renderDetails = item => {
		const {
			cover_hospitalization,
			cover_surgery,
			cover_sonography,
			cover_brain_bar,
			cover_decrepitude,
			cover_laboratory,
			cover_ambulance,
			cover_childbirth,
			cover_visit,
			cover_dentistry,
			cover_sterile,
			cover_eyes,
			cover_earphone
		} = item.content
		const wrapper = (
			<div className={'row ' + classes.table}>
				<div className="col-sm-6 col-md-4 border py-2">
					پوشش ویروس کرونا: دارد
				</div>
				<div className="col-sm-6 col-md-4 border py-2">
					هزینه های بیمارستانی:‌ {cover_hospitalization}
				</div>
				<div className="col-sm-6 col-md-4 border py-2">
					عمل های جراحی مهم: {cover_surgery}
				</div>
				<div className="col-sm-6 col-md-4 border py-2">
					پاراکلینیکی ۱ (انواع اسکن): {cover_sonography}
				</div>
				<div className="col-sm-6 col-md-4 border py-2">
					پاراکلینیکی گروه 2: {cover_brain_bar}
				</div>
				<div className="col-sm-6 col-md-4 border py-2">
					جراحی‌های مجاز سرپایی: {cover_brain_bar}
				</div>
				<div className="col-sm-6 col-md-4 border py-2">
					خدمات آزمایشگاهی: {cover_laboratory}
				</div>
				<div className="col-sm-6 col-md-4 border py-2">
					هزینه آمبولانس: {cover_ambulance}
				</div>
				<div className="col-sm-6 col-md-4 border py-2">
					هزینه زایمان: {cover_childbirth}
				</div>
				<div className="col-sm-6 col-md-4 border py-2">
					ویزیت و دارو: {cover_visit}
				</div>
				<div className="col-sm-6 col-md-4 border py-2">
					دندانپزشکی : {cover_dentistry}
				</div>
				<div className="col-sm-6 col-md-4 border py-2">نازایی : -</div>
				<div className="col-sm-6 border py-2">
					عیوب انکساری دو چشم : -
				</div>
				<div className="col-sm-6 border py-2">سمعک : -</div>
			</div>
		)
		return wrapper
	}

	const getStickyLabel = item => item && item.content.name

	return (
		<EnquiryLayout
			title="بیمه درمان تکمیلی"
			filterWrapper={filterWrapper}
			onSubmit={onSubmit}
			onReturn={onReturn}
			isValid={canCheck()}
			badges={state.form.badges}
			loading={insurances.loading}
			data={insurances.data}
			properties={properties}
			orderPath="/health-order"
			renderDetails={renderDetails}
			getStickyLabel={getStickyLabel}
			infoTitle="مشخصات کلی"
		>
			<div className="input-box py-2 px-2 mx-2">
				<span className="input-box__title">اطلاعات</span>
				<form>
					<div className="row mt-4">
						<div className="col-12 col-md-6  mb-3 mb-md-4 mx-auto">
							<Select
								label="بیمه پایه"
								id="base-insurance"
								changed={value =>
									onSetField('baseInsurance', value)
								}
								options={state.baseInsurances}
								value={form.baseInsurance}
								renderer={option => option.name}
							/>
						</div>
					</div>
					<div className="mt-2">
						<h1 className="mb-3" style={{ fontSize: 18 }}>
							سن متقاضی (های) بیمه
						</h1>
						<Actions
							items={state.applicantAges}
							values={form.applicants}
							onAdd={onAddApplicant}
							onRemove={onSubtractApplicant}
						/>
					</div>
				</form>
			</div>
		</EnquiryLayout>
	)
}

export default withEnquiryGuard(HealthEnquiry, 'health-insurance')
