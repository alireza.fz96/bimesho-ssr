import { useState } from 'react'
import OrderForm from '../../components/order-form/OrderForm'
import { useSelector } from 'react-redux'
import withAuth from '../../hoc/withAuth'
import withInsuranceGuard from '../../hoc/withInsuranceGuard'

const LifeOrder = () => {
	const form = useSelector(state => state.product.form)

	const [meliImage, setMeliImage] = useState(form.card_meli || '')

	const photos = [
		{
			name: 'card_meli',
			label: 'عکس کارت ملی',
			image: meliImage,
			setImage: setMeliImage,
			required: true
		}
	]

	return <OrderForm photos={photos} />
}

export default withAuth(withInsuranceGuard(LifeOrder, 'life-insurance'))
