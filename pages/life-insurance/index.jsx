import React from 'react'
import InsuranceStepsLayout from '../../layout/insurance-steps-layout/InsuranceStepsLayout'
import { useSelector, useDispatch } from 'react-redux'
import * as actions from '../../store/actions/lifeInsuranceActions'
import SwipeableViews from 'react-swipeable-views'
import InsuranceRight from '../../components/life-insurance/insurance-right/InsuranceRight'
import ContractTime from '../../components/life-insurance/contract-time/ContractTime'
import LifeInsuranceFaq from '../../components/life-insurance/faq/LifeInsuranceFaq'

const HealthInsurance = () => {
	const state = useSelector(state => state.lifeInsurance)
	const dispatch = useDispatch()

	const nextSlide = () => {
		dispatch(actions.nextSlide())
	}

	const prevSlide = () => {
		dispatch(actions.prevSlide())
	}

	React.useEffect(() => {
		dispatch(actions.reset())
	}, [dispatch])

	const { step } = state

	return (
		<div>
			<InsuranceStepsLayout
				{...step}
				title="بیمه عمر "
				next={nextSlide}
				prev={prevSlide}
				canReturn={step.index > 0}
				badges={state.form.badges}
			>
				<SwipeableViews index={step.index} axis="x-reverse">
					<div className="content__step">
						<InsuranceRight />
					</div>
					<div className="content__step">
						<ContractTime />
					</div>
				</SwipeableViews>
			</InsuranceStepsLayout>
			<LifeInsuranceFaq />
		</div>
	)
}

export default HealthInsurance
