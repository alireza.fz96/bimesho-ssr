import React from 'react'
import classes from '../../styles/Home.module.scss'
import InsuranceCards from '../../components/insurance-cards/InsuranceCards'
import ServiceCards from '../../components/service-cards/ServiceCards'
import PartnersCarousel from '../../components/partners-carousel/PartnersCarousel'

export default function Home() {
	return (
		<>
			<div className="container-lg max-width-100">
				<h1
					className={
						'py-3 pb-md-0 pt-md-5 mb-0 mb-md-4 w-100 max-width-100 ' +
						classes.title
					}
				>
					<span className="d-none d-md-inline">مقایسه و </span>
					خرید بیمه آنلاین بیمه شو
				</h1>
				<div className="container-fluid">
					<div className="row">
						<div className="col-12">
							<div className="row align-items-start">
								<div className="col-md-7 col-12">
									<InsuranceCards />
								</div>
								<div className="col-md-5 d-none d-md-block pr-0">
									<div className={classes.banner}>
										<div
											className={
												classes.banner__background
											}
										></div>
										<div
											className={classes.banner__content}
										>
											<h1>
												با بیمه شو
												<br />ارزانتر بیمه شو
											</h1>
											<hr className="my-1" />
											<span
												className={classes.banner__link}
											>
												bimesho.com
											</span>
											<br />
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div className="container">
				<section className={classes.services}>
					<h2 className={classes.heading}>
						خدمات بیمه شو را بیشتر بشناسیم
					</h2>
					<ServiceCards />
				</section>
				<section className={classes.partners}>
					<h2 className={classes.heading}>بیمه‌های همکار</h2>
					<PartnersCarousel />
				</section>
			</div>
		</>
	)
}
