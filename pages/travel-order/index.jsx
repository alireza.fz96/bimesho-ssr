import React, { useState } from 'react'
import OrderForm from '../../components/order-form/OrderForm'
import { useSelector } from 'react-redux'
import withAuth from '../../hoc/withAuth'
import withInsuranceGuard from '../../hoc/withInsuranceGuard'

const TravelOrder = () => {
	const form = useSelector(state => state.product.form)

	const [passportImage, setPassportImage] = useState(form.passport || '')

	const photos = [
		{
			name: 'passport',
			label: 'تصویر گذرنامه',
			image: passportImage,
			setImage: setPassportImage,
			required: true
		}
	]

	return <OrderForm photos={photos} />
}

export default withAuth(withInsuranceGuard(TravelOrder, 'travel-insurance'))
