import React, { useState, useEffect } from 'react'
import {
	fetchInsurances,
	makeRequestBody
} from '../../api/ThirdpersonInsuranceApi'
import EnquiryLayout from '../../layout/enquiry-layout/EnquiryLayout'

import { useSelector, useDispatch } from 'react-redux'
import Select from '../../components/UI/Select/Select'
import { DatePicker } from '@material-ui/pickers'
import { Autocomplete } from '@material-ui/lab'
import { Dialog, TextField } from '@material-ui/core'
import { setForm as thirdSetForm } from '../../store/actions/thirdPartyInsuranceActions'
import ThirdPartyFilter from '../../components/third-party-insurance/third-party-filter/ThirdPartyFilter'
import { useBrands, useCategories, useModels } from '../../hooks/vehicles'
import classes from '../../styles/ThirdPartyEnquiry.module.scss'
import jMoment from 'moment-jalaali'
import withEnquiryGuard from '../../hoc/withEnquiryGuard'

const ThirdPartyEnquiry = () => {
	const state = useSelector(state => {
		const {
			driverAccidentDamageCounts,
			driverAccidentDamageDiscounts,
			lifeDamageCounts,
			financeDamageCounts,
			years,
			insuranceDiscounts,
			companiesList
		} = state.coreData
		return {
			driverAccidentDamageCounts,
			driverAccidentDamageDiscounts,
			form: state.thirdPartyInsurance.form,
			insuranceDiscounts,
			years,
			financeDamageCounts,
			lifeDamageCounts,
			companiesList
		}
	})

	const insuranceHistoryOptions = [
		{ name: 'فاقد بیمه نامه', id: '120' },
		{ name: 'خودرو صفر کیلومتر', id: '110' },
		...state.companiesList
	]

	const dispatch = useDispatch()

	const [form, setForm] = useState(state.form)

	const { categories } = useCategories()
	const { brands } = useBrands(form.category && form.category.id)
	const { models } = useModels(form.brand && form.brand.id)

	useEffect(() => {
		if (form.category && form.category.name !== state.form.category.name) {
			setForm(form => ({ ...form, model: null, brand: null }))
		}
	}, [form.category, state.form])

	useEffect(() => {
		if (form.brand && form.brand.name !== state.form.brand.name) {
			setForm(form => ({ ...form, model: null }))
		}
	}, [form.brand, state.form])

	const onSubmit = () => {
		dispatch(
			thirdSetForm({
				...form,
				badges: [
					form.category.name,
					form.brand.name,
					form.model.name,
					form.productionYear.name
				]
			})
		)
	}

	const onSetField = (field, value) => {
		setForm({ ...form, [field]: value })
	}

	const [coverMoney, setCoverMoney] = useState(11)

	const [insurances, setInsurances] = useState({ data: [], loading: false })

	const onChangeCoverMoney = value => {
		setCoverMoney(value)
	}

	useEffect(() => {
		const getInsurances = () => {
			setInsurances({ data: [], loading: true })
			const body = makeRequestBody(state.form, coverMoney)
			fetchInsurances(body)
				.then(data => {
					setInsurances({
						data: Object.values(data.data.data),
						loading: false
					})
				})
				.catch(error => {
					setInsurances({ data: [], loading: false })
				})
		}
		getInsurances()
	}, [coverMoney, state.form])

	const filterWrapper = <ThirdPartyFilter changed={onChangeCoverMoney} />

	const onReturn = () => {
		setErrorMessage('')
		setForm(state.form)
	}

	const getProperties = () => {
		const properties = [
			{ key: 'گروه خودرو', value: state.form.category.name },
			{ key: 'نوع خودرو', value: state.form.brand.name },
			{ key: 'مدل خودرو', value: state.form.model.name },
			{
				key: 'بیمه نامه قبلی',
				value: insuranceHistoryOptions.find(
					i => i.id === state.form.prevCompany
				).name
			}
		]
		if (form.prevCompany !== '110' && form.prevCompany !== '120') {
			properties.push(
				{
					key: 'شروع بیمه نامه',
					value: jMoment(form.insuranceStart)
						.locale('en')
						.format('jYYYY/jM/jD')
				},
				{
					key: 'پایان بیمه نامه',
					value: jMoment(form.insuranceEnd)
						.locale('en')
						.format('jYYYY/jM/jD')
				},
				{
					key: 'تخفیف بیمه شخص ثالث',
					value: form.insuranceDiscount.name
				},
				{
					key: 'تخفیف حوادث راننده',
					value: form.driverAccidentDamageDiscount.name
				}
			)
			if (hasAccidents.value) {
				properties.push(
					{ key: 'خسارت مالی', value: form.financeDamageCount.name },
					{ key: 'خسارت جانی', value: form.lifeDamageCount.name },
					{
						key: 'خسارت حوادث راننده',
						value: form.driverAccidentDamageCount.name
					}
				)
			}
		} else if (form.prevCompany === '110') {
			properties.push({
				key: 'تاریخ ترخیص خودرو',
				value: jMoment(form.datePrice)
					.locale('en')
					.format('jYYYY/jM/jD')
			})
		}
		return properties
	}

	const yearRenderer = year => (
		<>
			<span className="year miladi">{year.miladi}</span>|
			<span className="year">{year.jalali}</span>
		</>
	)

	const [errorMessage, setErrorMessage] = React.useState('')

	const setError = (start, end) => {
		if (start.isAfter && start.isAfter(end, 'day'))
			setErrorMessage({ type: 'smaller' })
		else if (start.isSame && start.isSame(end, 'day'))
			setErrorMessage({ type: 'notEqual' })
		else if (end.diff && end.diff(start, 'day') > 366)
			setErrorMessage({ type: 'limit' })
		else {
			setErrorMessage('')
		}
	}

	const clearFields = prevCompany => {
		if (prevCompany === '110' || prevCompany === '120') {
			setForm({
				...form,
				driverAccidentDamageCount: null,
				driverAccidentDamageDiscount: null,
				financeDamageCount: null,
				lifeDamageCount: null,
				insuranceDiscount: null,
				datePrice: null,
				insuranceEnd: null,
				prevCompany
			})
			setHasAccidents(hasAccidentsOptions[1])
		} else setForm({ ...form, prevCompany })
	}

	const getErrorMessage = () => {
		if (errorMessage) {
			switch (errorMessage.type) {
				case 'notEqual':
					return 'تاریخ شروع و انقضای بیمه نامه نمیتواند یکی باشد'
				case 'smaller':
					return 'تاریخ شروع بیمه نامه نمیتواند بعد از تاریخ انقضا باشد'
				case 'limit':
					return 'انقضای بیمه نامه نمیتواند بیشتر از یک سال باشد'
				default:
					return ''
			}
		} else return ''
	}

	const isValidForm = () => {
		if (form.prevCompany !== '110' && form.prevCompany !== '120') {
			if (hasAccidents.value)
				return (
					form.driverAccidentDamageCount &&
					form.driverAccidentDamageDiscount &&
					form.insuranceDiscount &&
					form.financeDamageCount &&
					form.lifeDamageCount &&
					form.insuranceEnd
				)
			else
				return (
					form.driverAccidentDamageDiscount &&
					form.insuranceDiscount &&
					form.insuranceEnd
				)
		} else if (form.prevCompany === '110') return form.datePrice
		else return true
	}

	const [dialog, setDialog] = React.useState({
		open: false,
		content: null
	})

	const onOpenHelp = content => {
		setDialog({ open: true, content })
	}

	const handleClose = () => {
		setDialog({ open: false, content: null })
	}

	const [hasAccidents, setHasAccidents] = React.useState(
		!!state.form.lifeDamageCount
			? hasAccidentsOptions[0]
			: hasAccidentsOptions[1]
	)

	const onSetHasAccident = option => {
		setHasAccidents(option)
		if (!option.value) onSetNoAccident()
	}

	const onSetNoAccident = () => {
		setForm({
			...form,
			driverAccidentDamageCount: null,
			financeDamageCount: null,
			lifeDamageCount: null
		})
	}

	const details = (
		<div className={classes.cardsContainer}>
			<div className={classes.simpleCard}>
				<div
					className={[
						classes.simpleCard__item,
						classes.simpleCard__value
					].join(' ')}
				>
					میزان جریمه
				</div>
				<div
					className={[
						classes.simpleCard__item,
						classes.simpleCard__key
					].join(' ')}
				>
					1,968,445 تومان
				</div>
			</div>
			<div className={classes.simpleCard}>
				<div
					className={[
						classes.simpleCard__item,
						classes.simpleCard__value
					].join(' ')}
				>
					میزان تاخیر
				</div>
				<div
					className={[
						classes.simpleCard__item,
						classes.simpleCard__key
					].join(' ')}
				>
					365 روز
				</div>
			</div>
			<div className={classes.simpleCard}>
				<div
					className={[
						classes.simpleCard__item,
						classes.simpleCard__value
					].join(' ')}
				>
					جریمه/روز
				</div>
				<div
					className={[
						classes.simpleCard__item,
						classes.simpleCard__key
					].join(' ')}
				>
					5,393 تومان
				</div>
			</div>
		</div>
	)

	return (
		<EnquiryLayout
			title="بیمه شخص ثالث"
			onSubmit={onSubmit}
			isValid={form.model && form.brand && !errorMessage && isValidForm()}
			badges={state.form.badges}
			filterWrapper={filterWrapper}
			onReturn={onReturn}
			data={insurances.data || []}
			loading={insurances.loading}
			properties={getProperties}
			orderPath="/third-party-order"
			renderDetails={() => details}
			infoTitle="مشخصات اتومبیل"
			insuranceParams={[
				{
					key: 'پوشش مالی',
					name: 'commitments',
					data: coverMoney,
					value:
						Number(coverMoney + '000000').toLocaleString() +
						' تومان'
				}
			]}
		>
			<form>
				<div className="input-box py-2 px-lg-5 px-3 mx-md-3 mx-2">
					<span className="input-box__title">اطلاعات خودرو</span>
					<div className="row mt-4">
						<div className="col-12 col-md-6  mb-3 mb-md-4">
							<Autocomplete
								options={categories || []}
								getOptionLabel={option => option.name || ''}
								value={form.category}
								closeIcon={false}
								onChange={(_, value) =>
									typeof value === 'object' &&
									onSetField('category', value)
								}
								renderInput={params => (
									<TextField
										{...params}
										label="نوع وسیله"
										variant="outlined"
										autoFocus
									/>
								)}
							/>
						</div>
						<div className="col-12 col-md-6  mb-3 mb-md-4">
							<Autocomplete
								options={brands || []}
								getOptionLabel={option => option.name || ''}
								value={form.brand}
								closeIcon={false}
								onChange={(_, value) =>
									onSetField('brand', value)
								}
								renderInput={params => (
									<TextField
										{...params}
										label="برند"
										variant="outlined"
										autoFocus
										required
										error={!form.brand}
										helperText={
											!form.brand
												? 'لطفا برند را مشخص کنید'
												: ''
										}
									/>
								)}
							/>
						</div>
					</div>
					<div className="row">
						<div className="col-12 col-md-6  mb-3 mb-md-4">
							<Autocomplete
								options={models || []}
								getOptionLabel={option => option.name || ''}
								value={form.model}
								closeIcon={false}
								onChange={(_, value) =>
									onSetField('model', value)
								}
								disabled={!form.brand}
								renderInput={params => (
									<TextField
										{...params}
										label="مدل"
										variant="outlined"
										autoFocus
										required
										error={!form.model}
										helperText={
											!form.model
												? 'لطفا مدل را مشخص کنید'
												: ''
										}
									/>
								)}
							/>
						</div>
						<div className="col-12 col-md-6  mb-3 mb-md-4">
							<Select
								label="سال ساخت وسیله نقلیه"
								id="production-year"
								changed={value =>
									onSetField('productionYear', value)
								}
								options={state.years}
								value={form.productionYear}
								renderer={yearRenderer}
							/>
						</div>
					</div>
				</div>
				<div className="input-box py-2 px-lg-5 px-3 mx-md-3 mx-2 mt-5">
					<span className="input-box__title">
						اطلاعات بیمه نامه قبلی
					</span>
					<div className="row mt-4">
						<div className="col-12 col-md-6 mb-3 mb-md-4">
							<Autocomplete
								options={insuranceHistoryOptions}
								getOptionLabel={option => option.name || ''}
								value={insuranceHistoryOptions.find(
									a => a.id === form.prevCompany
								)}
								closeIcon={false}
								onChange={(_, value) =>
									value && clearFields(value.id)
								}
								renderInput={params => (
									<TextField
										{...params}
										label="سابقه بیمه نامه قبلی"
										variant="outlined"
									/>
								)}
							/>
						</div>
						<div className="col-12 col-md-6  mb-3 mb-md-4">
							{form.prevCompany !== '110' &&
								form.prevCompany !== '120' && (
									<DatePicker
										value={form.insuranceStart}
										labelFunc={date =>
											date
												? date.format('jYYYY/jMM/jDD')
												: ''
										}
										onChange={value => {
											onSetField('insuranceStart', value)
											setError(value, form.insuranceEnd)
										}}
										variant="inline"
										inputVariant="outlined"
										label="تاریخ شروع بیمه نامه قبلی"
										autoOk
									/>
								)}
							{form.prevCompany === '110' && (
								<DatePicker
									value={form.datePrice}
									labelFunc={date =>
										date ? date.format('jYYYY/jMM/jDD') : ''
									}
									onChange={value => {
										onSetField('datePrice', value)
									}}
									variant="inline"
									inputVariant="outlined"
									label="تاریخ ترخیص خودرو"
									autoOk
								/>
							)}
						</div>
					</div>
					{form.prevCompany !== '110' && form.prevCompany !== '120' && (
						<div className="row">
							<div className="col-12 col-md-6  mb-3 mb-md-4">
								<DatePicker
									value={form.insuranceEnd}
									labelFunc={date =>
										date ? date.format('jYYYY/jMM/jDD') : ''
									}
									onChange={value => {
										onSetField('insuranceEnd', value)
										setError(form.insuranceStart, value)
									}}
									variant="inline"
									inputVariant="outlined"
									label="تاریخ پایان بیمه نامه قبلی"
									error={!!errorMessage}
									helperText={getErrorMessage()}
									autoOk
								/>
							</div>
						</div>
					)}
				</div>
				{form.prevCompany !== '110' && form.prevCompany !== '120' && (
					<div className="input-box py-2 px-lg-5 px-3 mx-md-3 mx-2 mt-5">
						<span className="input-box__title">
							تخفیف بیمه نامه قبلی
						</span>
						<div className="row mt-4">
							<div className="col-12 col-md-6  mb-3 mb-md-4">
								<div className="help-input">
									<Select
										label="تخفیف حوادث راننده"
										id="driver-accident-discount"
										changed={value =>
											onSetField(
												'driverAccidentDamageDiscount',
												value
											)
										}
										options={
											state.driverAccidentDamageDiscounts
										}
										value={
											form.driverAccidentDamageDiscount
										}
										renderer={value => value.name}
									/>
									<div
										className="extra-badge"
										onClick={() =>
											onOpenHelp(
												<img
													src="/imgs/help/2.jpg"
													alt="help_img"
													className="img-fluid"
												/>
											)
										}
									>
										راهنما
									</div>
								</div>
							</div>
							<div className="col-12 col-md-6 mb-3 mb-md-4">
								<div className="help-input">
									<Select
										id="prev-insurance-discount"
										value={form.insuranceDiscount}
										changed={value =>
											onSetField(
												'insuranceDiscount',
												value
											)
										}
										label="تخفیف شخص ثالث"
										options={state.insuranceDiscounts}
										renderer={option => option.name}
									/>
									<div
										className="extra-badge"
										onClick={() =>
											onOpenHelp(
												<img
													src="/imgs/help/1.jpg"
													alt="help_img"
													className="img-fluid"
												/>
											)
										}
									>
										راهنما
									</div>
								</div>
							</div>
						</div>

						<div className="row">
							<div className="col-12 col-md-6  mb-3 mb-md-4">
								<Select
									label="از بیمه نامه قبلی خود خسارت گرفته اید؟"
									id="driver-accident-count"
									changed={onSetHasAccident}
									options={hasAccidentsOptions}
									value={hasAccidents}
									renderer={value => value.name}
								/>
							</div>
							{hasAccidents.value && (
								<div className="col-12 col-md-6  mb-3 mb-md-4">
									<div className="help-input">
										<Select
											label="خسارت حوادث راننده"
											id="driver-accident-count"
											changed={value =>
												onSetField(
													'driverAccidentDamageCount',
													value
												)
											}
											options={
												state.driverAccidentDamageCounts
											}
											value={
												form.driverAccidentDamageCount
											}
											renderer={value => value.name}
										/>
										<div
											className="extra-badge"
											onClick={() =>
												onOpenHelp(
													<p className="p-3">
														تعداد دفعاتی که خسارت
														جانی راننده خودرو (شامل
														دیه جرح، دیه فوت و دیه
														نقص عضو) را از بیمه‌نامه
														قبلی خود دریافت
														کرده‌اید.
													</p>
												)
											}
										>
											راهنما
										</div>
									</div>
								</div>
							)}
						</div>
						{hasAccidents.value && (
							<div className="row">
								<div className="col-12 col-md-6  mb-3 mb-md-4">
									<div className="help-input">
										<Select
											label="خسارت جانی"
											id="life-damge-counts"
											changed={value =>
												onSetField(
													'lifeDamageCount',
													value
												)
											}
											options={state.lifeDamageCounts}
											value={form.lifeDamageCount}
											renderer={value => value.name}
										/>
										<div
											className="extra-badge"
											onClick={() =>
												onOpenHelp(
													<p className="p-3">
														تعداد دفعاتی که خسارت
														جانی شخص ثالث (شامل دیه
														جرح، دیه فوت و دیه نقص
														عضو) را از بیمه‌نامه
														قبلی خود دریافت
														کرده‌اید.
													</p>
												)
											}
										>
											راهنما
										</div>
									</div>
								</div>
								<div className="col-12 col-md-6  mb-3 mb-md-4">
									<div className="help-input">
										<Select
											label="خسارت مالی"
											id="finance-damge-counts"
											changed={value =>
												onSetField(
													'financeDamageCount',
													value
												)
											}
											options={state.financeDamageCounts}
											value={form.financeDamageCount}
											renderer={value => value.name}
										/>
										<div
											className="extra-badge"
											onClick={() =>
												onOpenHelp(
													<p className="p-3">
														تعداد دفعاتی که خسارت
														مالی شخص ثالث را از
														بیمه‌نامه قبلی خود
														دریافت کرده‌اید.
													</p>
												)
											}
										>
											راهنما
										</div>
									</div>
								</div>
							</div>
						)}
					</div>
				)}
			</form>
			<Dialog onClose={handleClose} open={dialog.open}>
				<div onClick={handleClose}>{dialog.content}</div>
			</Dialog>
		</EnquiryLayout>
	)
}

const hasAccidentsOptions = [
	{
		name: 'بله',
		value: true
	},
	{ name: 'خیر', value: false }
]

export default withEnquiryGuard(ThirdPartyEnquiry, 'third-party-insurance')
