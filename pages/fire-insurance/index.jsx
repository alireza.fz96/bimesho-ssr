import React from 'react'
import InsuranceStepsLayout from '../../layout/insurance-steps-layout/InsuranceStepsLayout'
import { useSelector, useDispatch } from 'react-redux'
import * as actions from '../../store/actions/fireInsuranceActions'
import SwipeableViews from 'react-swipeable-views'
import PropertyType from '../../components/fire-insurance/property-type/PropertyType'
import UnitCount from '../../components/fire-insurance/unit-count/UnitCount'
import UnitValue from '../../components/fire-insurance/unit-value/UnitValue'
import FireInsuranceFaq from '../../components/fire-insurance/faq/FireInsuranceFaq'

const FireInsurance = () => {
	const state = useSelector(state => state.fireInsurance)
	const dispatch = useDispatch()

	const nextSlide = () => {
		dispatch(actions.nextSlide())
	}

	const prevSlide = () => {
		dispatch(actions.prevSlide())
	}

	React.useEffect(() => {
		dispatch(actions.reset())
	}, [dispatch])

	const { step } = state

	return (
		<div>
			<InsuranceStepsLayout
				{...step}
				title="بیمه آتش سوزی"
				next={nextSlide}
				prev={prevSlide}
				canReturn={step.index > 0}
				badges={state.form.badges}
			>
				<SwipeableViews index={step.index} axis="x-reverse">
					<div className="content__step">
						<PropertyType />
					</div>
					<div className="content__step">
						<UnitCount />
					</div>
					<div className="content__step">
						<UnitValue />
					</div>
				</SwipeableViews>
			</InsuranceStepsLayout>
			<FireInsuranceFaq />
		</div>
	)
}

export default FireInsurance
