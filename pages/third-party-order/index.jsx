import { useState } from 'react'
import OrderForm from '../../components/order-form/OrderForm'
import { useSelector } from 'react-redux'
import withAuth from '../../hoc/withAuth'
import withInsuranceGuard from '../../hoc/withInsuranceGuard'

const ThirdPartyOrder = () => {
	const form = useSelector(state => state.product.form)
	const [frontImage, setFrontImage] = useState(form.card_front || '')
	const [backImage, setBackImage] = useState(form.card_back || '')
	const [prevInsurance, setPrevInsurance] = useState(
		form.prev_insurance || ''
	)

	const photos = [
		{
			name: 'card_front',
			label: 'عکس روی کارت ماشین',
			image: frontImage,
			setImage: setFrontImage,
			required: true
		},
		{
			name: 'card_back',
			label: 'عکس پشت کارت ماشین',
			image: backImage,
			setImage: setBackImage,
			required: true
		},
		{
			name: 'prev_insurance',
			label: 'عکس بیمه نامه قبلی (در صورت وجود)',
			image: prevInsurance,
			setImage: setPrevInsurance,
			required: false
		}
	]

	return <OrderForm photos={photos} />
}

export default withAuth(
	withInsuranceGuard(ThirdPartyOrder, 'third-party-insurance')
)
