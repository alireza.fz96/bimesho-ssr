import React, { useState } from 'react'
import OrderSteps from '../../components/order-steps/OrderSteps'
import classes from '../../styles/Payment.module.scss'
import { useSelector } from 'react-redux'
import {
	Button,
	Dialog,
	DialogActions,
	DialogContent,
	DialogContentText,
	Slide,
	useMediaQuery,
	useTheme
} from '@material-ui/core'
import Terms from '../../components/terms/Terms'
import DiscountCode from '../../components/discount-code/DiscountCode'
import withPaymentGuard from '../../hoc/withPaymentGuard'
import withAuth from '../../hoc/withAuth'

const Transition = React.forwardRef(function Transition(props, ref) {
	return <Slide direction="up" ref={ref} {...props} />
})

const Payment = () => {
	const state = useSelector(state => state.product.product.summary)
	const discount = useSelector(state => state.product.discountCode)
	const globalState = useSelector(state => state)

	const [bank, setBank] = React.useState('')

	const [showTerms, setShowTerms] = React.useState(false)
	const theme = useTheme()

	const fullScreen = useMediaQuery(theme.breakpoints.down('sm'))

	const onSubmit = data => {
		console.log(data)
	}

	const [isLoading, setIsLoading] = useState('')

	const onToggleLoading = () => {
		setIsLoading(i => !i)
	}

	return (
		<form>
			<div style={{ fontSize: 14 }}>
				<div className="container my-2 px-md-5">
					<OrderSteps step={3} />
				</div>
				<div className="mb-5 container">
					<div className="content-wrapper">
						<h1 className="text-center mb-4">فاکتور پرداخت</h1>
						<div className={classes.payment__container}>
							<div className="px-3">
								{state.map(value => (
									<div key={value.header}>
										<div className="row justify-content-center">
											<div className={classes.title}>
												<p>{value.header}</p>
											</div>
										</div>
										{value.body.map(value => (
											<div
												className="row justify-content-between my-3"
												key={value.key}
											>
												<div className={classes.key}>
													{value.key}
												</div>
												<div className={classes.value}>
													{value.value}
												</div>
											</div>
										))}
									</div>
								))}
							</div>
							<div className={classes.payment__details}>
								مبلغ کل قابل پرداخت:{' '}
								<span
									style={{
										textDecoration: discount
											? 'line-through'
											: 'none'
									}}
								>
									{
										state[1].body.find(
											value =>
												value.key === 'قیمت بیمه نامه'
										).value
									}
									<br />
								</span>
								{discount && (
									<span>
										{Number(
											discount.final_price
										).toLocaleString()}{' '}
										تومان
									</span>
								)}
							</div>
							<div className={classes.gift__container}>
								<div className="d-flex align-items-center mb-2">
									<img
										src="/imgs/svg/gift.svg"
										alt="gift_icon"
										className={classes.gift__icon}
									/>
									<h2>
										در صورت داشتن کد تخفیف یا کد معرف اینجا
										وارد کنید.
									</h2>
								</div>
								<DiscountCode toggleLoading={onToggleLoading} />
							</div>
						</div>

						<div className={classes.ipg__container}>
							<div className="text-center mt-4">
								<h1 className="mb-4">درگاه پرداخت</h1>
								<div className="mb-4">
									لطفا درگاه پرداخت مورد نظر خود را انتخاب
									کنید.
								</div>
								<div className={classes.payment__cont}>
									<div className={classes.payment__agree}>
										با انتخاب درگاه بانک{' '}
										<span
											className={classes.termLink}
											onClick={() => setShowTerms(true)}
										>
											شرایط و قوانین
										</span>{' '}
										را مطالعه نموده و پذیرفته‌ام
									</div>
								</div>
								<div className="row justify-content-center px-3">
									{banks.map(bankItem => (
										<div
											key={bankItem.id}
											className="grid-view__wrapper col-6 col-md-4 col-lg-3"
										>
											<div
												className={
													'grid-view__item ' +
													(bank === bankItem.value
														? 'active'
														: '')
												}
												onClick={() =>
													setBank(bankItem.value)
												}
											>
												<img
													src={`/imgs/ipg/${bankItem.id}.png`}
													alt={bankItem.name + 'logo'}
													className={
														classes.ipg__icon
													}
												/>
												<span className="grid-view__title">
													{bankItem.name}
												</span>
											</div>
										</div>
									))}
								</div>
							</div>
							<div className="mt-5 text-center">
								<Button
									color="primary"
									variant="contained"
									disabled={!bank || isLoading}
									onClick={onSubmit}
									type="submit"
								>
									تایید و پرداخت
								</Button>
							</div>
						</div>
					</div>
				</div>
			</div>
			<Dialog
				open={showTerms}
				TransitionComponent={Transition}
				keepMounted
				onClose={() => setShowTerms(false)}
				fullScreen={fullScreen}
			>
				<DialogContent>
					<DialogContentText className="py-3 p-md-3" component="div">
						<div className={'container ' + classes.dialog}>
							<Terms />
						</div>
					</DialogContentText>
				</DialogContent>
				<DialogActions>
					<div className="text-center w-100">
						<Button
							onClick={() => setShowTerms(false)}
							color="primary"
							variant="contained"
						>
							بستن
						</Button>
					</div>
				</DialogActions>
			</Dialog>
		</form>
	)
}

const banks = [
	{ name: 'سامان', value: 'saman', id: 6 },
	{ name: 'ملی', value: 'meli', id: 3 }
]

export default withAuth(withPaymentGuard(Payment))
