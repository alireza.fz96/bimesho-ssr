import '../styles/globals.scss'
import { useEffect } from 'react'
import { createMuiTheme, ThemeProvider, makeStyles } from '@material-ui/core'
import RTL from '../hoc/RTL'
import { MuiPickersUtilsProvider } from '@material-ui/pickers'
import DateFnsUtils from '@date-io/jalaali'
import jMoment from 'moment-jalaali'
jMoment.loadPersian({ dialect: 'persian-modern', usePersianDigits: true })
import Layout from '../layout/Layout'
import { wrapper } from '../store/store'
import 'bootstrap/dist/css/bootstrap-grid.min.css'
import 'slick-carousel/slick/slick.css'
import 'slick-carousel/slick/slick-theme.css'
import 'nprogress/nprogress.css'
import Nprogress from 'nprogress'
import Router from 'next/router'

Router.onRouteChangeStart = () => Nprogress.start()
Router.onRouteChangeError = () => Nprogress.done()
Router.onRouteChangeComplete = () => Nprogress.done()

const theme = createMuiTheme({
	direction: 'rtl',
	typography: {
		fontFamily: 'Yekan'
	}
})

const useStyle = makeStyles({
	root: {
		'& .MuiTextField-root': {
			width: '100%'
		},
		'& .MuiOutlinedInput-input': {
			padding: '16px 14px',
			fontSize: 14,
			outline: 'none'
		},
		'& .MuiSelect-selectMenu': {
			overflow: 'unset'
		},
		'& .MuiFormControl-root': {
			width: '100%'
		},
		'& .MuiStepLabel-label:not(.MuiStepLabel-active)': {
			[theme.breakpoints.down('sm')]: {
				display: 'none'
			}
		}
	}
})

function MyApp({ Component, pageProps }) {
	useEffect(() => {
		// Remove the server-side injected CSS.
		const jssStyles = document.querySelector('#jss-server-side')
		if (jssStyles) {
			jssStyles.parentElement.removeChild(jssStyles)
		}
	}, [])
	const styles = useStyle()
	return (
		<RTL>
			<ThemeProvider theme={theme}>
				<MuiPickersUtilsProvider utils={DateFnsUtils} locale="fa">
					<div className={styles.root}>
						<Layout>
							<Component {...pageProps} />
						</Layout>
					</div>
				</MuiPickersUtilsProvider>
			</ThemeProvider>
		</RTL>
	)
}

export default wrapper.withRedux(MyApp)
