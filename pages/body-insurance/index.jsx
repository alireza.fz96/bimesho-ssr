import React from 'react'
import InsuranceStepsLayout from '../../layout/insurance-steps-layout/InsuranceStepsLayout'
import VehicleBrands from '../../components/vehicle-brands/VehicleBrands'
import VehicleModels from '../../components/vehicle-models/VehicleModels'
import ProductionYear from '../../components/production-year/ProductionYear'
import BodyInsuranceFaq from '../../components/body-insurance/faq/BodyInsuranceFaq'
import { useSelector, useDispatch } from 'react-redux'
import * as actions from '../../store/actions/bodyInsuranceActions'
import SwipeableViews from 'react-swipeable-views'
import CoverDiscount from '../../components/body-insurance/cover-discount/CoverDiscount'
import VehicleValue from '../../components/body-insurance/vehicle-value/VehicleValue'

const BodyInsurance = () => {
	const state = useSelector(state => state.bodyInsurance)
	const dispatch = useDispatch()

	const nextSlide = () => {
		dispatch(actions.nextSlide())
	}

	const prevSlide = () => {
		dispatch(actions.prevSlide())
	}

	React.useEffect(() => {
		dispatch(actions.initial())
	}, [dispatch])

	const { step } = state

	return (
		<div>
			<InsuranceStepsLayout
				{...step}
				title="بیمه بدنه"
				next={nextSlide}
				prev={prevSlide}
				canReturn={step.index > 0}
				badges={state.form.badges}
				disabled
			>
				<SwipeableViews index={step.index} axis="x-reverse">
					<div className="content__step">
						<VehicleBrands bodyInsurance />
					</div>
					<div className="content__step">
						<VehicleModels bodyInsurance />
					</div>
					<div className="content__step">
						<ProductionYear bodyInsurance />
					</div>
					<div className="content__step">
						<CoverDiscount />
					</div>
					<div className="content__step">
						<VehicleValue />
					</div>
				</SwipeableViews>
			</InsuranceStepsLayout>
			<BodyInsuranceFaq />
		</div>
	)
}

export default BodyInsurance
