import React, { useState } from 'react'
import OrderForm from '../../components/order-form/OrderForm'
import { useSelector } from 'react-redux'
import withAuth from '../../hoc/withAuth'
import withInsuranceGuard from '../../hoc/withInsuranceGuard'

const MotorOrder = () => {
	const form = useSelector(state => state.product.form)

	const [frontImage, setFrontImage] = useState(form.card_front || '')
	const [backImage, setBackImage] = useState(form.card_back || '')

	const photos = [
		{
			name: 'card_front',
			label: 'عکس روی کارت موتور یا فاکتور خرید',
			image: frontImage,
			setImage: setFrontImage,
			required: true
		},
		{
			name: 'card_back',
			label: 'عکس پشت کارت موتور یا فاکتور خرید',
			image: backImage,
			setImage: setBackImage,
			required: true
		}
	]

	return <OrderForm photos={photos} />
}

export default withAuth(withInsuranceGuard(MotorOrder, 'motor-insurance'))
