import React, { useEffect, useState } from 'react'
import EnquiryLayout from '../../layout/enquiry-layout/EnquiryLayout'
import { useSelector, useDispatch } from 'react-redux'
import Select from '../../components/UI/Select/Select'
import { setForm as liefSetForm } from '../../store/actions/lifeInsuranceActions'
import { fetchInsurances, makeRequestBody } from '../../api/lifeInsuranceApi'
import withEnquiryGuard from '../../hoc/withEnquiryGuard'

const LifeEnquiry = () => {
	const state = useSelector(state => {
		const {
			insuranceRights,
			paymentMethods,
			contractTimes,
			years
		} = state.coreData
		return {
			insuranceRights,
			paymentMethods,
			contractTimes,
			years,
			form: state.lifeInsurance.form
		}
	})
	const dispatch = useDispatch()

	const [form, setForm] = useState(state.form)

	const onSubmit = () => {
		dispatch(
			liefSetForm({
				...form
			})
		)
	}

	const onSetField = (field, value) => {
		setForm({ ...form, [field]: value })
	}

	const onReturn = () => {
		setForm(state.form)
	}

	const [insurances, setInsurances] = useState({ data: [], loading: false })

	useEffect(() => {
		const getInsurances = async () => {
			const body = makeRequestBody(state.form)
			setInsurances({ data: [], loading: true })
			fetchInsurances(body)
				.then(data => {
					setInsurances({ data: data.data.data, loading: false })
				})
				.catch(error => setInsurances({ data: [], loading: false }))
		}
		getInsurances()
	}, [state.form])

	const properties = item => {
		const props = [
			{ key: 'حق بیمه', value: state.form.insuranceRight.name },
			{ key: 'نحوه ی پرداخت', value: state.form.paymentMethod.name },
			{ key: 'طول مدت قرارداد', value: state.form.contractTime.name },
			{ key: 'تاریخ تولد', value: state.form.birthday.jalali },
			...getParams(item)
		]
		return props
	}

	const renderer = year => (
		<>
			<span className="year miladi">{year.miladi}</span>|
			<span className="year">{year.jalali}</span>
		</>
	)

	const filterWrapper = (
		<div className="image-cover">
			<img src="/imgs/svg/life-icon.svg" alt="health_image" />
		</div>
	)

	const getParams = item => {
		const {
			price,
			periodic,
			all,
			s_diseases,
			s_medical,
			death,
			s_maim
		} = item
		const params = [
			{
				key: 'توان پرداخت سالانه',
				value: `${Number(price).toLocaleString()} تومان`
			},
			{
				key: 'مبلغ مستمری',
				value: `${Number(periodic).toLocaleString()} تومان`
			},
			{
				key: 'ارزش بازخریدی',
				value: `${Number(all).toLocaleString()} تومان`
			},
			{
				key: 'ضریب هزینه پزشکی بر اثر حادثه',
				value: `${Number(price).toLocaleString()} تومان`
			},
			{
				key: 'ضریب نقص عضو بر اثر حادثه',
				value: `${Number(s_maim).toLocaleString()} تومان`
			},
			{
				key: 'ضریب معافیت از پرداخت حق بیمه',
				value: `-`
			},
			{
				key: 'ضریب از کار افتادگی کامل',
				value: `-`
			},
			{
				key: 'ضریب فوت بر اثر حادثه',
				value: `${Number(death).toLocaleString()} تومان`
			},
			{
				key: 'ضریب غرامت امراض خاص',
				value: `${Number(s_diseases).toLocaleString()} تومان`
			}
		]
		return params
	}

	const renderDetails = item => {
		const params = getParams(item)
		const wrapper = (
			<div className="row table m-0">
				{params.map(param => (
					<div
						className="col-sm-6 col-md-4 border py-2"
						key={param.key}
					>
						{param.key} : {param.value}
					</div>
				))}
			</div>
		)
		return wrapper
	}

	return (
		<EnquiryLayout
			title="بیمه عمر"
			filterWrapper={filterWrapper}
			onSubmit={onSubmit}
			onReturn={onReturn}
			isValid={true}
			badges={state.form.badges}
			loading={insurances.loading}
			data={insurances.data}
			properties={properties}
			infoTitle="مشخصات کلی"
			orderPath="/life-order"
			renderDetails={renderDetails}
		>
			<form>
				<div className="input-box py-2 px-lg-5 px-3 mx-md-3 mx-2">
					<span className="input-box__title">اطلاعات</span>
					<div className="row mt-4">
						<div className="col-12 col-md-6  mb-3 mb-md-4">
							<Select
								label="حق بیمه"
								id="insurance-right"
								changed={value =>
									onSetField('insuranceRight', value)
								}
								options={state.insuranceRights}
								value={form.insuranceRight || ''}
								renderer={value => value.name}
							/>
						</div>
						<div className="col-12 col-md-6  mb-3 mb-md-4">
							<Select
								label="نحوه ی پرداخت"
								id="payment method"
								changed={value =>
									onSetField('paymentMethod', value)
								}
								options={state.paymentMethods}
								value={form.paymentMethod || ''}
								renderer={value => value.name}
							/>
						</div>
					</div>
					<div className="row">
						<div className="col-12 col-md-6  mb-3 mb-md-4">
							<Select
								label="طول مدت قرارداد"
								id="contract-time"
								changed={value =>
									onSetField('contractTime', value)
								}
								options={state.contractTimes}
								value={form.contractTime || ''}
								renderer={value => value.name}
							/>
						</div>
						<div className="col-12 col-md-6  mb-3 mb-md-4">
							<Select
								label="سال تولد"
								id="birthday"
								changed={value => onSetField('birthday', value)}
								options={state.years}
								value={form.birthday || ''}
								renderer={renderer}
							/>
						</div>
					</div>
				</div>
			</form>
		</EnquiryLayout>
	)
}

export default withEnquiryGuard(LifeEnquiry, 'life-insurance')
