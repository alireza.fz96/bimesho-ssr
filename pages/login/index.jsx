import React, { useState, useEffect, useRef } from 'react'
import {
	TextField,
	Button,
	IconButton,
	CircularProgress
} from '@material-ui/core'
import classes from '../../styles/Login.module.scss'
import { useForm } from 'react-hook-form'
import { KeyboardArrowRight } from '@material-ui/icons'
import {
	login,
	resendCode,
	signup,
	verifyCode,
	handleLoginErrors,
	handleVerfiyErrors
} from '../../api/userApi'
import { useRouter } from 'next/router'
import { useDispatch } from 'react-redux'
import { login as loginUser } from '../../store/actions/userActions'
import { toEnglishDigit, mobileRegx } from '../../utilities/utilities'

const Login = ({ onLoggedIn }) => {
	const [loginMode, setLoginMode] = useState(true)
	const [request, setRequest] = useState({ loading: false, error: '' })

	const onToggleLoginMode = () => {
		setLoginMode(l => !l)
	}

	const onSubmit = formData => {
		setRequest({ loading: true, error: '' })
		const promise = loginMode ? login : signup
		promise(formData.mobile)
			.then(data => {
				setRequest({ loading: false, error: '' })
				onLoggedIn(formData)
			})
			.catch(error => {
				setRequest({
					loading: false,
					error:
						error.response &&
						handleLoginErrors(error.response.status)
				})
			})
	}

	const { register, errors, handleSubmit, setValue, trigger } = useForm()

	const getErrorMessage = () => {
		if (errors.mobile) {
			const { type } = errors.mobile
			if (type === 'required') return 'لطفا شماره موبایل خود را وارد کنید'
			if (type === 'pattern') return 'شماره موبایل صحیح نمیباشد'
		} else return ''
	}

	const onSetNumberField = (field, value) => {
		const newValue = toEnglishDigit(value)
		setValue(field, newValue)
		if (value.length === 11) trigger(field)
	}

	return (
		<div className={classes.form}>
			<h2 className={classes.form__title}>
				{loginMode ? 'ورود' : 'ثبت نام'}
			</h2>
			<h3 className={classes.form__description}>
				برای {loginMode ? 'ورود' : 'ثبت نام'} شماره تلفن همراه خود را
				وارد کنید
			</h3>
			<form onSubmit={handleSubmit(onSubmit)}>
				<div className="my-5">
					<TextField
						label="شماره موبایل"
						placeholder="مثال: ۰۹۱۰۱۲۳۴۵۶۷"
						variant="outlined"
						name="mobile"
						inputRef={register({
							required: true,
							pattern: mobileRegx
						})}
						error={!!errors.mobile}
						helperText={getErrorMessage()}
						onChange={event =>
							onSetNumberField(
								'mobile',
								toEnglishDigit(event.target.value)
							)
						}
					/>
				</div>
				<div className="text-danger mb-2">{request.error}</div>
				{request.loading ? (
					<CircularProgress />
				) : (
					<Button
						fullWidth
						variant="contained"
						color="primary"
						type="submit"
					>
						ادامه
					</Button>
				)}
			</form>
			<div className="mt-4 input-wrapper">
				<div className={classes.form__resendContainer + ' pt-4'}>
					<span
						style={{ cursor: 'pointer' }}
						onClick={onToggleLoginMode}
					>
						{loginMode ? 'ثبت نام' : 'ورود'}
					</span>
				</div>
			</div>
		</div>
	)
}

const Check = ({ mobile, onBack }) => {
	const { register, errors, handleSubmit, setValue } = useForm()
	const [request, setRequest] = useState({ loading: false, error: '' })
	const dispatch = useDispatch()

	const [counter, setCounter] = useState(60)

	let interval = useRef()

	const onSetCounter = () => {
		if (!interval.current) {
			let i = 59
			setCounter(60)
			interval.current = setInterval(() => {
				if (i < 0) {
					clearInterval(interval.current)
					interval.current = undefined
				} else setCounter(i--)
			}, 1000)
		}
	}

	const onResendCode = () => {
		setRequest({ loading: true, error: '' })
		resendCode(mobile)
			.then(() => {
				onSetCounter()
				setRequest({ loading: false, error: '' })
			})
			.catch(error => {
				setRequest({ loading: false, error: error.response.data.msg })
			})
	}

	const router = useRouter()
	let from = { pathname: router.query.redirect || '/' }

	const onSubmit = formData => {
		setRequest({ loading: true, error: '' })
		verifyCode(mobile, formData.code)
			.then(res => {
				setRequest({ loading: false, error: '' })
				dispatch(loginUser(res.data))
				router.replace(from)
			})
			.catch(error => {
				setRequest({
					loading: false,
					error:
						error.response &&
						handleVerfiyErrors(error.response.status)
				})
			})
	}

	useEffect(() => {
		onSetCounter()
		return () => clearInterval(interval.current)
	}, [])

	return (
		<div className={classes.form}>
			<h2 className={classes.form__title}>تایید کد</h2>
			<h3 className={classes.form__description}>
				کد ارسال شده برای شماره {mobile} را وارد کنید
			</h3>
			<form onSubmit={handleSubmit(onSubmit)}>
				<div className="my-5">
					<TextField
						label="کد ارسال شده"
						placeholder="کد ارسال شده"
						variant="outlined"
						name="code"
						inputRef={register({ required: true })}
						error={!!errors.code}
						onChange={event =>
							setValue('code', toEnglishDigit(event.target.value))
						}
						helperText={
							errors.code
								? 'لطفا کد ارسال شده را وارد نمایید'
								: ''
						}
					/>
				</div>
				<div className="text-danger mb-2">{request.error}</div>
				{request.loading ? (
					<CircularProgress />
				) : (
					<>
						<div className="row align-items-center">
							<div className="col-2">
								<IconButton className="border" onClick={onBack}>
									<KeyboardArrowRight />
								</IconButton>
							</div>
							<div className="col-10">
								<Button
									fullWidth
									variant="contained"
									color="primary"
									type="submit"
								>
									ادامه
								</Button>
							</div>
						</div>
						<div className="mt-4 input-wrapper">
							<div
								className={
									classes.form__resendContainer + ' pt-4'
								}
							>
								{counter > 0 ? (
									<span>
										شما می توانید {counter} ثانیه دیگر
										درخواست ارسال مجدد کد نمایید.
									</span>
								) : (
									<Button
										variant="contained"
										fullWidth
										onClick={onResendCode}
									>
										ارسال مجدد کد
									</Button>
								)}
							</div>
						</div>
					</>
				)}
			</form>
		</div>
	)
}

const Auth = () => {
	const [state, setState] = useState('login')
	const [mobile, setMobile] = useState('')

	const onLogin = ({ mobile }) => {
		setMobile(mobile)
		setState('check')
	}

	const render = () => {
		switch (state) {
			case 'login':
				return <Login onLoggedIn={onLogin} />
			case 'check':
				return (
					<Check mobile={mobile} onBack={() => setState('login')} />
				)
			default:
				return ''
		}
	}

	return (
		<div>
			<div className="container">
				<div className="content-wrapper">{render()}</div>
			</div>
		</div>
	)
}

export default Auth
