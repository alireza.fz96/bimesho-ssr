import { useState, useEffect } from 'react'
import EnquiryLayout from '../../layout/enquiry-layout/EnquiryLayout'
import { useSelector, useDispatch } from 'react-redux'
import Select from '../../components/UI/Select/Select'
import { TextField } from '@material-ui/core'
import { setForm as fireSetForm } from '../../store/actions/fireInsuranceActions'
import FireInsuranceFilter from '../../components/fire-insurance/fire-insurance-filter/FireInsuranceFilter'
import { fetchInsurances, makeRequestBody } from '../../api/fireInsurance'
import {
	fromSeparetedNumber,
	separateNumbers,
	toEnglishDigit
} from '../../utilities/utilities'
import withEnquiryGuard from '../../hoc/withEnquiryGuard'

const FireEnquiry = () => {
	const state = useSelector(state => {
		const { propertyTypes, buildingTypes } = state.coreData
		return {
			propertyTypes,
			buildingTypes,
			form: state.fireInsurance.form
		}
	})
	const dispatch = useDispatch()

	const [form, setForm] = useState(state.form)

	const onSubmit = () => {
		dispatch(
			fireSetForm({
				...form
			})
		)
	}

	const onSetField = (field, value) => {
		setForm({ ...form, [field]: value })
	}

	const [filters, setFilters] = useState({
		earthquake: false,
		pipe: false,
		earth: false,
		rain: false,
		tornado: false,
		flood: false,
		airplane: false,
		steal: false
	})

	const fields = [
		{
			name: 'earthquake',
			value: filters.earthquake,
			label: 'زلزله'
		},
		{
			name: 'pipe',
			value: filters.pipe,
			label: 'ترکیدگی لوله'
		},
		{
			name: 'earth',
			value: filters.earth,
			label: 'نشست زمین'
		},
		{
			name: 'rain',
			value: filters.rain,
			label: 'ضایعات ناشی از برف و باران'
		},
		{
			name: 'tornado',
			value: filters.tornado,
			label: 'طوفان'
		},
		{
			name: 'flood',
			value: filters.flood,
			label: 'سیل'
		},
		{
			name: 'airplane',
			value: filters.airplane,
			label: 'سقوط هواپیما'
		},
		{
			name: 'steal',
			value: filters.steal,
			label: 'سرقت مربوط به شکست حرز'
		}
	]

	const onSetFilter = (name, value) => {
		setFilters({ ...filters, [name]: value })
	}

	const onReturn = () => {
		setForm(state.form)
	}

	const [insurances, setInsurances] = useState({ data: [], loading: false })

	useEffect(() => {
		const getInsurances = async () => {
			const body = makeRequestBody(state.form, filters)
			setInsurances({ data: [], loading: true })
			fetchInsurances(body)
				.then(data => {
					setInsurances({ data: data.data.data, loading: false })
				})
				.catch(error => setInsurances({ data: [], loading: false }))
		}
		getInsurances()
	}, [state.form, filters])

	const filterWrapper = (
		<FireInsuranceFilter changed={onSetFilter} values={fields} />
	)

	const getCovers = () => {
		const covers = []
		for (let field of fields)
			if (filters[field.name]) covers.push(field.label)
		return covers
	}

	const properties = () => {
		const props = [
			{ key: 'نوع ملک', value: state.form.propertyType.name },
			{ key: 'نوع سازه', value: state.form.buildingType.name },
			{ key: 'تعداد واحد ها', value: state.form.unitCount },
			{ key: 'متراژ واحد(ها)', value: state.form.unitMeter },
			{
				key: 'ارزش لوازم خانگی (تومان)',
				value: Number(state.form.value).toLocaleString()
			}
		]
		const covers = getCovers()
		if (covers.length)
			props.push({ key: 'پوشش های اضافی', value: covers.join(' ,') })
		return props
	}

	return (
		<EnquiryLayout
			title="بیمه آتش سوزی"
			filterWrapper={filterWrapper}
			onSubmit={onSubmit}
			onReturn={onReturn}
			isValid={form.unitCount && form.unitMeter && form.value}
			badges={state.form.badges}
			data={insurances.data}
			loading={insurances.loading}
			properties={properties}
			orderPath="/fire-order"
			infoTitle="مشخصات کلی"
		>
			<form>
				<div className="input-box py-2 px-lg-5 px-3 mx-md-3 mx-2">
					<span className="input-box__title">اطلاعات</span>
					<div className="row mt-4">
						<div className="col-12 col-md-6  mb-3 mb-md-4">
							<Select
								label="نوع ملک"
								id="property-type"
								changed={value =>
									onSetField('propertyType', value)
								}
								options={state.propertyTypes}
								value={form.propertyType}
								renderer={value => value.name}
							/>
						</div>
					</div>
					<div className="row">
						<div className="col-12 col-md-6  mb-3 mb-md-4">
							<Select
								label="نوع سازه"
								id="building-type"
								changed={value =>
									onSetField('buildingType', value)
								}
								options={state.buildingTypes}
								value={form.buildingType}
								renderer={value => value.name}
							/>
						</div>
						<div className="col-12 col-md-6  mb-3 mb-md-4">
							<TextField
								label="تعداد واحد"
								placeholder="تعداد واحد"
								value={form.unitCount}
								onChange={event => {
									const newValue = toEnglishDigit(
										event.target.value
									)
									if (+newValue || !newValue)
										onSetField('unitCount', newValue)
								}}
								variant="outlined"
								error={!form.unitCount}
								helperText={
									form.unitCount
										? ''
										: 'لطفا تعداد واحد را مشخص کنید'
								}
							/>
						</div>
					</div>
					<div className="row">
						<div className="col-12 col-md-6  mb-3 mb-md-4">
							<TextField
								label="متراژ واحد"
								placeholder="متراژ واحد"
								value={form.unitMeter}
								onChange={event => {
									const newValue = toEnglishDigit(
										event.target.value
									)
									if (+newValue || !newValue)
										onSetField('unitMeter', newValue)
								}}
								variant="outlined"
								error={!form.unitMeter}
								helperText={
									form.unitMeter
										? ''
										: 'لطفا متراژ واحد را مشخص کنید'
								}
							/>
						</div>
						<div className="col-12 col-md-6  mb-3 mb-md-4">
							<TextField
								label="ارزش لوازم خانگی (تومان)"
								placeholder="ارزش لوازم خانگی (تومان)"
								value={separateNumbers(form.value)}
								onChange={event => {
									const newValue = fromSeparetedNumber(
										event.target.value
									)
									if (+newValue || !newValue)
										onSetField('value', newValue)
								}}
								variant="outlined"
								error={!form.value}
								helperText={
									form.value
										? ''
										: 'لطفا ارزش لوازم خانگی را مشخص کنید'
								}
							/>
						</div>
					</div>
				</div>
			</form>
		</EnquiryLayout>
	)
}

export default withEnquiryGuard(FireEnquiry, 'fire-insurance')
