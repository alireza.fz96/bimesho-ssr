import React, { useState, useEffect } from 'react'
import OrderSteps from '../../components/order-steps/OrderSteps'
import { Button, CircularProgress, TextField } from '@material-ui/core'
import { Alert, Autocomplete } from '@material-ui/lab'
import { useForm, Controller } from 'react-hook-form'
import { useCities, useProvinces } from '../../hooks/locations'
import classes from '../../styles/OrderAddress.module.scss'
import {
	emailRegex,
	mobileRegx,
	phoneRegex,
	postalCodeRegex,
	toEnglishDigit
} from '../../utilities/utilities'
import { useSelector, useDispatch } from 'react-redux'
import { setInvoice, setProductAddress } from '../../store/actions/productActions'
import { logout } from '../../store/actions/userActions'
import DeliveryTime from '../../components/delivery-time/DeliveryTime'
import { useRouter } from 'next/router'
import withAuth from '../../hoc/withAuth'
import { saveInsurance } from '../../api/ThirdpersonInsuranceApi'
import withOrderGuard from '../../hoc/withOrderGuard'

const OrderForm = () => {
	const state = useSelector(state => state.product.address)

	const globalState = useSelector(state => state)

	const {
		name,
		address,
		phone,
		mobile,
		city,
		province,
		postal_code,
		email,
		explain,
		deliveryTime,
		index
	} = state

	const {
		watch,
		register,
		errors,
		handleSubmit,
		setValue,
		control,
		trigger
	} = useForm({
		defaultValues: {
			name,
			address,
			phone,
			mobile,
			city,
			province,
			postal_code,
			email,
			explain
		}
	})

	const [hasPrint, setHasPrint] = useState(!!name)
	const [hasEmail, setHasEmail] = useState(!!email)

	const selectedProvince = hasPrint && watch('province')

	const { provinces } = useProvinces()
	const { cities } = useCities(selectedProvince && selectedProvince.id)

	useEffect(() => {
		if (selectedProvince !== province || !province) {
			setValue('city', null)
		}
	}, [selectedProvince, setValue, province])

	const router = useRouter()
	const dispatch = useDispatch()

	const onSubmit = data => {
		const address = {
			...data,
			index: hasPrint ? delivery.index : 0
		}
		dispatch(setProductAddress(address))
		if (globalState.product.product.insuranceType === 'بیمه شخص ثالث') {
			setPaymentLoading({ error: '', loading: true })
			saveInsurance({
				...globalState,
				address: { ...globalState.address, address }
			})
				.then((res) => {
					dispatch(setInvoice(res.data))
					navigateToPayment()
				})
				.catch(error => {
					if (error.response && error.response.status == '401') {
						dispatch(logout())
						router.replace(
							'/login?redirect=/order-address',
							'/login'
						)
					} else
						setPaymentLoading({
							loading: false,
							error: 'مشکلی پیش آمد. لطفا بعدا تلاش کنید.'
						})
				})
		} else navigateToPayment()
	}

	const navigateToPayment = () => {
		router.push('/payment').finally(() => {
			setPaymentLoading({ error: '', loading: false })
		})
	}

	const getErrorMessage = (error, label) => {
		if (error.type) {
			if (error.type === 'required')
				return `لطفا ${label} خود را وارد کنید`
			if (error.type === 'pattern') return `${label} صحیح نمیباشد`
		}
		return ''
	}

	const onSetNumberField = (value, field, length) => {
		const newValue = toEnglishDigit(value)
		setValue(field, newValue)
		if (value.length === length) trigger(field)
	}

	const [delivery, setDelivery] = React.useState({
		value: deliveryTime,
		index
	})

	const [paymentLoading, setPaymentLoading] = useState({
		loading: false,
		error: ''
	})

	return (
		<div style={{ fontSize: 14 }}>
			<div className="container my-2 px-md-5">
				<OrderSteps step={2} />
			</div>
			<div className="mb-5 container">
				<div className="content-wrapper">
					<div className={classes.address__content}>
						<h2 className="section-title mb-3 text-center">
							اطلاعات ارسال
						</h2>
						<Alert
							variant="outlined"
							severity="info"
							className="my-4"
						>
							طبق دستور بیمه مرکزی از این پس برای دریافت و پرداخت
							خسارت بیمه شخص ثالث نیازی به نسخه کاغذی بیمه‌نامه‌
							نیست و با استفاده از کد یکتای بیمه‌نامه و یا کد ملی
							بیمه‌گذار انجام می‌شود.
						</Alert>
						<div className={classes.address__section}>
							<div className={classes.address__title}>
								ارسال نسخه چاپی بیمه نامه
							</div>
							<div className="py-3 px-1">
								<p>
									آیا مایلید بعد از صدور بیمه نسخه چاپ شده
									بیمه‌نامه‌ برای شما ارسال شود؟
								</p>
								<div
									className={
										'row mt-4 ' + classes.address__input
									}
								>
									<div className="col-xs-12 col-sm-6 col-md-4 mt-2 col-md-6">
										<Button
											variant="contained"
											color={
												hasPrint ? 'primary' : 'default'
											}
											onClick={() => setHasPrint(true)}
											fullWidth
										>
											بله
										</Button>
									</div>
									<div className="col-xs-12 col-sm-6 col-md-4 mt-2 col-md-6">
										<Button
											variant="contained"
											color={
												!hasPrint
													? 'primary'
													: 'default'
											}
											onClick={() => setHasPrint(false)}
											fullWidth
										>
											خیر
										</Button>
									</div>
								</div>
							</div>
						</div>

						{hasPrint && (
							<div className={classes.address__section}>
								<div className={classes.address__title}>
									مشخصات تحویل گیرنده
								</div>
								<div className="py-3 px-1">
									<div
										className={
											classes.address__input + ' mt-3'
										}
									>
										<form>
											<div className="my-3">
												<TextField
													variant="outlined"
													color="primary"
													label="نام و نام خانوادگی"
													placeholder="نام و نام خانوادگی"
													name="name"
													inputRef={register({
														required: true
													})}
													error={!!errors.name}
													helperText={
														errors.name
															? 'لطفا نام و نام خانوادگی را تعیین کنید'
															: ''
													}
												/>
											</div>
											<div className="my-3">
												<TextField
													variant="outlined"
													color="primary"
													label="شماره موبایل"
													placeholder="مثال: ۰۹۱۰۱۲۳۴۵۶۷"
													name="mobile"
													inputRef={register({
														required: true,
														pattern: mobileRegx
													})}
													onChange={event =>
														onSetNumberField(
															event.target.value,
															'mobile',
															11
														)
													}
													error={!!errors.mobile}
													helperText={
														errors.mobile
															? getErrorMessage(
																	errors.mobile,
																	'شماره موبایل'
															  )
															: ''
													}
												/>
											</div>
											<div className="my-3">
												<TextField
													variant="outlined"
													color="primary"
													label="تلفن ثابت"
													placeholder="مثال:‌ ۰۲۱۹۱۰۰۲۰۰۸"
													name="phone"
													inputRef={register({
														required: true,
														pattern: phoneRegex
													})}
													error={!!errors.phone}
													onChange={event =>
														onSetNumberField(
															event.target.value,
															'phone',
															11
														)
													}
													helperText={
														errors.phone
															? getErrorMessage(
																	errors.phone,
																	'تلفن ثابت'
															  )
															: ''
													}
												/>
											</div>
											<div className="my-3">
												<TextField
													variant="outlined"
													color="primary"
													label="کد پستی"
													onChange={event =>
														onSetNumberField(
															event.target.value,
															'postal_code',
															10
														)
													}
													placeholder="کدپستی (۱۰ رقم)"
													name="postal_code"
													inputRef={register({
														required: true,
														pattern: postalCodeRegex
													})}
													error={!!errors.postal_code}
													helperText={
														errors.postal_code
															? getErrorMessage(
																	errors.postal_code,
																	'کد پستی'
															  )
															: ''
													}
												/>
											</div>
											<div className="row">
												<div className="col-xs-12 col-sm-6 col-md-4 mt-2 col-md-6">
													<Controller
														render={props => (
															<Autocomplete
																options={
																	provinces
																}
																getOptionLabel={option =>
																	option.name ||
																	''
																}
																closeIcon={
																	false
																}
																renderInput={params => (
																	<TextField
																		{...params}
																		label="استان"
																		variant="outlined"
																		error={
																			!!errors.province
																		}
																		helperText={
																			errors.province
																				? 'لطفا استان را انتخاب کنید'
																				: ''
																		}
																	/>
																)}
																defaultValue={
																	province
																}
																onChange={(
																	_,
																	data
																) =>
																	props.onChange(
																		data
																	)
																}
															/>
														)}
														name="province"
														control={control}
														rules={{
															required: true
														}}
													/>
												</div>
												<div className="col-xs-12 col-sm-6 col-md-4 mt-2 col-md-6">
													<Controller
														render={props => (
															<Autocomplete
																options={cities}
																getOptionLabel={option =>
																	option.name ||
																	''
																}
																closeIcon={
																	false
																}
																disabled={
																	!selectedProvince
																}
																value={
																	props.value
																}
																renderInput={params => (
																	<TextField
																		{...params}
																		label="شهر"
																		variant="outlined"
																		error={
																			!!errors.city
																		}
																		helperText={
																			errors.city
																				? 'لطفا شهر را انتخاب کنید'
																				: ''
																		}
																	/>
																)}
																defaultValue={
																	city
																}
																onChange={(
																	_,
																	data
																) =>
																	props.onChange(
																		data
																	)
																}
															/>
														)}
														name="city"
														control={control}
														rules={{
															required: true
														}}
													/>
												</div>
											</div>
											<div className="my-3">
												<TextField
													multiline
													variant="outlined"
													color="primary"
													label="آدرس دقیق"
													placeholder="آدرس دقیق"
													name="address"
													inputRef={register({
														required: true
													})}
													error={!!errors.address}
													helperText={
														errors.address
															? 'لطفا ادرس را تعیین کنید'
															: ''
													}
												/>
											</div>
										</form>
									</div>
								</div>
							</div>
						)}
						{hasPrint && (
							<div className={classes.address__section}>
								<div className={classes.address__title}>
									زمان تحویل
								</div>
								<div className="py-3 px-1">
									<p className="mb-3">
										روز و زمان تحویل بیمه‌نامه خود را انتخاب
										کنید.
									</p>
									<div className={classes.times}>
										<DeliveryTime
											control={control}
											initialValue={delivery.value}
											initialIndex={delivery.index}
											changed={setDelivery}
											error={errors.deliveryTime}
										/>
									</div>
									<div className="input-wrapper mt-4">
										<Alert
											variant="outlined"
											severity="success"
											className="text-center"
										>
											بیمه نامه شما امروز صادر میشود
										</Alert>
									</div>
								</div>
							</div>
						)}
						<div className={classes.address__section}>
							<div className={classes.address__title}>ایمیل</div>
							<div className="py-3 px-1">
								<p>
									آیا مایلید بعد از صدور بیمه، تصویر
									بیمه‌نامه‌ خود را از طریق ایمیل دریافت کنید؟
								</p>
								<div className={classes.address__input}>
									<div className="row mt-4 ">
										<div className="col-xs-12 col-sm-6 col-md-4 mt-2 col-md-6">
											<Button
												variant="contained"
												color={
													hasEmail
														? 'primary'
														: 'default'
												}
												onClick={() =>
													setHasEmail(true)
												}
												fullWidth
											>
												بله
											</Button>
										</div>
										<div className="col-xs-12 col-sm-6 col-md-4 mt-2 col-md-6">
											<Button
												variant="contained"
												color={
													!hasEmail
														? 'primary'
														: 'default'
												}
												onClick={() =>
													setHasEmail(false)
												}
												fullWidth
											>
												خیر
											</Button>
										</div>
									</div>
									{hasEmail && (
										<div className="mt-4">
											<TextField
												variant="outlined"
												label="ایمیل"
												placeholder="مثال:‌ test@gmail.com"
												inputRef={register({
													required: true,
													pattern: emailRegex
												})}
												name="email"
												error={errors.email}
												helperText={
													errors.email
														? getErrorMessage(
																errors.email,
																'ایمیل'
														  )
														: ''
												}
											/>
										</div>
									)}
								</div>
							</div>
						</div>
						<div className={classes.address__section}>
							<div className={classes.address__title}>
								توضیحات
							</div>
							<div className="py-3 px-1">
								<p>
									چنانچه توضیح اضافه‌ای دارید اینجا قید
									نمایید.
								</p>
								<div
									className={classes.address__input + ' mt-3'}
								>
									<TextField
										multiline
										placeholder="توضیحات"
										label="توضیحات"
										variant="outlined"
										inputRef={register}
										name="explain"
									/>
								</div>
							</div>
						</div>
					</div>

					<div className="text-center mt-3">
						{paymentLoading.loading ? (
							<div className="w-100 text-center">
								<CircularProgress />
							</div>
						) : (
							<Button
								color="primary"
								variant="contained"
								onClick={handleSubmit(onSubmit)}
							>
								تایید و ادامه
							</Button>
						)}
						<div
							className="text-center mt-3"
							style={{ color: 'red' }}
						>
							{paymentLoading.error}
						</div>
					</div>
				</div>
			</div>
		</div>
	)
}

export default withAuth(withOrderGuard(OrderForm))
