import { useState, useEffect } from 'react'
import EnquiryLayout from '../../layout/enquiry-layout/EnquiryLayout'
import { useSelector, useDispatch } from 'react-redux'
import Select from '../../components/UI/Select/Select'
import { Autocomplete } from '@material-ui/lab'
import { TextField } from '@material-ui/core'
import { setForm as bodySetForm } from '../../store/actions/bodyInsuranceActions'
import BodyInsuranceFilter from '../../components/body-insurance/body-insurance-filter/BodyInsuranceFilter'
import { fetchInsurances, makeRequestBody } from '../../api/BodyInsuranceApi'
import { useBrands, useModels } from '../../hooks/vehicles'
import { fromSeparetedNumber, separateNumbers } from '../../utilities/utilities'
import withEnquiryGuard from '../../hoc/withEnquiryGuard'

const BodyEnquiry = () => {
	const state = useSelector(state => {
		const {
			vehicleUsages,
			years,
			additionalCoverDiscounts,
			mainCoverDiscounts
		} = state.coreData
		return {
			years,
			additionalCoverDiscounts,
			vehicleUsages,
			mainCoverDiscounts,
			form: state.bodyInsurance.form
		}
	})
	const dispatch = useDispatch()

	const [form, setForm] = useState(state.form)

	const { brands } = useBrands(form.category.id)
	const { models } = useModels(form.brand && form.brand.id)

	useEffect(() => {
		if (form.brand && form.brand.name !== state.form.brand.name) {
			setForm(form => ({ ...form, model: null }))
		}
	}, [form.brand, state.form])

	const [insurances, setInsurances] = useState({ data: [], loading: false })

	const [filters, setFilters] = useState({
		chemical: false,
		break_glass: false,
		natural_disaster: false,
		steal: false,
		transit: false
	})

	const fields = [
		{
			name: 'chemical',
			value: filters.chemical,
			label: 'رنگ، اسید، مواد شیمیایی'
		},
		{ name: 'break_glass', value: filters.break_glass, label: 'شکست شیشه' },
		{
			name: 'natural_disaster',
			value: filters.natural_disaster,
			label: 'سیل، زلزله و بلایای طبیعی'
		},
		{ name: 'steal', value: filters.steal, label: 'سرقت کلیه قطعات خودرو' },
		{ name: 'transit', value: filters.transit, label: 'خروج از کشور' }
	]

	useEffect(() => {
		const getInsurances = async () => {
			const body = makeRequestBody(state.form, filters)
			setInsurances({ data: [], loading: true })
			fetchInsurances(body)
				.then(data => {
					setInsurances({
						data: Object.values(data.data.data),
						loading: false
					})
				})
				.catch(error => setInsurances({ data: [], loading: false }))
		}
		getInsurances()
	}, [state.form, filters])

	const onSubmit = () => {
		dispatch(
			bodySetForm({
				...form,
				badges: [
					form.brand.name,
					form.model.name,
					form.productionYear.name
				]
			})
		)
	}

	const onSetField = (field, value) => {
		setForm({ ...form, [field]: value })
	}

	const yearRenderer = year => (
		<>
			<span className="year miladi">{year.miladi}</span>|
			<span className="year">{year.jalali}</span>
		</>
	)

	const onSetFilter = (name, value) => {
		setFilters({ ...filters, [name]: value })
	}

	const filterWrapper = (
		<BodyInsuranceFilter changed={onSetFilter} values={fields} />
	)

	const onReturn = () => {
		setForm(state.form)
	}

	const getCovers = () => {
		const covers = []
		for (let field of fields)
			if (filters[field.name]) covers.push(field.label)
		return covers
	}

	const properties = () => {
		const props = [
			{ key: 'گروه خودرو', value: state.form.category.name },
			{ key: 'نوع خودرو', value: state.form.brand.name },
			{ key: 'مدل خودرو', value: state.form.model.name },
			{ key: 'کاربری خودرو', value: state.form.vehicleUsage.name },
			{
				key: 'ارزش خودرو',
				value: Number(state.form.vehicleValue).toLocaleString()
			}
		]
		const covers = getCovers()
		if (covers.length)
			props.push({ key: 'پوشش های اضافی', value: covers.join(' ,') })
		return props
	}

	const isValidPrice =
		form.vehicleValue > 1000000 && form.vehicleValue < 9000000000

	return (
		<EnquiryLayout
			title="بیمه بدنه"
			filterWrapper={filterWrapper}
			onSubmit={onSubmit}
			isValid={form.model && isValidPrice}
			badges={state.form.badges}
			onReturn={onReturn}
			data={insurances.data}
			loading={insurances.loading}
			properties={properties}
			orderPath="/body-order"
			infoTitle="مشخصات اتومبیل"
		>
			<form>
				<div className="input-box py-2 px-lg-5 px-3 mx-md-3 mx-2">
					<span className="input-box__title">اطلاعات خودرو</span>
					<div className="row mt-4">
						<div className="col-12 col-md-6  mb-3 mb-md-4">
							<Autocomplete
								options={brands || []}
								getOptionLabel={option => option.name || ''}
								value={form.brand}
								closeIcon={false}
								onChange={(_, value) =>
									onSetField('brand', value)
								}
								renderInput={params => (
									<TextField
										{...params}
										label="برند"
										variant="outlined"
									/>
								)}
							/>
						</div>
						<div className="col-12 col-md-6  mb-3 mb-md-4">
							<Autocomplete
								options={models || []}
								getOptionLabel={option => option.name || ''}
								value={form.model}
								closeIcon={false}
								onChange={(_, value) =>
									onSetField('model', value)
								}
								disabled={!form.brand}
								renderInput={params => (
									<TextField
										{...params}
										label="مدل"
										variant="outlined"
										autoFocus
										required
										error={!form.model}
										helperText={
											!form.model
												? 'لطفا مدل را مشخص کنید'
												: ''
										}
									/>
								)}
							/>
						</div>
					</div>
					<div className="row">
						<div className="col-12 col-md-6  mb-3 mb-md-4">
							<Select
								label="سال ساخت وسیله نقلیه"
								id="production-year"
								changed={value =>
									onSetField('productionYear', value)
								}
								options={state.years}
								value={form.productionYear}
								renderer={yearRenderer}
							/>
						</div>
						<div className="col-12 col-md-6  mb-3 mb-md-4">
							<Select
								label="اصالت خودرو"
								id="domestic-production"
								changed={value =>
									onSetField(
										'domesticProduction',
										value.value
									)
								}
								options={domesticProductionOptions}
								value={domesticProductionOptions.find(
									o => o.value === form.domesticProduction
								)}
								renderer={option => option.name}
							/>
						</div>
					</div>
					<div className="row">
						<div className="col-12 col-md-6  mb-3 mb-md-4">
							<Select
								label="خودرو صفر کیلومتر"
								id="brand-new-car"
								changed={value =>
									onSetField('isBrandNew', value)
								}
								options={isNewOptions}
								value={form.isBrandNew}
								renderer={option => option.name}
							/>
						</div>
					</div>
					<div className="row">
						<div className="col-12 col-md-6  mb-3 mb-md-4">
							<Select
								changed={value =>
									onSetField('additionalCoverDiscount', value)
								}
								id="additional-cover-discount-label"
								value={form.additionalCoverDiscount}
								renderer={value => value.name}
								options={state.additionalCoverDiscounts}
								label="تخفیف عدم خسارت پوشش های اضافی"
							/>
						</div>
						<div className="col-12 col-md-6  mb-3 mb-md-4">
							<Select
								changed={value =>
									onSetField('mainCoverDiscount', value)
								}
								id="main-cover-discount-label"
								label="تخفیف عدم خسارت پوشش های اصلی"
								value={form.mainCoverDiscount}
								renderer={value => value.name}
								options={state.mainCoverDiscounts}
							/>
						</div>
					</div>
					<div className="row">
						<div className="col-12 col-md-6  mb-3 mb-md-4">
							<Select
								id="vehicle-usage-label"
								label="کاربری خودرو"
								changed={value =>
									onSetField('vehicleUsage', value)
								}
								renderer={value => value.name}
								value={form.vehicleUsage}
								options={state.vehicleUsages}
							/>
						</div>
						<div className="col-12 col-md-6  mb-3 mb-md-4">
							<TextField
								variant="outlined"
								color="primary"
								value={separateNumbers(form.vehicleValue) || ''}
								onChange={event => {
									const newValue = fromSeparetedNumber(
										event.target.value
									)
									if (+newValue || !newValue)
										onSetField('vehicleValue', newValue)
								}}
								label="ارزش خودرو (تومان)"
								placeholder="ارزش خودرو (تومان)"
								error={!isValidPrice}
								helperText={
									isValidPrice
										? ''
										: 'ارزش خودرو باید بین ۱،۰۰۰،۰۰۰ و ۹،۰۰۰،۰۰۰،۰۰۰ باشد'
								}
							/>
						</div>
					</div>
				</div>
			</form>
		</EnquiryLayout>
	)
}

const isNewOptions = [
	{ name: 'بله', value: true },
	{ name: 'خیر', value: false }
]

const domesticProductionOptions = [
	{ name: 'تولید داخل', value: true },
	{ name: 'تولید خارج', value: false }
]

export default withEnquiryGuard(BodyEnquiry)
