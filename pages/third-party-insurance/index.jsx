import InsuranceStepsLayout from '../../layout/insurance-steps-layout/InsuranceStepsLayout'
import VehicleTypes from '../../components/vehicle-types/VehicleTypes'
import VehicleBrands from '../../components/vehicle-brands/VehicleBrands'
import VehicleModels from '../../components/vehicle-models/VehicleModels'
import ProductionYear from '../../components/production-year/ProductionYear'
import ThirdPartyInsuranceFaq from '../../components/third-party-insurance/faq/ThirdPartyInsuranceFaq'
import { useSelector, useDispatch } from 'react-redux'
import * as actions from '../../store/actions/thirdPartyInsuranceActions'
import SwipeableViews from 'react-swipeable-views'
import InsuranceDiscount from '../../components/insurance-discount/InsuranceDiscount'
import InsuranceValidationTime from '../../components/insurance-validation-time/InsuranceValidationTime'
import AccidentHistory from '../../components/third-party-insurance/accident-history/AccidentHistory'
import { initial } from '../../store/actions/thirdPartyInsuranceActions'
import InsuranceHistory from '../../components/third-party-insurance/insurance-history/InsuranceHistory'

const ThirdPartyInsurance = () => {
    const state = useSelector(state => state.thirdPartyInsurance)
    const dispatch = useDispatch()

    const nextSlide = () => {
        dispatch(actions.nextSlide())
    }

    const prevSlide = () => {
        dispatch(actions.prevSlide())
    }

    React.useEffect(() => {
        dispatch(initial())
    }, [dispatch])

    const { step } = state

    return (
        <div>
            <InsuranceStepsLayout
                {...step}
                title="بیمه شخص ثالث"
                next={nextSlide}
                prev={prevSlide}
                canReturn={step.index > 0}
                badges={state.form.badges}
            >
                <SwipeableViews index={step.index} axis="x-reverse" disabled>
                    <div className="content__step">
                        <VehicleTypes />
                    </div>
                    <div className="content__step">
                        <VehicleBrands />
                    </div>
                    <div className="content__step">
                        <VehicleModels />
                    </div>
                    <div className="content__step">
                        <ProductionYear />
                    </div>
                    <div className="content__step">
                        <InsuranceHistory />
                    </div>
                    <div className="content__step">
                        <InsuranceValidationTime />
                    </div>
                    <div className="content__step">
                        <InsuranceDiscount />
                    </div>
                    <div className="content__step">
                        <AccidentHistory />
                    </div>
                </SwipeableViews>
            </InsuranceStepsLayout>
            <ThirdPartyInsuranceFaq />
        </div>
    )
}

export default ThirdPartyInsurance
