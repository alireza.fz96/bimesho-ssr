import classes from '../styles/404.module.scss'
import { Button } from '@material-ui/core'
import Link from 'next/link'

export default function Custom404() {
	return (
		<div className="container">
			<div className={classes.pageContainer}>
				<div className="row">
					<div className="col-12">
						<div className={classes.errorBox}>
							<div>
								<h1 className={classes.errorBox__code}>404</h1>
								<h2 className={classes.errorBox__title}>
									صفحه‌ای که دنبال آن بودید پیدا نشد!
								</h2>
								<div className={classes.errorBox__description}>
									ممکن است آدرس اشتباهی وارد کرده باشید
								</div>
							</div>
						</div>
						<div className={classes.btnBox}>
							<Link href="/">
								<Button color="primary" variant="contained">
									بازگشت به خانه
								</Button>
							</Link>
						</div>
					</div>
				</div>
			</div>
		</div>
	)
}
