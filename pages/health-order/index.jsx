import React, { useState } from 'react'
import OrderForm from '../../components/order-form/OrderForm'
import { useSelector } from 'react-redux'
import withInsuranceGuard from '../../hoc/withInsuranceGuard'
import withAuth from '../../hoc/withAuth'

const HealthOrder = () => {
	const form = useSelector(state => state.product.form)

	const [meliImage, setMeliImage] = useState(form.card_meli || '')
	const [shenasnameImage, setShenasnameImage] = useState(
		form.shenasname || ''
	)
	const [healthFormImage, setHealthFormImage] = useState(
		form.health_form || ''
	)
	const [baseInsuranceImage, setBaseInsuranceImage] = useState(
		form.base_insurance || ''
	)

	const photos = [
		{
			name: 'card_meli',
			label: 'تصویر کارت ملی',
			image: meliImage,
			setImage: setMeliImage,
			required: true
		},
		{
			name: 'shenasname',
			label: 'تصویر صفحه اول شناسنامه',
			image: shenasnameImage,
			setImage: setShenasnameImage,
			required: true
		},
		{
			name: 'health_form',
			label: 'اسکن فرم پرسشنامه سلامت',
			image: healthFormImage,
			setImage: setHealthFormImage,
			required: true
		},
		{
			name: 'base_insurance',
			label: 'عکس دفترچه بیمه گر پایه',
			image: baseInsuranceImage,
			setImage: setBaseInsuranceImage,
			required: false
		}
	]

	return <OrderForm photos={photos} />
}

export default withAuth(withInsuranceGuard(HealthOrder, 'health-insurance'))
