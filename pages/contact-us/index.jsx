import React from 'react'
import { Button, TextField } from '@material-ui/core'
import { useForm } from 'react-hook-form'
import classes from '../../styles/ContactUs.module.scss'

const ContactUs = () => {
	const { register, errors, handleSubmit } = useForm()

	const onSubmit = data => {
		alert(JSON.stringify(data))
	}

	return (
		<div className="container">
			<div className={'content-wrapper ' + classes.contactUs}>
				<h1>تماس با بیمه شو</h1>
				<p>
					مشتری گرامی برای پیگیری یا سؤال درباره سفارش خود، از فرم زیر
					استفاده کنید و یا به آدرس ایمیل زیر ارسال کنید. برای تسریع
					در پاسخ گویی شماره سفارش (پیگیری) را در ایمیل خود ذکر کنید.
				</p>
				<p>
					آدرس:‌ خیابان ولیعصر پایین تر از خیابان فاطمی خیابان پزشک
					پور پلاک 8
				</p>
				<p>
					اطلاعات تماس: <a href="tel:021-91002008">021-91002008</a> ،
					<a href="tel:021-91002008">  021-91002008</a>
				</p>
				<p>
					پست الکترونیکی:‌
					<a href="mailto:info@BimeSho.com">info@BimeSho.com</a>
				</p>
				<h2 className="mt-4">فرم تماس</h2>
				<form onSubmit={handleSubmit(onSubmit)}>
					<div className="row mt-3">
						<div className="col-12 col-md-6 mb-4">
							<TextField
								placeholder="نام"
								label="نام"
								name="name"
								inputRef={register({ required: true })}
								error={errors.name}
								helperText={
									errors.name ? 'لطفا نام را وارد کنید' : ''
								}
							/>
						</div>
						<div className="col-12 col-md-6 mb-4">
							<TextField
								placeholder="نام خانوادگی"
								label="نام خانوادگی"
								name="lastname"
								inputRef={register({ required: true })}
								error={errors.lastname}
								helperText={
									errors.lastname
										? 'لطفا نام خانوادگی را وارد کنید'
										: ''
								}
							/>
						</div>
					</div>
					<div className="row">
						<div className="col-12 col-md-6 mb-4">
							<TextField
								placeholder="تلفن ثابت"
								label="تلفن ثابت"
								name="phone"
								inputRef={register({ required: true })}
								error={errors.phone}
								helperText={
									errors.phone
										? 'لطفا تلفن ثابت را وارد کنید'
										: ''
								}
							/>
						</div>
						<div className="col-12 col-md-6 mb-4">
							<TextField
								placeholder="ایمیل"
								label="ایمیل"
								name="email"
								inputRef={register({ required: true })}
								error={errors.email}
								helperText={
									errors.email
										? 'لطفا ایمیل را وارد کنید'
										: ''
								}
							/>
						</div>
					</div>
					<div className="mb-4">
						<TextField
							placeholder="متن پیام"
							label="متن پیام"
							name="message"
							inputRef={register({ required: true })}
							error={errors.message}
							helperText={
								errors.message
									? 'لطفا پیام خود را وارد کنید'
									: ''
							}
						/>
					</div>
					<Button color="primary" variant="contained" type="submit">
						ثبت و ارسال فرم
					</Button>
				</form>
			</div>
		</div>
	)
}

export default ContactUs
