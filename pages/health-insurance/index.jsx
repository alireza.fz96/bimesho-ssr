import React from 'react'
import InsuranceStepsLayout from '../../layout/insurance-steps-layout/InsuranceStepsLayout'
import HealthInsuranceFaq from '../../components/health-insurance/faq/HealthInsuranceFaq'
import { useSelector, useDispatch } from 'react-redux'
import * as actions from '../../store/actions/healthInsuranceActions'
import SwipeableViews from 'react-swipeable-views'
import BaseInsurance from '../../components/health-insurance/base-insurance/BaseInsurance'
import Applicants from '../../components/health-insurance/applicants/Applicants'

const HealthInsurance = () => {
	const state = useSelector(state => state.healthInsurance)
	const dispatch = useDispatch()

	const nextSlide = () => {
		dispatch(actions.nextSlide())
	}

	const prevSlide = () => {
		dispatch(actions.prevSlide())
	}

	React.useEffect(() => {
		dispatch(actions.reset())
	}, [dispatch])

	const { step } = state

	return (
		<div>
			<InsuranceStepsLayout
				{...step}
				title="بیمه درمان تکمیلی "
				next={nextSlide}
				prev={prevSlide}
				canReturn={step.index > 0}
				badges={state.form.badges}
			>
				<SwipeableViews index={step.index} axis="x-reverse">
					<div className="content__step">
						<BaseInsurance />
					</div>
					<div className="content__step">
						<Applicants />
					</div>
				</SwipeableViews>
			</InsuranceStepsLayout>
			<HealthInsuranceFaq />
		</div>
	)
}

export default HealthInsurance
