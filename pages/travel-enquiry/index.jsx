import React, { useState, useEffect } from 'react'
import EnquiryLayout from '../../layout/enquiry-layout/EnquiryLayout'
import { useSelector, useDispatch } from 'react-redux'
import Select from '../../components/UI/Select/Select'
import { TextField } from '@material-ui/core'
import { setForm as travelSetForm } from '../../store/actions/travelInsuranceActions'
import { Autocomplete } from '@material-ui/lab'
import { fetchInsurances, makeRequestBody } from '../../api/travelInsuranceApi'
import { useCountries } from '../../hooks/countries'
import withEnquiryGuard from '../../hoc/withEnquiryGuard'

const TravelEnquiry = () => {
	const state = useSelector(state => {
		const { ages, stayingTimes } = state.coreData
		return {
			ages,
			stayingTimes,
			form: state.travelInsurance.form
		}
	})
	const dispatch = useDispatch()

	const [form, setForm] = useState(state.form)

	const onSubmit = () => {
		dispatch(
			travelSetForm({
				...form
			})
		)
	}

	const [insurances, setInsurances] = useState({ data: [], loading: false })

	useEffect(() => {
		const getInsurances = async () => {
			const body = makeRequestBody(state.form)
			setInsurances({ data: [], loading: true })
			fetchInsurances(body)
				.then(data => {
					setInsurances({ data: data.data.data, loading: false })
				})
				.catch(error => setInsurances({ data: [], loading: false }))
		}
		getInsurances()
	}, [state.form])

	const onSetField = (field, value) => {
		setForm({ ...form, [field]: value })
	}

	const onReturn = () => {
		setForm(state.form)
	}

	const filterWrapper = (
		<div className="image-cover">
			<img src="/imgs/svg/travel-icon.svg" alt="travel_image" />
		</div>
	)

	const properties = () => [
		{ key: 'مقصد', value: state.form.country.title },
		{ key: 'مدت اقامت', value: state.form.stayingTime.name },
		{ key: 'سن مسافر', value: state.form.age.name }
	]

	const { countries, isLoading } = useCountries()

	return (
		<EnquiryLayout
			title="بیمه مسافرتی"
			filterWrapper={filterWrapper}
			onSubmit={onSubmit}
			onReturn={onReturn}
			isValid={true}
			badges={state.form.badges}
			loading={insurances.loading}
			data={insurances.data}
			properties={properties}
			orderPath="/travel-order"
			infoTitle="مشخصات کلی"
		>
			<form>
				<div className="input-box py-2 px-lg-5 px-3 mx-md-3 mx-2">
					<span className="input-box__title">اطلاعات</span>
					<div className="row mt-4">
						<div className="col-12 col-md-6  mb-3 mb-md-4">
							<Autocomplete
								options={countries}
								getOptionLabel={option => option.title || ''}
								value={form.country}
								closeIcon={false}
								onChange={(_, value) =>
									onSetField('country', value)
								}
								renderInput={params => (
									<TextField
										{...params}
										label="مقصد"
										variant="outlined"
										autoFocus
									/>
								)}
							/>
						</div>
					</div>
					<div className="row">
						<div className="col-12 col-md-6  mb-3 mb-md-4">
							<Select
								label="مدت اقامت"
								id="staying-time"
								changed={value =>
									onSetField('stayingTime', value)
								}
								options={state.stayingTimes}
								value={form.stayingTime || ''}
								renderer={value => value.name}
							/>
						</div>
						<div className="col-12 col-md-6  mb-3 mb-md-4">
							<Select
								label="سن مسافر"
								id="passenger-age"
								changed={value => onSetField('age', value)}
								options={state.ages}
								value={form.age}
								renderer={option => option.name}
							/>
						</div>
					</div>
				</div>
			</form>
		</EnquiryLayout>
	)
}

export default withEnquiryGuard(TravelEnquiry, 'travel-insurance')
