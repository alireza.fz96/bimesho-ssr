import React from 'react'
import InsuranceStepsLayout from '../../layout/insurance-steps-layout/InsuranceStepsLayout'
import VehicleBrands from '../../components/vehicle-brands/VehicleBrands'
import VehicleModels from '../../components/vehicle-models/VehicleModels'
import ProductionYear from '../../components/production-year/ProductionYear'
import ThirdPartyInsuranceFaq from '../../components/third-party-insurance/faq/ThirdPartyInsuranceFaq'
import { useSelector, useDispatch } from 'react-redux'
import * as actions from '../../store/actions/motorInsuranceActions'
import SwipeableViews from 'react-swipeable-views'
import InsuranceDiscount from '../../components/insurance-discount/InsuranceDiscount'
import InsuranceValidationTime from '../../components/insurance-validation-time/InsuranceValidationTime'
import AccidentHistory from '../../components/third-party-insurance/accident-history/AccidentHistory'
import InsuranceHistory from '../../components/third-party-insurance/insurance-history/InsuranceHistory'

const MotorThirdPartyInsurance = () => {
	const state = useSelector(state => state.motorInsurance)
	const dispatch = useDispatch()

	const nextSlide = () => {
		dispatch(actions.nextSlide())
	}

	const prevSlide = () => {
		dispatch(actions.prevSlide())
	}

	React.useEffect(() => {
		dispatch(actions.initial(true))
	}, [dispatch])

	const { step } = state

	return (
		<div>
			<InsuranceStepsLayout
				{...step}
				title="بیمه موتور"
				next={nextSlide}
				prev={prevSlide}
				canReturn={step.index > 0}
				badges={state.form.badges}
			>
				<SwipeableViews index={step.index} axis="x-reverse" disabled>
					<div className="content__step">
						<VehicleBrands motor />
					</div>
					<div className="content__step">
						<VehicleModels motor />
					</div>
					<div className="content__step">
						<ProductionYear motor />
					</div>
					<div className="content__step">
						<InsuranceHistory motor />
					</div>
					<div className="content__step">
						<InsuranceValidationTime motor />
					</div>
					<div className="content__step">
						<InsuranceDiscount motor />
					</div>
					<div className="content__step">
						<AccidentHistory motor />
					</div>
				</SwipeableViews>
			</InsuranceStepsLayout>
			<ThirdPartyInsuranceFaq />
		</div>
	)
}

export default MotorThirdPartyInsurance
