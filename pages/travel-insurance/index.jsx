import React from 'react'
import InsuranceStepsLayout from '../../layout/insurance-steps-layout/InsuranceStepsLayout'
import { useSelector, useDispatch } from 'react-redux'
import * as actions from '../../store/actions/travelInsuranceActions'
import SwipeableViews from 'react-swipeable-views'
import Destination from '../../components/travel-insurance/destination/Destination'
import PassengerAge from '../../components/travel-insurance/passenger-age/PassengerAge'
import TravelInsuranceFaq from '../../components/travel-insurance/faq/TravelInsuranceFaq'

const TravelInsurance = () => {
	const state = useSelector(state => state.travelInsurance)
	const dispatch = useDispatch()

	const nextSlide = () => {
		dispatch(actions.nextSlide())
	}

	const prevSlide = () => {
		dispatch(actions.prevSlide())
	}

	React.useEffect(() => {
		dispatch(actions.reset())
	}, [dispatch])

	const { step } = state

	return (
		<div>
			<InsuranceStepsLayout
				{...step}
				title="بیمه مسافرتی "
				next={nextSlide}
				prev={prevSlide}
				canReturn={step.index > 0}
				badges={state.form.badges}
			>
				<SwipeableViews index={step.index} axis="x-reverse">
					<div className="content__step">
						<Destination />
					</div>
					<div className="content__step">
						<PassengerAge />
					</div>
				</SwipeableViews>
			</InsuranceStepsLayout>
			<TravelInsuranceFaq />
		</div>
	)
}

export default TravelInsurance
