import useSWR from 'swr'
import { poster } from '../api/axios-base'

export function useThirdInsurances(body, initialData) {
	const params = Object.values(body).map(
		param => param && (param.value || param)
	)
	const { data, error } = useSWR(
		[`/insurance/online/request/thirdperson`, ...params],
		url => poster(url, body),
		// { initialData }
	)
	return {
		insurances: data || [],
		isLoading: !error && !data,
		isError: error
	}
}
