import useSWR from 'swr'
import { poster } from '../api/axios-base'

export function useProvinces() {
	const { data, error } = useSWR('/request/get-province', poster)
	return {
		provinces: data || [],
		isLoading: !error && !data,
		isError: error
	}
}

export function useCities(provinceId) {
	const { data, error } = useSWR(
		provinceId ? '/request/get-city?id=' + provinceId : null,
		poster
	)
	return {
		cities: data || [],
		isLoading: !error && !data,
		isError: error
	}
}
