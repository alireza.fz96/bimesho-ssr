import useSWR from 'swr'
import { fetcher } from '../api/axios-base'

export function useCountries() {
	const { data, error } = useSWR(`/insurance/country`, fetcher)
	return {
		countries: data || [],
		isLoading: !error && !data,
		isError: error
	}
}