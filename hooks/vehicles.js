import useSWR from 'swr'
import { fetcher } from '../api/axios-base'

export function useCategories() {
	const { data, error } = useSWR(`/request/get-cat-cars`, fetcher)
	return {
		categories: data || [],
		isLoading: !error && !data,
		isError: error
	}
}

export function useBrands(categoryId) {
	const { data, error } = useSWR(
		categoryId ? '/request/get-kind-cars/' + categoryId : null,
		fetcher
	)
	return {
		brands: data || [],
		isLoading: !error && !data,
		isError: error
	}
}

export function useModels(brandId) {
	const { data, error } = useSWR(
		brandId ? '/request/get-model-cars/' + brandId : null,
		fetcher
	)
	return {
		models: data || [],
		isLoading: !error && !data,
		isError: error
	}
}
