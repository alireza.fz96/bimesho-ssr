import React, { useState, useEffect } from 'react'
import { useSelector } from 'react-redux'
import { useRouter } from 'next/router'

const withOrderGuard = WrappedComponent => {
	return () => {
		const [isLoading, setLoading] = useState(true)
		const state = useSelector(state => state)
		const router = useRouter()

		useEffect(() => {
			if (state._persist.rehydrated) {
				const canVisit =
					state.product.product && state.product.form.name
				if (!canVisit) router.replace('/')
				else setLoading(false)
			}
		}, [setLoading, state._persist])

		return !isLoading && <WrappedComponent />
	}
}

export default withOrderGuard
