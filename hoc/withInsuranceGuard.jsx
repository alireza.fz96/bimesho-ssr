import { useState, useEffect, useCallback } from 'react'
import { useSelector } from 'react-redux'
import { useRouter } from 'next/router'

const withInsuranceGuard = (WrappedComponent, insuranceType) => {
	return () => {
		const [isLoading, setLoading] = useState(true)
		const state = useSelector(state => state)
		const router = useRouter()

		const canVisitForm = useCallback(state => {
			let canVisit
			switch (insuranceType) {
				case 'fire-insurance':
					canVisit =
						state.product.product &&
						state.product.product.insuranceType === 'بیمه آتش سوزی'
					break
				case 'third-party-insurance':
					canVisit =
						state.product.product &&
						state.product.product.insuranceType === 'بیمه شخص ثالث'
					break
				case 'body-insurance':
					canVisit =
						state.product.product &&
						state.product.product.insuranceType === 'بیمه بدنه'
					break
				case 'travel-insurance':
					canVisit =
						state.product.product &&
						state.product.product.insuranceType === 'بیمه مسافرتی'
					break
				case 'life-insurance':
					canVisit =
						state.product.product &&
						state.product.product.insuranceType === 'بیمه عمر'
					break
				case 'health-insurance':
					canVisit =
						state.product.product &&
						state.product.product.insuranceType ===
							'بیمه درمان تکمیلی'
					break
				case 'motor-insurance':
					canVisit =
						state.product.product &&
						state.product.product.insuranceType === 'بیمه موتور'
					break
				default:
					canVisit = true
			}
			return canVisit
		}, [])

		useEffect(() => {
			if (state._persist.rehydrated) {
				if (!canVisitForm(state)) router.replace('/')
				else setLoading(false)
			}
		}, [setLoading, canVisitForm, state._persist])

		return !isLoading && <WrappedComponent />
	}
}

export default withInsuranceGuard
