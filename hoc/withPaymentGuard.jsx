import React, { useState, useEffect } from 'react'
import { useSelector } from 'react-redux'
import { useRouter } from 'next/router'

const withProductGuard = WrappedComponent => {
	return () => {
		const [isLoading, setLoading] = useState(true)
		const state = useSelector(state => state)
		const router = useRouter()

		useEffect(() => {
			if (state._persist.rehydrated) {
				let canVisit
				if (
					state.product.product &&
					state.product.product.insuranceType === 'بیمه شخص ثالث'
				)
					canVisit = !!state.product.invoice
				else if (state.product.form.name) canVisit = true
				if (!canVisit) router.replace('/')
				else setLoading(false)
			}
		}, [setLoading, state._persist])

		return !isLoading && <WrappedComponent />
	}
}

export default withProductGuard
