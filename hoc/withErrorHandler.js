import React, { Component } from 'react'
import ErrorDialog from '../components/error-dialog/ErrorDialog'

const authPaths = ['/login', '/signup', '/verify', '/resend-code-otp']

const errorMessage = (
	<p>
		خطایی پیش آمد. لطفا دوباره تلاش کنید.
		<br /> در صورت نیاز با پشتیبانی تماس بگیرید.
		<div className="text-center">
			<a href="tel:+982191002008" style={{ color: 'blue' }}>
				021-91002008
			</a>
		</div>
	</p>
)

const withErrorHandler = (WrappedComponent, axios) => {
	return class extends Component {
		state = {
			error: null
		}

		constructor(props) {
			super(props)
			this.reqInterceptor = axios.interceptors.request.use(req => {
				this.setState({ error: null })
				return req
			})
			this.resInterceptor = axios.interceptors.response.use(
				req => req,
				error => {
					const url = error.response.config.url
					const isAuthPath = authPaths.some(path =>
						url.startsWith(path)
					)
					if (!isAuthPath) this.setState({ error })
					return Promise.reject(error)
				}
			)
		}

		componentWillUnmount() {
			axios.interceptors.response.eject(this.reqInterceptor)
			axios.interceptors.request.eject(this.reqInterceptor)
		}

		errorConfirmed = () => {
			this.setState({ error: null })
		}

		render() {
			const { error } = this.state
			return (
				<>
					<ErrorDialog
						open={!!error}
						close={this.errorConfirmed}
						error={errorMessage}
					/>
					<WrappedComponent {...this.props} />
				</>
			)
		}
	}
}

export default withErrorHandler
