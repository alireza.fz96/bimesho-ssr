import React, { useState, useEffect } from 'react'
import { useSelector } from 'react-redux'
import { useRouter } from 'next/router'

const withAuth = WrappedComponent => {
	return () => {
		const [isLoading, setLoading] = useState(true)
		const state = useSelector(state => state)
		const router = useRouter()

		useEffect(() => {
			if (state._persist.rehydrated) {
				if (!state.user.user)
					router.replace(
						'/login?redirect=' + router.pathname,
						'/login'
					)
				else setLoading(false)
			}
		}, [state._persist, setLoading])

		return !isLoading && <WrappedComponent />
	}
}

export default withAuth
