import { useState, useEffect, useCallback } from 'react'
import { useSelector } from 'react-redux'
import { useRouter } from 'next/router'

const withEnquiryGuard = (WrappedComponent, insuranceType) => {
	return () => {
		const [isLoading, setLoading] = useState(true)
		const state = useSelector(state => state)
		const router = useRouter()

		const canVisitForm = useCallback(state => {
			let canVisit
			switch (insuranceType) {
				case 'fire-insurance':
					canVisit = state.fireInsurance.form.done
					break
				case 'third-party-insurance':
					canVisit = state.thirdPartyInsurance.form.done
					break
				case 'body-insurance':
					canVisit = state.bodyInsurance.form.done
					break
				case 'travel-insurance':
					canVisit = state.travelInsurance.form.done
					break
				case 'life-insurance':
					canVisit = state.lifeInsurance.form.done
					break
				case 'health-insurance':
					canVisit = state.healthInsurance.form.done
					break
				case 'motor-insurance':
					canVisit = state.motorInsurance.form.done
					break
				default:
					canVisit = true
			}
			return canVisit
		}, [])

		useEffect(() => {
			if (state._persist.rehydrated) {
				if (!canVisitForm(state)) router.replace('/' + insuranceType)
				else setLoading(false)
			}
		}, [setLoading, canVisitForm, state._persist])

		return !isLoading && <WrappedComponent />
	}
}

export default withEnquiryGuard
