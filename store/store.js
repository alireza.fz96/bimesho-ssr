import { createStore, applyMiddleware, combineReducers, compose } from 'redux'
import thirdPartyInsuranceReducer from './reducers/thirdPartyInsuranceReducer'
import bodyInsuranceReducer from './reducers/bodyInsuranceReducer'
import fireInsuranceReducer from './reducers/fireInsuranceReducer'
import travelInsuranceReducer from './reducers/travelInsuranceReducer'
import healthInsuranceReducer from './reducers/healthInsuranceReducer'
import LifeInsuranceReducer from './reducers/lifeInsuranceReducer'
import coreDataReducer from './reducers/coreData'
import productReducer from './reducers/productReducer'
import MotorInsuranceReducer from './reducers/motorThirdPartyInsurance'
import userReducer from './reducers/userReducer'
import { createWrapper } from 'next-redux-wrapper'
import thunk from 'redux-thunk'
import hardSet from 'redux-persist/lib/stateReconciler/autoMergeLevel2'

const composeEnhancers =
	(process.env.NODE_ENV === 'development' &&
		global.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__) ||
	compose

const rootReducer = combineReducers({
	thirdPartyInsurance: thirdPartyInsuranceReducer,
	bodyInsurance: bodyInsuranceReducer,
	coreData: coreDataReducer,
	fireInsurance: fireInsuranceReducer,
	travelInsurance: travelInsuranceReducer,
	healthInsurance: healthInsuranceReducer,
	lifeInsurance: LifeInsuranceReducer,
	motorInsurance: MotorInsuranceReducer,
	user: userReducer,
	product: productReducer
})

const makeStore = ({ isServer }) => {
	if (isServer) {
		return createStore(
			rootReducer,
			composeEnhancers(applyMiddleware(thunk))
		)
	} else {
		const { persistStore, persistReducer } = require('redux-persist')
		const storage = require('redux-persist/lib/storage').default

		const persistConfig = {
			key: 'root',
			storage,
			stateReconciler: hardSet
		}

		const persistedReducer = persistReducer(persistConfig, rootReducer)

		const store = createStore(
			persistedReducer,
			composeEnhancers(applyMiddleware(thunk))
		)

		store.__persistor = persistStore(store)

		return store
	}
}

export const wrapper = createWrapper(makeStore, { debug: true })
