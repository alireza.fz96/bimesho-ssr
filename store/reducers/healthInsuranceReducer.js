import * as Actions from '../actions/actionTypes'

const steps = [
	{
		sectionTitle: 'بیمه پایه خود را انتخاب کنید',
		header: 'بیمه پایه',
		img: '/imgs/svg/health-icon.svg',
		name: 'baseInsurance'
	},
	{
		sectionTitle: 'سن متقضی (ها) را تعیین کنید',
		header: 'سن متقضی (ها)',
		img: '/imgs/svg/health-icon.svg',
		name: 'applicants'
	}
]

const initialState = {
	form: {
		baseInsurance: null,
		applicants: { qty1: 0, qty2: 0, qty3: 0, qty4: 0 },
		done: false,
		badges: []
	},
	step: {
		...steps[0],
		isValid: false,
		index: 0
	}
}

export default function reducer(state = initialState, action) {
	switch (action.type) {
		case Actions.HEALTH_SET_PARAM:
			return {
				...state,
				form: {
					...state.form,
					[action.payload.field]: action.payload.value
				}
			}

		case Actions.HEALTH_NEXT_SLIDE:
			const nextStep = steps[state.step.index + 1]
			return {
				...state,
				step: {
					...nextStep,
					index: state.step.index + 1,
					isValid:
						!!state.form[nextStep.name] &&
						state.step.index + 1 < steps.length - 1
				}
			}

		case Actions.HEALTH_PREV_SLIDE:
			const prevStep = steps[state.step.index - 1]
			return {
				...state,
				step: {
					...prevStep,
					isValid: !!state.form[prevStep.name],
					index: state.step.index - 1
				}
			}

		case Actions.HEALTH_SET_FORM:
			return {
				...state,
				form: action.payload
			}

		case Actions.HEALTH_INITIAL:
			return initialState

		default:
			return state
	}
}
