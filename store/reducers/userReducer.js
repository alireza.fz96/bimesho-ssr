import * as Actions from '../actions/actionTypes'

const initialState = {
	user: null
}

export default function userReducer(state = initialState, action) {
	switch (action.type) {
		case Actions.LOGIN:
			return { ...state, user: action.payload }
		case Actions.LOGOUT:
			return initialState
		default:
			return state
	}
}
