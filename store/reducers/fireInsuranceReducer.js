import * as Actions from '../actions/actionTypes'

const steps = [
	{
		sectionTitle: 'نوع ملک و سازه خود را انتخاب کنید',
		header: 'نوع ملک و سازه',
		img: '/imgs/svg/fire-icon.svg',
		name: 'propertyType'
	},
	{
		sectionTitle: 'تعداد و متراژ واحد های خود را تعیین کنید',
		header: 'تعداد و متراژ واحد',
		img: '/imgs/svg/fire-icon.svg',
		name: 'unitCount'
	},
	{
		sectionTitle: 'ارزش لوازم خانگی خود را تعیین کنید',
		header: 'تعداد و متراژ واحد',
		img: '/imgs/svg/fire-icon.svg',
		name: 'value'
	}
]

const initialState = {
	form: {
		propertyType: null,
		buildingType: null,
		unitCount: null,
		unitMeter: null,
		value: null,
		badges: [],
		done: false
	},
	step: {
		...steps[0],
		isValid: false,
		index: 0
	}
}

export default function reducer(state = initialState, action) {
	let badges
	switch (action.type) {
		case Actions.FIRE_PROPERTY_TYPE:
			badges = [action.payload.name]
			if (state.form.buildingType)
				badges.push(state.form.buildingType.name)
			return {
				...state,
				form: {
					...state.form,
					propertyType: action.payload,
					badges
				}
			}

		case Actions.FIRE_BUILDING_TYPE:
			badges = []
			if (state.form.propertyType)
				badges.push(state.form.propertyType.name)
			badges.push(action.payload.name)
			return {
				...state,
				form: {
					...state.form,
					buildingType: action.payload,
					badges
				}
			}

		case Actions.FIRE_UNIT_COUNTS:
			return {
				step: {
					...state.step,
					isValid: action.payload && state.form.unitMeter
				},
				form: { ...state.form, unitCount: action.payload }
			}

		case Actions.FIRE_UNIT_METER:
			return {
				step: {
					...state.step,
					isValid: action.payload && state.form.unitCount
				},
				form: { ...state.form, unitMeter: action.payload }
			}

		case Actions.FIRE_VALUE:
			return {
				...state,
				form: { ...state.form, value: action.payload }
			}

		case Actions.FIRE_NEXT_SLIDE:
			const nextStep = steps[state.step.index + 1]
			return {
				...state,
				step: {
					...nextStep,
					index: state.step.index + 1,
					isValid:
						!!state.form[nextStep.name] &&
						state.step.index + 1 < steps.length - 1
				}
			}

		case Actions.FIRE_PREV_SLIDE:
			const prevStep = steps[state.step.index - 1]
			return {
				...state,
				step: {
					...prevStep,
					isValid: !!state.form[prevStep.name],
					index: state.step.index - 1
				}
			}

		case Actions.FIRE_SET_FORM:
			return {
				...state,
				form: action.payload
			}

		case Actions.FIRE_SET_DONE:
			return {
				...state,
				form: { ...state.form, done: true }
			}

		case Actions.FIRE_INITIAL:
			return initialState

		default:
			return state
	}
}
