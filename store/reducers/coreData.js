const initialState = {
	additionalCoverDiscounts: [
		{ name: 'با خسارت', value: 0 },
		{ name: 'بدون تخفیف', value: 0 },
		{ name: '۱ سال', value: 1 },
		{ name: '۲ سال', value: 2 },
		{ name: '۳ سال', value: 3 },
		{ name: '۴ سال', value: 4 },
		{ name: '۵ سال', value: 5 },
		{ name: '۶ سال', value: 6 },
		{ name: '۷ سال', value: 7 },
		{ name: '۸ سال یا بیشتر', value: 8 }
	],
	mainCoverDiscounts: [
		{ name: 'با خسارت', value: 0 },
		{ name: 'بدون تخفیف', value: 0 },
		{ name: '۱ سال', value: 1 },
		{ name: '۲ سال', value: 2 },
		{ name: '۳ سال', value: 3 },
		{ name: '۴ سال', value: 4 },
		{ name: '۵ سال', value: 5 },
		{ name: '۶ سال', value: 6 },
		{ name: '۷ سال', value: 7 },
		{ name: '۸ سال یا بیشتر', value: 8 }
	],
	vehicleUsages: [
		{ name: 'شخصی', value: 1 },
		{ name: 'تاکسی درون شهری', value: 2 },
		{ name: 'تاکسی برون شهری', value: 3 }
	],
	years: makeYears(),
	lifeDamageCounts: [
		{ name: 'بدون خسارت جانی', value: -1 },
		{ name: '۱ خسارت جانی', value: 1 },
		{ name: '۲ خسارت جانی', value: 2 },
		{ name: '۳ خسارت جانی یا بیشتر', value: 4 }
	],
	financeDamageCounts: [
		{ name: 'بدون خسارت مالی', value: -1 },
		{ name: '۱ خسارت مالی', value: 1 },
		{ name: '۲ خسارت مالی', value: 2 },
		{ name: '۳ خسارت مالی یا بیشتر', value: 4 }
	],
	driverAccidentDamageCounts: [
		{ name: 'بدون خسارت ', value: -1 },
		{ name: '۱ خسارت ', value: 1 },
		{ name: '۲ خسارت ', value: 2 },
		{ name: '۳ خسارت  یا بیشتر', value: 4 }
	],
	driverAccidentDamageDiscounts: [
		{ name: '۰ درصد (بدون تخفیف)', value: -10 },
		{ name: '۵ درصد', value: 5 },
		{ name: '۱۰ درصد', value: 10 },
		{ name: '۱۵ درصد', value: 15 },
		{ name: '۲۰ درصد', value: 20 },
		{ name: '۲۵ درصد', value: 25 },
		{ name: '۳۰ درصد', value: 30 },
		{ name: '۳۵ درصد', value: 35 },
		{ name: '۴۰ درصد', value: 40 },
		{ name: '۴۵ درصد', value: 45 },
		{ name: '۵۰ درصد', value: 50 },
		{ name: '۵۵ درصد', value: 55 },
		{ name: '۶۰ درصد', value: 60 },
		{ name: '۶۵ درصد', value: 65 },
		{ name: '۷۰ درصد', value: 70 },
		{ name: '۷۵ درصد', value: 75 }
	],
	insuranceDiscounts: [
		{ name: '۰ درصد (بدون تخفیف)', value: -10 },
		{ name: '۵ درصد', value: 5 },
		{ name: '۱۰ درصد', value: 10 },
		{ name: '۱۵ درصد', value: 15 },
		{ name: '۲۰ درصد', value: 20 },
		{ name: '۲۵ درصد', value: 25 },
		{ name: '۳۰ درصد', value: 30 },
		{ name: '۳۵ درصد', value: 35 },
		{ name: '۴۰ درصد', value: 40 },
		{ name: '۴۵ درصد', value: 45 },
		{ name: '۵۰ درصد', value: 50 },
		{ name: '۵۵ درصد', value: 55 },
		{ name: '۶۰ درصد', value: 60 },
		{ name: '۶۵ درصد', value: 65 },
		{ name: '۷۰ درصد', value: 70 },
		{ name: '۷۵ درصد', value: 75 }
	],
	propertyTypes: [
		{ name: 'یک واحد در آپارتمان', value: 1 },
		{ name: 'یک ساختمان ویلایی', value: 2 },
		{ name: 'آپارتمان یا مجتمع', value: 3 }
	],
	buildingTypes: [
		{ name: 'بتنی', value: 'concrete' },
		{ name: 'آجری', value: 'bricks' },
		{ name: 'فلزی', value: 'metal' }
	],
	orderSteps: ['انتخاب', 'مشخصات', 'ارسال', 'پرداخت'],
	companiesList: [
		{ name: 'بیمه راضی', id: '13' },
		{ name: 'بیمه نوین', id: '12' },
		{ name: 'بیمه پارسیان', id: '11' },
		{ name: 'بیمه آرمان', id: '10' },
		{ name: 'بیمه سینا', id: '9' },
		{ name: 'بیمه کوثر', id: '8' },
		{ name: 'بیمه ما', id: '7' },
		{ name: 'بیمه البرز', id: '4' },
		{ name: 'کارآفرین', id: '6' },
		{ name: 'بیمه دانا', id: '5' },
		{ name: 'بیمه آسیا', id: '3' },
		{ name: 'بیمه ایران', id: '2' },
		{ name: 'بیمه معلم', id: '14' },
		{ name: 'بیمه پاسارگاد', id: '15' },
		{ name: 'بیمه سامان', id: '16' },
		{ name: 'بیمه دی', id: '18' },
		{ name: 'بیمه ملت', id: '19' },
		{ name: 'بیمه میهن', id: '20' },
		{ name: 'بیمه تجارت نو', id: '21' },
		{ name: 'بیمه آسماری', id: '23' },
		{ name: 'بیمه تعاون', id: '24' }
	],
	ages: [
		{ name: '1-12', value: '0' },
		{ name: '13-65', value: '1' },
		{ name: '66-70', value: '2' },
		{ name: '71-75', value: '3' },
		{ name: '76-80', value: '4' },
		{ name: '81-85', value: '5' }
	],
	stayingTimes: [
		{ name: '۱ تا ۷ روز', value: '1-7' },
		{ name: '۸ تا ۱۵ روز', value: '8-15' },
		{ name: '۱۶ تا ۲۳ روز', value: '16-23' },
		{ name: '۲۴ تا ۳۱ روز', value: '24-31' },
		{ name: '۳۲ تا ۴۵ روز', value: '32-45' },
		{ name: '۴۶ تا ۶۲ روز', value: '46-62' },
		{ name: '۶۳ تا ۹۲ روز', value: '63-92' },
		{ name: '۶ ماه', value: '93-180' },
		{ name: '۱ سال', value: '181-365' }
	],
	baseInsurances: [
		{ name: 'تامین اجتماعی', value: '1' },
		{ name: 'نیرو های مسلح', value: '2' },
		{ name: 'بانک ها', value: '3' },
		{ name: 'بیمه سلامت', value: '4' },
		{ name: 'ندارم', value: '0' }
	],
	applicantAges: [
		{ name: 'از ۱ تا ۱۵ سال', value: 'qty1' },
		{ name: 'از ۱۶ تا ۵۰ سال', value: 'qty2' },
		{ name: 'از ۵۱ تا ۶۰ سال', value: 'qty3' },
		{ name: 'از ۶۱ تا ۷۰ سال', value: 'qty4' }
	],
	insuranceRights: [
		{ name: '2,400,000 تومان', value: '5' },
		{ name: '3,200,000 تومان', value: '4' },
		{ name: '5,300,000 تومان', value: '3' },
		{ name: '8,500,000 تومان', value: '2' },
		{ name: '10,000,000 تومان', value: '6' },
		{ name: '12,750,000 تومان', value: '1' },
		{ name: '15,000,000 تومان', value: '10' },
		{ name: '20,000,000 تومان', value: '9' },
		{ name: '40,000,000 تومان', value: '8' },
		{ name: '80,000,000 تومان', value: '7' }
	],
	paymentMethods: [{ name: 'سالانه', value: '4' }],
	contractTimes: [
		{ name: '۱۲ ساله', value: '12' },
		{ name: '۱۳ ساله', value: '13' },
		{ name: '۱۶ ساله', value: '16' },
		{ name: '۲۰ ساله', value: '20' },
		{ name: '۲۵ ساله', value: '25' }
	]
}

function makeYears() {
	const years = []
	for (let i = 0, j = 1399, k = 2020; i < 100; i++)
		years.push({
			jalali: j - i,
			miladi: k - i,
			name: `${j - i} | ${k - i}`
		})
	return years
}

export default function coreDataReducer() {
	return initialState
}
