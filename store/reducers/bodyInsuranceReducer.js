import * as Actions from '../actions/actionTypes'

const steps = [
	{
		sectionTitle: 'بـــرند وسیله نقلیه خود را انتخاب کنید',
		header: 'برند',
		img: '/imgs/svg/car-brand-ic.svg',
		name: 'brand'
	},
	{
		sectionTitle: 'مدل وسیله نقلیه خود را انتخاب کنید',
		header: 'مدل',
		img: '/imgs/svg/car-model-ic.svg',
		name: 'model'
	},
	{
		sectionTitle: 'سال تولید وسیله نقلیه خود را انتخاب کنید',
		header: 'سال تولید',
		img: '/imgs/svg/generated-year-ic.svg',
		name: 'productionYear'
	},
	{
		sectionTitle: 'تخفیف عدم خسارت پوشش ها را انتخاب کنید',
		header: 'تخفیف عدم خسارت پوشش ها',
		img: '/imgs/svg/insurance-history-ic.svg',
		name: 'mainCoverDiscount'
	},
	{
		sectionTitle: 'ارزش خودرو را تعیین کنید',
		header: 'ارزش خودرو',
		img: '/imgs/svg/car-price-ic.svg',
		name: 'vehicleValue'
	}
]

const initialState = {
	form: {
		category: {
			id: 1,
			name: 'سواری',
			icon: '901',
			brandTitle: 'برند',
			modelTitle: 'مدل',
			status: 1
		},
		brand: null,
		model: null,
		productionYear: null,
		mainCoverDiscount: null,
		additionalCoverDiscount: null,
		vehicleValue: null,
		vehicleUsage: null,
		badges: [],
		done: false,
		domesticProduction: true,
		isBrandNew: { name: 'خیر', value: false }
	},
	step: {
		...steps[0],
		isValid: false,
		index: 0
	}
}

export default function reducer(state = initialState, action) {
	switch (action.type) {
		case Actions.BODY_SET_BRAND:
			return {
				...state,
				form: {
					...state.form,
					brand: action.payload,
					model: null,
					productionYear: null,
					insuranceDiscount: null,
					badges: [action.payload.name]
				}
			}

		case Actions.BODY_SET_MODEL:
			return {
				...state,
				form: {
					...state.form,
					model: action.payload,
					productionYear: null,
					insuranceDiscount: null,
					badges: [state.form.brand.name, action.payload.name]
				}
			}

		case Actions.BODY_SET_PRODUCTION_YEAR:
			return {
				...state,
				form: {
					...state.form,
					productionYear: action.payload,
					badges: [
						state.form.brand.name,
						state.form.model.name,
						action.payload.name
					]
				}
			}

		case Actions.BODY_SET_ADDITIONAL_COVER_DISCOUNT:
			return {
				...state,
				form: { ...state.form, additionalCoverDiscount: action.payload }
			}

		case Actions.BODY_SET_MAIN_COVER_DISCOUNT:
			return {
				...state,
				form: { ...state.form, mainCoverDiscount: action.payload }
			}

		case Actions.BODY_SET_VEHICLE_USAGE:
			return {
				...state,
				form: { ...state.form, vehicleUsage: action.payload }
			}

		case Actions.BODY_SET_VEHICLE_VALUE:
			return {
				...state,
				form: { ...state.form, vehicleValue: action.payload }
			}

		case Actions.BODY_NEXT_SLIDE:
			const nextStep = steps[state.step.index + 1]
			return {
				...state,
				step: {
					...nextStep,
					index: state.step.index + 1,
					isValid:
						!!state.form[nextStep.name] &&
						state.step.index + 1 < steps.length - 1
				}
			}

		case Actions.BODY_PREV_SLIDE:
			const prevStep = steps[state.step.index - 1]
			return {
				...state,
				step: {
					...prevStep,
					isValid: !!state.form[prevStep.name],
					index: state.step.index - 1
				}
			}

		case Actions.BODY_SET_FORM:
			return {
				...state,
				form: action.payload
			}

		case Actions.BODY_SET_DONE:
			return {
				...state,
				form: { ...state.form, done: true }
			}

		case Actions.BODY_INITIAL:
			return initialState

		case Actions.BODY_NEW:
			return {
				...state,
				form: { ...state.form, isBrandNew: action.payload }
			}

		case Actions.BODY_SET_DOMESTIC_PRDOCUTION:
			return {
				...state,
				form: { ...state.form, domesticProduction: action.payload }
			}

		default:
			return state
	}
}
