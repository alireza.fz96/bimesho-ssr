import * as Actions from '../actions/actionTypes'
import moment from 'moment'

const steps = [
	{
		sectionTitle: 'بـــرند موتور خود را انتخاب کنید',
		header: 'برند',
		img: '/imgs/svg/motor-model-ic.svg',
		name: 'brand'
	},
	{
		sectionTitle: 'مدل موتور خود را انتخاب کنید',
		header: 'مدل',
		img: '/imgs/svg/motor-model-ic.svg',
		name: 'model'
	},
	{
		sectionTitle: 'سال تولید موتور خود را انتخاب کنید',
		header: 'سال تولید',
		img: '/imgs/svg/generated-year-ic.svg',
		name: 'productionYear'
	},
	{
		sectionTitle: 'سابقه بیمه نامه خود را مشخص کنید',
		header: 'سابقه بیمه نامه',
		img: '/imgs/svg/insurance-history-ic.svg',
		name: 'prevCompany'
	},
	{
		sectionTitle: 'مدت اعتبار بیمه نامه قبلی خود را تعیین کنید',
		header: 'مدت اعتبار',
		img: '/imgs/svg/insurance-expiration-ic.svg',
		name: 'insuranceEnd'
	},
	{
		sectionTitle: 'درصد تخفیف بیمه نامه قبلی خود را تعیین کنید',
		header: 'درصد تخفیف',
		img: '/imgs/svg/insurance-discount-ic.svg',
		name: 'insuranceDiscount'
	},
	{
		sectionTitle: 'سابقه دریافت خسارت از بیمه نامه قبلی را انتخاب کنید',
		header: 'سوابق خسارت',
		img: '/imgs/svg/insurance-damage-ic.svg',
		name: 'lifeDamageCount'
	}
]

const initialState = {
	form: {
		category: {
			id: 10,
			name: 'موتور سیکلت',
			icon: '910',
			brandTitle: 'برند',
			modelTitle: 'مدل',
			status: 1
		},
		brand: null,
		model: null,
		productionYear: null,
		insuranceDiscount: null,
		insuranceStart: moment().locale('fa').subtract(365, 'day'),
		insuranceEnd: null,
		lifeDamageCount: null,
		financeDamageCount: null,
		datePrice: null,
		prevCompany: null,
		driverAccidentDamageCount: null,
		driverAccidentDamageDiscount: null,
		badges: ['موتور سیکلت'],
		done: false
	},
	step: {
		...steps[0],
		isValid: false,
		index: 0
	}
}

const isValidDate = (start, end) => {
	return !(
		start.isAfter(end, 'day') ||
		start.isSame(end, 'day') ||
		end.diff(start, 'day') > 366
	)
}

const isValidForm = state => {
	const nextStep = steps[state.step.index + 1]
	let valid =
		!!state.form[nextStep.name] && state.step.index + 1 < steps.length - 1
	if (nextStep.name === 'prevCompany')
		return (
			valid &&
			state.form.prevCompany !== '0' &&
			state.form.prevCompany !== '110' &&
			state.form.prevCompany !== '120'
		)
	if (nextStep.name === 'insuranceDiscount')
		return (
			state.form.driverAccidentDamageDiscount &&
			state.form.insuranceDiscount
		)
	return valid
}

export default function motorInsuranceReducer(state = initialState, action) {
	switch (action.type) {
		case Actions.MOTOR_SET_BRAND:
			return {
				...state,
				form: {
					...state.form,
					brand: action.payload,
					model: null,
					productionYear: null,
					insuranceDiscount: null,
					badges: [state.form.category.name, action.payload.name]
				}
			}

		case Actions.MOTOR_SET_MODEL:
			return {
				...state,
				form: {
					...state.form,
					model: action.payload,
					productionYear: null,
					insuranceDiscount: null,
					badges: [
						state.form.category.name,
						state.form.brand.name,
						action.payload.name
					]
				}
			}

		case Actions.MOTOR_SET_PRODUCTION_YEAR:
			return {
				...state,
				form: {
					...state.form,
					productionYear: action.payload,
					badges: [
						state.form.category.name,
						state.form.brand.name,
						state.form.model.name,
						action.payload.name
					]
				}
			}

		case Actions.MOTOR_INSURANCE_DISCOUNT:
			return {
				...state,
				form: { ...state.form, insuranceDiscount: action.payload }
			}

		case Actions.MOTOR_INSURANCE_START:
			let isValidStart =
				state.form.insuranceEnd &&
				isValidDate(action.payload, state.form.insuranceEnd)
			return {
				step: {
					...state.step,
					isValid: isValidStart
				},
				form: { ...state.form, insuranceStart: action.payload }
			}

		case Actions.MOTOR_INSURANCE_END:
			const isValidEnd = isValidDate(
				state.form.insuranceStart,
				action.payload
			)
			return {
				step: { ...state.step, isValid: isValidEnd },
				form: { ...state.form, insuranceEnd: action.payload }
			}

		case Actions.MOTOR_LIFE_DAMAGE_COUNT:
			return {
				...state,
				form: { ...state.form, lifeDamageCount: action.payload }
			}

		case Actions.MOTOR_FINANCE_DAMAGE_COUNT:
			return {
				...state,
				form: { ...state.form, financeDamageCount: action.payload }
			}
		case Actions.MOTOR_DRIVER_ACCIDENT_COUNT:
			return {
				...state,
				form: {
					...state.form,
					driverAccidentDamageCount: action.payload
				}
			}

		case Actions.MOTOR_DRIVER_ACCIDENT_DISCOUNT:
			return {
				...state,
				form: {
					...state.form,
					driverAccidentDamageDiscount: action.payload
				}
			}

		case Actions.MOTOR_NEXT_SLIDE:
			const nextStep = steps[state.step.index + 1]
			return {
				...state,
				step: {
					...nextStep,
					index: state.step.index + 1,
					isValid: isValidForm(state)
				}
			}

		case Actions.MOTOR_PREV_SLIDE:
			const prevStep = steps[state.step.index - 1]
			return {
				...state,
				step: {
					...prevStep,
					isValid: !!state.form[prevStep.name],
					index: state.step.index - 1
				}
			}

		case Actions.MOTOR_SET_FORM:
			return {
				...state,
				form: action.payload
			}

		case Actions.MOTOR_SET_DONE:
			return {
				...state,
				form: { ...state.form, done: true }
			}

		case Actions.MOTOR_INITIAL:
			return initialState

		case Actions.MOTOR_DATE_PRICE:
			return {
				...state,
				form: { ...state.form, datePrice: action.payload }
			}

		case Actions.MOTOR_PREV_COMPANY:
			const isValid =
				action.payload !== '0' &&
				action.payload !== '110' &&
				action.payload !== '120'
			const newForm = isValid
				? { ...state.form, prevCompany: action.payload }
				: {
						...initialState.form,
						category: state.form.category,
						brand: state.form.brand,
						model: state.form.model,
						badges: state.form.badges,
						productionYear: state.form.productionYear,
						prevCompany: action.payload
				  }
			return {
				step: {
					...state.step,
					isValid
				},
				form: newForm
			}

		case Actions.MOTOR_NO_ACCIDENT:
			return {
				...state,
				form: {
					...state.form,
					driverAccidentDamageCount: null,
					financeDamageCount: null,
					lifeDamageCount: null
				}
			}

		default:
			return state
	}
}
