import * as Actions from '../actions/actionTypes'

const steps = [
	{
		sectionTitle: 'حق بیمه و نحوه پرداخت را تعیین کنید',
		header: 'حق بیمه',
		img: '/imgs/svg/life-icon.svg',
		name: 'insuranceRight'
	},
	{
		sectionTitle: 'طول مدت قرارداد را تعیین کنید',
		header: 'طول مدت قرارداد',
		img: '/imgs/svg/life-icon.svg',
		name: 'contractTime'
	}
]

const initialState = {
	form: {
		insuranceRight: null,
		paymentMethod: null,
		birthday: null,
		contractTime: null,
		badges: [],
		done: false
	},
	step: {
		...steps[0],
		isValid: false,
		index: 0
	}
}

export default function reducer(state = initialState, action) {
	switch (action.type) {
		case Actions.LIFE_SET_PARAM:
			return {
				...state,
				form: {
					...state.form,
					[action.payload.field]: action.payload.value
				}
			}

		case Actions.LIFE_NEXT_SLIDE:
			const nextStep = steps[state.step.index + 1]
			return {
				...state,
				step: {
					...nextStep,
					index: state.step.index + 1,
					isValid:
						!!state.form[nextStep.name] &&
						state.step.index + 1 < steps.length - 1
				}
			}

		case Actions.LIFE_PREV_SLIDE:
			const prevStep = steps[state.step.index - 1]
			return {
				...state,
				step: {
					...prevStep,
					isValid: !!state.form[prevStep.name],
					index: state.step.index - 1
				}
			}

		case Actions.LIFE_SET_FORM:
			return {
				...state,
				form: action.payload
			}

		case Actions.LIFE_INITIAL:
			return initialState

		default:
			return state
	}
}
