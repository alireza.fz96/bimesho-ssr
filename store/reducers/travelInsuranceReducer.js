import * as Actions from '../actions/actionTypes'

const steps = [
    {
        sectionTitle: 'مقصد خود را تعیین کنید',
        header: 'مقصد',
        img: '/imgs/svg/travel-icon.svg',
        name: 'country'
    },
    {
        sectionTitle: 'سن مسافر را تعیین کنید',
        header: 'سن مسافر',
        img: '/imgs/svg/travel-icon.svg',
        name: 'age'
    }
]

const initialState = {
    form: {
        country: null,
        age: null,
        stayingTime: null,
        done: false,
        badges: []
    },
    step: {
        ...steps[0],
        isValid: false,
        index: 0
    }
}

export default function reducer(state = initialState, action) {
    switch (action.type) {
        case Actions.TRAVEL_SET_PARAM:
            return {
                step: { ...state.step, isValid: action.payload.isValid },
                form: {
                    ...state.form,
                    [action.payload.field]: action.payload.value
                }
            }

        case Actions.TRAVEL_NEXT_SLIDE:
            const nextStep = steps[state.step.index + 1]
            return {
                ...state,
                step: {
                    ...nextStep,
                    index: state.step.index + 1,
                    isValid:
                        !!state.form[nextStep.name] &&
                        state.step.index + 1 < steps.length - 1
                }
            }

        case Actions.TRAVEL_PREV_SLIDE:
            const prevStep = steps[state.step.index - 1]
            return {
                ...state,
                step: {
                    ...prevStep,
                    isValid: !!state.form[prevStep.name],
                    index: state.step.index - 1
                }
            }

        case Actions.TRAVEL_SET_FORM:
            return {
                ...state,
                form: action.payload
            }

        case Actions.TRAVEL_INITIAL:
            return initialState

        default:
            return state
    }
}
