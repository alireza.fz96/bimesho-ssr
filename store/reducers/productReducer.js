import * as Actions from '../actions/actionTypes'

const initialState = {
	product: null,
	form: {
		phone: '',
		mobile: '',
		province: '',
		city: '',
		address: '',
		gender: '',
		national_code: '',
		birthday: null,
		name: ''
	},
	address: {
		phone: '',
		mobile: '',
		province: '',
		city: '',
		address: '',
		name: '',
		postal_code: '',
		explain: '',
		email: ''
	},
	discountCode: '',
	invoice: null
}

export default function productReducer(state = initialState, action) {
	switch (action.type) {
		case Actions.SET_PRODUCT:
			return {
				...initialState,
				product: action.payload,
				discountCode: state.discountCode
			}
		case Actions.SET_PRODUCT_FORM:
			return { ...state, form: action.payload }
		case Actions.SET_PRODUCT_SENT_ADDRESS:
			return { ...state, address: action.payload }
		case Actions.RESET_PRODUCT:
			return initialState
		case Actions.SET_DISCOUNT_CODE:
			return { ...state, discountCode: action.payload }
		case Actions.SET_INVOICE:
			return { ...state, invoice: action.payload }
		default:
			return state
	}
}
