import * as Actions from './actionTypes'
import { saveInsurance } from '../../api/ThirdpersonInsuranceApi'

export const setProduct = product => {
	return {
		type: Actions.SET_PRODUCT,
		payload: product
	}
}

export const setProductForm = form => {
	return {
		type: Actions.SET_PRODUCT_FORM,
		payload: form
	}
}

export const setProductAddress = address => {
	return { type: Actions.SET_PRODUCT_SENT_ADDRESS, payload: address }
}

export const resetPorduct = () => {
	return {
		type: Actions.RESET_PRODUCT
	}
}

export const setDiscountCode = code => {
	return {
		type: Actions.SET_DISCOUNT_CODE,
		payload: code
	}
}

export const setInvoice = invoice => {
	return {
		type: Actions.SET_INVOICE,
		payload: invoice
	}
}
