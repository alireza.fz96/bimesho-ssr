import * as Actions from './actionTypes'

export const setParam = (field, value) => {
	return {
		type: Actions.HEALTH_SET_PARAM,
		payload: { field, value }
	}
}

export const nextSlide = () => {
	return {
		type: Actions.HEALTH_NEXT_SLIDE
	}
}

export const prevSlide = () => {
	return {
		type: Actions.HEALTH_PREV_SLIDE
	}
}

export const setForm = form => {
	return {
		type: Actions.HEALTH_SET_FORM,
		payload: form
	}
}

export const reset = () => {
	return {
		type: Actions.HEALTH_INITIAL
	}
}
