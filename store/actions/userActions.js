import { saveInsurance } from '../../api/ThirdpersonInsuranceApi'
import * as Actions from './actionTypes'

export const login = (user, from) => {
	return (dispatch, getState) => {
		dispatch({
			type: Actions.LOGIN,
			payload: user
		})
		if (from === '/third-party-order')
			saveInsurance({ ...getState(), user: { user } })
	}
}

export const logout = () => {
	return {
		type: Actions.LOGOUT
	}
}
