import * as Actions from './actionTypes'

export const setParam = (field, value) => {
	return {
		type: Actions.LIFE_SET_PARAM,
		payload: { field, value }
	}
}

export const nextSlide = () => {
	return {
		type: Actions.LIFE_NEXT_SLIDE
	}
}

export const prevSlide = () => {
	return {
		type: Actions.LIFE_PREV_SLIDE
	}
}

export const setForm = form => {
	return {
		type: Actions.LIFE_SET_FORM,
		payload: form
	}
}

export const reset = () => {
	return {
		type: Actions.LIFE_INITIAL
	}
}
