import * as Actions from './actionTypes'

export const setPropertyType = propertyType => {
	return {
		type: Actions.FIRE_PROPERTY_TYPE,
		payload: propertyType
	}
}

export const setBuildingType = buildingType => {
	return {
		type: Actions.FIRE_BUILDING_TYPE,
		payload: buildingType
	}
}

export const setUnitCount = count => {
	return {
		type: Actions.FIRE_UNIT_COUNTS,
		payload: count
	}
}

export const setUnitMeter = meter => {
	return {
		type: Actions.FIRE_UNIT_METER,
		payload: meter
	}
}

export const setValue = value => {
	return {
		type: Actions.FIRE_VALUE,
		payload: value
	}
}

export const nextSlide = () => {
	return {
		type: Actions.FIRE_NEXT_SLIDE
	}
}

export const prevSlide = () => {
	return {
		type: Actions.FIRE_PREV_SLIDE
	}
}

export const setForm = form => {
	return {
		type: Actions.FIRE_SET_FORM,
		payload: form
	}
}

export const setDone = () => {
	return {
		type: Actions.FIRE_SET_DONE
	}
}

export const reset = () => {
	return {
		type: Actions.FIRE_INITIAL
	}
}
