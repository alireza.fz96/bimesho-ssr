import * as Actions from './actionTypes'

export const setBrand = brand => {
    return {
        type: Actions.MOTOR_SET_BRAND,
        payload: brand
    }
}

export const setModel = model => {
    return {
        type: Actions.MOTOR_SET_MODEL,
        payload: model
    }
}

export const nextSlide = () => {
    return {
        type: Actions.MOTOR_NEXT_SLIDE
    }
}

export const prevSlide = () => {
    return {
        type: Actions.MOTOR_PREV_SLIDE
    }
}

export const setProductionYear = year => {
    return {
        type: Actions.MOTOR_SET_PRODUCTION_YEAR,
        payload: year
    }
}

export const setInsuranceDiscount = discount => {
    return {
        type: Actions.MOTOR_INSURANCE_DISCOUNT,
        payload: discount
    }
}

export const setInsuranceStart = start => {
    return {
        type: Actions.MOTOR_INSURANCE_START,
        payload: start
    }
}

export const setInsuranceEnd = end => {
    return {
        type: Actions.MOTOR_INSURANCE_END,
        payload: end
    }
}

export const setLifeDamageCount = count => {
    return {
        type: Actions.MOTOR_LIFE_DAMAGE_COUNT,
        payload: count
    }
}

export const setFinanceDamageCount = count => {
    return {
        type: Actions.MOTOR_FINANCE_DAMAGE_COUNT,
        payload: count
    }
}

export const setDriverAccidentCount = count => {
    return {
        type: Actions.MOTOR_DRIVER_ACCIDENT_COUNT,
        payload: count
    }
}
export const setDriverAccidentDiscount = discount => {
    return {
        type: Actions.MOTOR_DRIVER_ACCIDENT_DISCOUNT,
        payload: discount
    }
}

export const setForm = form => {
    return {
        type: Actions.MOTOR_SET_FORM,
        payload: form
    }
}

export const done = () => {
    return {
        type: Actions.MOTOR_SET_DONE
    }
}

export const initial = isMotorInsurance => {
    return {
        type: Actions.MOTOR_INITIAL,
        payload: isMotorInsurance
    }
}

export const setDatePrice = datePrice => {
    return {
        type: Actions.MOTOR_DATE_PRICE,
        payload: datePrice
    }
}

export const setPrevCompany = prevCompany => {
    return {
        type: Actions.MOTOR_PREV_COMPANY,
        payload: prevCompany
    }
}

export const setNoAccident = () => {
    return {
        type: Actions.MOTOR_NO_ACCIDENT
    }
}
