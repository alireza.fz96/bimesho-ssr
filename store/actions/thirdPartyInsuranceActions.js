import { saveInsurance } from '../../api/ThirdpersonInsuranceApi'
import * as Actions from './actionTypes'

export const setCategory = category => {
	return {
		type: Actions.THIRD_SET_CATEGORY,
		payload: category
	}
}

export const setBrand = brand => {
	return {
		type: Actions.THIRD_SET_BRAND,
		payload: brand
	}
}

export const setModel = model => {
	return {
		type: Actions.THIRD_SET_MODEL,
		payload: model
	}
}

export const nextSlide = () => {
	return {
		type: Actions.THIRD_NEXT_SLIDE
	}
}

export const prevSlide = () => {
	return {
		type: Actions.THIRD_PREV_SLIDE
	}
}

export const setProductionYear = year => {
	return {
		type: Actions.THIRD_SET_PRODUCTION_YEAR,
		payload: year
	}
}

export const setInsuranceDiscount = discount => {
	return {
		type: Actions.THIRD_INSURANCE_DISCOUNT,
		payload: discount
	}
}

export const setInsuranceStart = start => {
	return {
		type: Actions.THIRD_INSURANCE_START,
		payload: start
	}
}

export const setInsuranceEnd = end => {
	return {
		type: Actions.THIRD_INSURANCE_END,
		payload: end
	}
}

export const setLifeDamageCount = count => {
	return {
		type: Actions.THIRD_LIFE_DAMAGE_COUNT,
		payload: count
	}
}

export const setFinanceDamageCount = count => {
	return {
		type: Actions.THIRD_FINANCE_DAMAGE_COUNT,
		payload: count
	}
}

export const setDriverAccidentCount = count => {
	return {
		type: Actions.THIRD_DRIVER_ACCIDENT_COUNT,
		payload: count
	}
}
export const setDriverAccidentDiscount = discount => {
	return {
		type: Actions.THIRD_DRIVER_ACCIDENT_DISCOUNT,
		payload: discount
	}
}

export const setForm = form => {
	return (dispatch, getState) => {
		dispatch({ type: Actions.THIRD_SET_FORM, payload: form })
		// saveInsurance({
		// 	...getState(),
		// 	thirdPartyInsurance: { ...getState().thirdPartyInsurance, form }
		// })
	}
}

export const done = () => {
	return { type: Actions.THIRD_SET_DONE }
}

export const initial = isMotorInsurance => {
	return {
		type: Actions.THIRD_INITIAL,
		payload: isMotorInsurance
	}
}

export const setDatePrice = datePrice => {
	return {
		type: Actions.THIRD_DATE_PRICE,
		payload: datePrice
	}
}

export const setPrevCompany = prevCompany => {
	return {
		type: Actions.THIRD_PREV_COMPANY,
		payload: prevCompany
	}
}

export const setNoAccident = () => {
	return {
		type: Actions.THIRD_NO_ACCIDENT
	}
}
