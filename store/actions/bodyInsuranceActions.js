import * as Actions from './actionTypes'

export const setBrand = brand => {
	return {
		type: Actions.BODY_SET_BRAND,
		payload: brand
	}
}

export const setModel = model => {
	return {
		type: Actions.BODY_SET_MODEL,
		payload: model
	}
}

export const nextSlide = () => {
	return {
		type: Actions.BODY_NEXT_SLIDE
	}
}

export const prevSlide = () => {
	return {
		type: Actions.BODY_PREV_SLIDE
	}
}

export const setProductionYear = year => {
	return {
		type: Actions.BODY_SET_PRODUCTION_YEAR,
		payload: year
	}
}

export const setMainCoverDiscount = (discount, additional) => {
	return dispatch => {
		dispatch({
			type: Actions.BODY_SET_MAIN_COVER_DISCOUNT,
			payload: discount
		})
		additional && dispatch(nextSlide())
	}
}

export const setAdditionalDiscount = (discount, main) => {
	return dispatch => {
		dispatch({
			type: Actions.BODY_SET_ADDITIONAL_COVER_DISCOUNT,
			payload: discount
		})
		main && dispatch(nextSlide())
	}
}

export const setVehicleValue = value => {
	return {
		type: Actions.BODY_SET_VEHICLE_VALUE,
		payload: value
	}
}

export const setVehicleUsage = usage => {
	return {
		type: Actions.BODY_SET_VEHICLE_USAGE,
		payload: usage
	}
}

export const setForm = form => {
	return {
		type: Actions.BODY_SET_FORM,
		payload: form
	}
}

export const done = () => {
	return {
		type: Actions.BODY_SET_DONE
	}
}

export const initial = () => {
	return {
		type: Actions.BODY_INITIAL
	}
}

export const setNew = () => {
	return {
		type: Actions.BODY_NEW
	}
}

export const setDomesticProduction = value => {
	return {
		type: Actions.BODY_SET_DOMESTIC_PRDOCUTION,
		payload: value
	}
}
