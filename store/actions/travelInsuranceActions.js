import * as Actions from './actionTypes'

export const setParam = (field, value, isValid) => {
	return {
		type: Actions.TRAVEL_SET_PARAM,
		payload: { field, value, isValid }
	}
}

export const nextSlide = () => {
	return {
		type: Actions.TRAVEL_NEXT_SLIDE
	}
}

export const prevSlide = () => {
	return {
		type: Actions.TRAVEL_PREV_SLIDE
	}
}

export const setForm = form => {
	return {
		type: Actions.TRAVEL_SET_FORM,
		payload: form
	}
}

export const reset = () => {
	return {
		type: Actions.TRAVEL_INITIAL
	}
}
