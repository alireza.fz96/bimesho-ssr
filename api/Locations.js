import axios from './axios-base'

export const fetchProvinces = () => {
	return axios.post('/request/get-province')
}

export const fetchCities = provinceId => {
	return axios.post('/request/get-city?id=' + provinceId)
}
