import axios from './axios-base'

export const fetchInsurances = body => {
	return axios.post('/insurance/online/request/body', body)
}

export const makeRequestBody = (form, filters) => {
	const {
		category,
		brand,
		model,
		productionYear,
		mainCoverDiscount,
		additionalCoverDiscount,
		vehicleUsage,
		vehicleValue,
		isBrandNew
	} = form
	const { chemical, break_glass, natural_disaster, steal, transit } = filters
	const body = {
		body_car_cate: category.id,
		body_car_model: model.id,
		body_car_name: brand.id,
		body_car_price: vehicleValue,
		body_car_years: +new Date().getFullYear() - productionYear.miladi,
		body_dmg_years: null,
		body_main_dmg_years: mainCoverDiscount.value,
		body_other_dmg_years: additionalCoverDiscount.value,
		break_glass,
		chemical,
		discount_life: 0,
		discount_long_time: 0,
		discount_third_person_company: 0,
		discount_third_person_year: 0,
		good_car: false,
		insurance_id: null,
		is_cash: 1,
		life: null,
		natural_disaster,
		nouse: false,
		price: false,
		price_down: null,
		sort: null,
		status_installment: null,
		steal,
		third: null,
		third_dmg_years: null,
		transit,
		usein: vehicleUsage.value,
		zero: null,
		zero_car: isBrandNew.value ? 1 : 0
	}
	return body
}
