import axios from './axios-base'

export const fetchInsurances = body => {
	return axios.post('/insurance/online/request/travel', body)
}

export const makeRequestBody = form => {
	const { country, stayingTime, age } = form
	const body = {
		travel_country: country.id,
		travel_time: stayingTime.value,
		travel_age: age.value
	}
	return body
}
