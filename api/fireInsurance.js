import axios from './axios-base'

export const fetchInsurances = body => {
	return axios.post('/insurance/online/request/fire', body)
}

export const makeRequestBody = (form, filters) => {
	const { propertyType, buildingType, unitCount, unitMeter, value } = form
	const body = {
		fire_home_type: propertyType.value,
		fire_home_count: unitCount.value,
		fire_structure: buildingType.value,
		fire_home_price: value,
		fire_meters: unitMeter.value,
		area_price: 1.1,
		...filters,
		sort: null
	}
	return body
}
