import axios from './axios-base'
import jmoment from 'moment-jalaali'

export const fetchCatCars = async () => {
	const data = await axios.get('/request/get-cat-cars')
	return data.data.data
}

export const fetchKindCars = async url => {
	const data = await axios.get(url)
	return data.data.data
}

export const fetchModelCars = kindId => {
	return axios.get('/request/get-model-cars/' + kindId)
}

export const fetchInsurances = body => {
	return axios.post('/insurance/online/request/thirdperson', body)
}

export const saveInsurance = state => {
	if (state.user.user) {
		const body = makeStoreRequestBody(state)
		return axios.post('/insurance/online/request/thirdperson/store', body)
	}
}

export const makeStoreRequestBody = state => {
	const {
		category,
		brand,
		model,
		productionYear,
		insuranceDiscount,
		insuranceStart,
		insuranceEnd,
		lifeDamageCount,
		financeDamageCount,
		driverAccidentDamageCount,
		driverAccidentDamageDiscount,
		datePrice,
		prevCompany
	} = state.thirdPartyInsurance.form
	const { mobile, address, city, province } = state.product.form
	const userAddress = state.product.address
	const body = {
		third_car_cate: category.id,
		third_car_name: brand.id,
		third_car_model: model.id,
		third_car_years: +new Date().getFullYear() - productionYear.miladi,
		car_dmg_percent: (insuranceDiscount && insuranceDiscount.value) || 0,
		car_dmg_life: (lifeDamageCount && lifeDamageCount.value) || 0,
		car_dmg_finance: (financeDamageCount && financeDamageCount.value) || 0,
		driver_dmg_percent:
			(driverAccidentDamageDiscount &&
				driverAccidentDamageDiscount.value) ||
			0,
		driver_dmg_count:
			(driverAccidentDamageCount && driverAccidentDamageCount.value) || 0,
		insurance_id: null,
		date_price: jmoment(datePrice || new Date())
			.locale('en')
			.format('jYYYY-jM-jD'),
		prev_company: prevCompany,
		date_start_third:
			insuranceStart &&
			jmoment(insuranceStart).locale('en').format('jYYYY-jM-jD'),
		date_end_third: jmoment(insuranceEnd || '')
			.locale('en')
			.format('jYYYY-jM-jD'),
		usein: 1,
		short_term: 365,
		commitments:
			state.product.product &&
			state.product.product.params &&
			state.product.product.params.length > 0 &&
			state.product.product.params[0].value,
		mobile,
		address,
		city: city && city.id,
		province: province && province.id,
		user_id: state.user.user && state.user.user.data.id,
		insurance_type: 'third_person',
		final_price:
			state.product.product && state.product.product.data.final_price,
		url: 'https://bimesho-ssr.vercel.app/payment',
		insurance_id: state.product.product && state.product.product.data.id,
		order_type:
			state.product.product && state.product.product.paymentMethod,
		json1: makeRequestBody(state.thirdPartyInsurance.form),
		json2: state.product.product && state.product.product.data,
	}
	return body
}

export const makeRequestBody = (form, commitments) => {
	const {
		category,
		brand,
		model,
		productionYear,
		insuranceDiscount,
		insuranceStart,
		insuranceEnd,
		lifeDamageCount,
		financeDamageCount,
		driverAccidentDamageCount,
		driverAccidentDamageDiscount,
		datePrice,
		prevCompany
	} = form
	const body = {
		third_car_cate: category.id,
		third_car_name: brand.id,
		third_car_model: model.id,
		third_car_years: +new Date().getFullYear() - productionYear.miladi,
		car_dmg_percent: insuranceDiscount && insuranceDiscount.value,
		car_dmg_life: lifeDamageCount && lifeDamageCount.value,
		car_dmg_finance: financeDamageCount && financeDamageCount.value,
		driver_dmg_percent:
			driverAccidentDamageDiscount && driverAccidentDamageDiscount.value,
		driver_dmg_count:
			driverAccidentDamageCount && driverAccidentDamageCount.value,
		insurance_id: null,
		date_price: jmoment(datePrice || new Date())
			.locale('en')
			.format('jYYYY-jM-jD'),
		prev_company: prevCompany,
		date_start_third:
			insuranceStart &&
			jmoment(insuranceStart).locale('en').format('jYYYY-jM-jD'),
		date_end_third:
			insuranceEnd &&
			jmoment(insuranceEnd).locale('en').format('jYYYY-jM-jD'),
		// date_start_third: '1399-6-29',
		// date_end_third: '1399-6-30',
		usein: 1,
		short_term: 365,
		commitments
	}
	return body
}
