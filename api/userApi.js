import axios from './axios-base'

export const login = mobile => {
	return axios.get(`/login?mobile=${mobile}`)
}

export const signup = mobile => {
	return axios.get(`/signup?mobile=${mobile}`)
}

export const verifyCode = (mobile, code) => {
	return axios.post(`/verify?mobile=${mobile}&code=${code}`)
}

export const resendCode = mobile => {
	return axios.get(`/resend-code-otp?mobile=${mobile}`)
}

export const discountCode = (code, invoice_id) => {
	// 	return new Promise((resolutionFunc, rejectionFunc) => {
	// 	resolutionFunc({
	// 		data: {
	// 			discount: '400000',
	// 			final_price: '300000',
	// 			message: 'Code Success'
	// 		}
	// 	})
	// })
	return axios.post('/insurance/invoice/discount', {
		discount_code: code,
		invoice_id: invoice_id || 687
	})
}

export const removeCode = code => {
	return axios.post('/insurance/invoice/discount/remove', {
		discount_code: code,
		invoice_id: 687
	})
}

export const handleLoginErrors = statusCode => {
	switch (statusCode) {
		case 404:
			return 'فردی با این شماره همراه ثبت نام نکرده است. لطفا ابتدا ثبت نام کنید.'
		default:
			return 'یک خطای ناشناخته پیش آمد. لطفا بعدا دوباره تلاش کنید'
	}
}

export const handleVerfiyErrors = statusCode => {
	switch (statusCode) {
		case 500:
			return 'کد اشتباه است. لطفا دوباره تلاش کنید.'
		default:
			return 'یک خطای ناشناخته پیش آمد. لطفا بعدا دوباره تلاش کنید'
	}
}
