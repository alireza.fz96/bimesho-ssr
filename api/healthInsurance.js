import axios from './axios-base'

export const fetchInsurances = body => {
	return axios.post('/insurance/online/request/health', body)
}

export const makeRequestBody = form => {
	const { applicants, baseInsurance } = form
	const body = {
		age: 1,
		type: 1,
		insurancer: baseInsurance.value,
		...applicants
	}
	return body
}
