import axios from './axios-base'

export const fetchInsurances = body => {
	return axios.post('/insurance/online/request/life', body)
}

export const makeRequestBody = form => {
	const { insuranceRight, paymentMethod, birthday, contractTime } = form
	const body = {
		life_cost: insuranceRight && insuranceRight.value,
		life_pay: paymentMethod && paymentMethod.value,
		life_years: contractTime && contractTime.value,
		brith_years: birthday && birthday.jalali
	}
	return body
}
