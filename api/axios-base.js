import axios from 'axios'

const instance = axios.create({
	baseURL: 'https://bimesho.com/api/front/insurance/ver2'
})

export default instance
export const fetcher = url => instance.get(url).then(res => res.data.data)
export const poster = (url, body) =>
	instance.post(url, body).then(res => res.data.data)
