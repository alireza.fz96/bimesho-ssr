import React from 'react'
import { useSelector, useDispatch } from 'react-redux'
import {
	setVehicleUsage,
	setVehicleValue,
	done
} from '../../../store/actions/bodyInsuranceActions'
import { TextField, Button, CircularProgress } from '@material-ui/core'
import { useRouter } from 'next/router'
import Select from '../../UI/Select/Select'
import {
	fromSeparetedNumber,
	separateNumbers
} from '../../../utilities/utilities'

const CoverDiscount = () => {
	const state = useSelector(state => {
		const { vehicleValue, vehicleUsage } = state.bodyInsurance.form
		const { vehicleUsages } = state.coreData
		return {
			value: vehicleValue,
			usage: vehicleUsage,
			vehicleUsages
		}
	})
	const dispatch = useDispatch()
	const router = useRouter()

	const onSetVehicleUsage = usage => {
		dispatch(setVehicleUsage(usage))
	}

	const onSetVehicleValue = value => {
		dispatch(setVehicleValue(value))
	}

	const navigateTo = () => {
		dispatch(done())
		setEnquiryLoading(true)
		router.push('/body-enquiry').finally(() => {
			setEnquiryLoading(false)
		})
	}

	const isValidPrice =
		(state.value > 1000000 && state.value < 9000000000) || !state.value

	const [enquiryLoading, setEnquiryLoading] = React.useState(false)

	return (
		<div className="input-wrapper d-flex flex-column">
			<Select
				id="vehicle-usage-label"
				label="کاربری خودرو"
				changed={onSetVehicleUsage}
				renderer={value => value.name}
				value={state.usage || ''}
				options={state.vehicleUsages}
			/>

			<div className="mt-4">
				<TextField
					variant="outlined"
					color="primary"
					value={separateNumbers(state.value)}
					onChange={event => {
						const newValue = fromSeparetedNumber(event.target.value)
						if (+newValue || !newValue) onSetVehicleValue(newValue)
					}}
					label="ارزش خودرو (تومان)"
					placeholder="ارزش خودرو (تومان)"
					error={!isValidPrice}
					helperText={
						isValidPrice
							? ''
							: 'ارزش خودرو باید بین ۱،۰۰۰،۰۰۰ و ۹،۰۰۰،۰۰۰،۰۰۰ باشد'
					}
				/>
			</div>
			<div className="mt-4">
				{enquiryLoading ? (
					<div className="w-100 text-center">
						<CircularProgress />
					</div>
				) : (
					<Button
						variant="contained"
						color="primary"
						fullWidth
						onClick={navigateTo}
						disabled={!state.usage || !state.value || !isValidPrice}
					>
						استعلام
					</Button>
				)}
			</div>
		</div>
	)
}

export default CoverDiscount
