import React from 'react'
import { useSelector, useDispatch } from 'react-redux'
import {
	setMainCoverDiscount,
	setAdditionalDiscount
} from '../../../store/actions/bodyInsuranceActions'
import Select from '../../UI/Select/Select'

const CoverDiscount = () => {
	const state = useSelector(state => {
		const {
			mainCoverDiscount,
			additionalCoverDiscount
		} = state.bodyInsurance.form
		const { additionalCoverDiscounts, mainCoverDiscounts } = state.coreData
		return {
			main: mainCoverDiscount,
			additional: additionalCoverDiscount,
			mainCoverDiscounts,
			additionalCoverDiscounts
		}
	})
	const dispatch = useDispatch()

	const onSetMainCoverDiscount = discount => {
		dispatch(setMainCoverDiscount(discount, state.additional))
	}

	const onSetAdditionalCoverDiscount = discount => {
		dispatch(setAdditionalDiscount(discount, state.main))
	}

	return (
		<div className="input-wrapper d-flex flex-column">
			<Select
				changed={onSetAdditionalCoverDiscount}
				id="additional-cover-discount-label"
				value={state.additional || ''}
				renderer={value => value.name}
				options={state.additionalCoverDiscounts}
				label="تخفیف عدم خسارت پوشش های اضافی"
			/>
			<div className="mt-4">
				<Select
					changed={onSetMainCoverDiscount}
					id="main-cover-discount-label"
					value={state.main || ''}
					renderer={value => value.name}
					options={state.mainCoverDiscounts}
					label="تخفیف عدم خسارت پوشش های اصلی"
				/>
			</div>
		</div>
	)
}

export default CoverDiscount
