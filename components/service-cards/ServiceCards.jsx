import React from 'react'
import ServiceCard from './service-card/ServiceCard'
import classes from './ServiceCards.module.scss'

const services = [
	{
		icon: '/imgs/service1.png',
		title: 'مشاوره تخصصی بیمه',
		text:
			'ارائه مشاوره تخصصی، کاملا بی طرفانه و رایگان توسط کارشناسان بیمه ای ازکی'
	},
	{
		icon: '/imgs/service2.png',
		title: 'مقایسه قیمت و شرایط بیمه',
		text: 'مقایسه قیمت، خدمات و پوشش ها برای انتخاب بهترین بیمه'
	},
	{
		icon: '/imgs/service3.png',
		title: 'خرید سریع و آسان',
		text: 'ارسال فوری در تهران و مراکز استان ها بصورت کاملا رایگان'
	},
	{
		icon: '/imgs/service4.png',
		title: 'پرداخت آسان و مطمئن',
		text:
			'امکان پرداخت حق بیمه به سه روش آنلاین، کارت به کارت و پرداخت در محل'
	}
]

const ServiceCards = () => {
	return (
		<div className={classes.serviceCards}>
			<div className="row">
				{services.map(service => (
					<div
						className={
							'col-12 col-sm-6 col-lg-3 ' + classes.serviceCard
						}
						key={service.title}
					>
						<ServiceCard service={service} />
					</div>
				))}
			</div>
		</div>
	)
}

export default ServiceCards
