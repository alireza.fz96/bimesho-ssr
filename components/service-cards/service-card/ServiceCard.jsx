import React from 'react'
import classes from './ServiceCard.module.scss'

const ServiceCard = ({ service }) => {
	return (
		<div className={classes.service}>
			<img
				src={service.icon}
				alt="service"
				className={classes.service__img}
			/>
			<h3 className={classes.service__title}>{service.title}</h3>
			<p className={classes.service__text}>{service.text}</p>
		</div>
	)
}

export default ServiceCard
