import React, { useState, useEffect } from 'react'
import OrderSteps from '../../components/order-steps/OrderSteps'
import {
	Button,
	TextField,
	FormControlLabel,
	RadioGroup,
	Radio,
	Collapse,
	FormControl,
	InputLabel,
	Select,
	MenuItem,
	FormHelperText,
	CircularProgress
} from '@material-ui/core'
import { DatePicker } from '@material-ui/pickers'
import { useForm, Controller } from 'react-hook-form'
import { useCities, useProvinces } from '../../hooks/locations'
import {
	mobileRegx,
	phoneRegex,
	toEnglishDigit,
	vmsNationalCode
} from '../../utilities/utilities'
import { useDispatch, useSelector } from 'react-redux'
import { setProductForm } from '../../store/actions/productActions'
import { useRouter } from 'next/router'

const imageFormats = ['jpeg', 'png', 'jpg']

const OrderForm = ({ photos }) => {
	const state = useSelector(state => ({
		form: state.product.form,
		user: state.user
	}))

	const [buyer, setBuyer] = useState(state.form.buyer)

	const {
		watch,
		register,
		errors,
		handleSubmit,
		setValue,
		control,
		clearErrors,
		trigger,
		reset
	} = useForm({
		defaultValues: {
			name: ' ',
			address: ' ',
			birthday: null,
			phone: ' ',
			mobile: ' ',
			// city: ' ',
			// province: ' ',
			gender: ' ',
			national_code: ' '
		}
	})

	const setUserFields = () => {
		if (!state.form.name || state.form.buyer !== 'self') {
			const {
				fname,
				lname,
				mobile,
				phone,
				birthday,
				province,
				city,
				sex,
				national_id
			} = state.user.user.data
			const name = fname ? fname + ' ' + lname : ''
			reset({
				name,
				mobile,
				phone,
				birthday,
				province,
				city,
				gender: sex,
				national_code: national_id || ''
			})
		} else {
			setFields()
		}
	}

	const setOtherFields = () => {
		if (state.form.buyer === 'other') setFields()
		else reset({ gender: '' })
	}

	const setFields = React.useCallback(() => {
		const {
			name,
			address,
			birthday,
			phone,
			mobile,
			city,
			province,
			gender,
			national_code
		} = state.form
		reset({
			name: name,
			address,
			birthday,
			phone,
			mobile,
			city,
			province,
			gender,
			national_code
		})
	}, [state.form, reset])

	React.useEffect(() => {
		if (state.form.name) setFields()
	}, [reset, state.form, setFields])

	const dispatch = useDispatch()

	const readImage = (event, func) => {
		const input = event.target
		const reader = new FileReader()
		reader.onload = function () {
			const dataURL = reader.result
			func(dataURL)
		}
		if (input.files[0]) {
			const fileType = input.files[0].name.split('.').pop().toLowerCase()
			const isImageFile = ~imageFormats.indexOf(fileType)
			if (isImageFile) reader.readAsDataURL(input.files[0])
		}
	}

	const selectedProvince = watch('province')

	const { provinces } = useProvinces()
	const { cities } = useCities(selectedProvince && selectedProvince.id)

	const router = useRouter()

	const { province } = state.form

	useEffect(() => {
		if (selectedProvince !== province || !province) {
			setValue('city', null)
		}
	}, [selectedProvince, setValue, province])

	const onSubmit = data => {
		const form = { ...data, buyer }
		photos.map(photo => (form[photo.name] = photo.image))
		dispatch(setProductForm(form))
		setAddressFormLoading(true)
		router.push('/order-address').finally(() => {
			setAddressFormLoading(false)
		})
	}
	
	photos.forEach(photo => {
		if (photo.image && errors[photo.name]) clearErrors(photo.name)
	})

	const getErrorMessage = (error, label) => {
		if (error.type) {
			if (error.type === 'required')
				return `لطفا ${label} خود را وارد کنید`
			if (error.type === 'pattern' || error.type === 'validate')
				return `${label} صحیح نمیباشد`
		}
		return ''
	}

	const onSetNumberField = (field, value, length) => {
		const newValue = toEnglishDigit(value)
		setValue(field, newValue)
		if (value.length === length) trigger(field)
	}

	const isValidNationalCode = value => {
		const newValue = toEnglishDigit(value)
		if (value.length === 10 && vmsNationalCode(newValue)) {
			return true
		} else return false
	}

	const [addressFormLoading, setAddressFormLoading] = useState(false)

	return (
		<div style={{ fontSize: 14 }}>
			<div className="container my-2 px-md-5">
				<OrderSteps step={1} />
			</div>
			<div className="mb-5 container">
				<div className="content-wrapper">
					<form
						className="text-center"
						onSubmit={handleSubmit(onSubmit)}
					>
						<h2 className="section-title mb-3">ارسال مدارک</h2>
						<div className="row justify-content-center min-height-200">
							{photos.map(photo => (
								<div
									className="col-sm-4 col-md-3"
									key={photo.name}
								>
									<div className="uploader-card">
										<input
											type="file"
											className="uploader-card__input"
											onChange={event =>
												readImage(event, data => {
													setValue(photo.name, data)
													photo.setImage(data)
												})
											}
											accept=".png,.jpeg,.jpg"
										/>
										<input
											name={photo.name}
											ref={register({
												required: photo.required
											})}
											type="text"
											style={{ display: 'none' }}
											defaultValue={photo.image}
										/>
										<div className="position-relative">
											<div className="uploader-card__image">
												<img
													src={
														photo.image ||
														'/imgs/svg/image-upload-ic.svg'
													}
													alt="upload_photo"
													className={
														photo.image
															? ''
															: 'uploader-card__image--no-image'
													}
												/>
											</div>
											{photo.image && (
												<div className="uploader-card__cover">
													<img
														src="/imgs/svg/upload-again-ic.svg"
														alt="upload_again_photo"
													/>
												</div>
											)}
										</div>
										<div>{photo.label}</div>
										{errors[photo.name] && (
											<span className="text-danger">
												لطفا {photo.label} را انتخاب
												کنید
											</span>
										)}
									</div>
								</div>
							))}
						</div>
						<h2 className="section-title">مشخصات</h2>
						<div className="insurance-info">
							<div className="mb-3">
								بیمه نامه را برای چه کسی خریداری میکنید ؟
							</div>
							<div className="row mb-4">
								<div className="col-xs-12 col-sm-6 col-md-4 mt-2 col-md-6">
									<Button
										variant="contained"
										color={
											buyer === 'self'
												? 'primary'
												: 'default'
										}
										onClick={() => {
											setBuyer('self')
											setUserFields()
										}}
										fullWidth
									>
										برای خودم
									</Button>
								</div>
								<div className="col-xs-12 col-sm-6 col-md-4 mt-2 col-md-6">
									<Button
										variant="contained"
										color={
											buyer === 'other'
												? 'primary'
												: 'default'
										}
										onClick={() => {
											setBuyer('other')
											setOtherFields()
										}}
										fullWidth
									>
										برای دیگری
									</Button>
								</div>
							</div>
							<Collapse
								in={buyer || Object.keys(errors).length !== 0}
							>
								<>
									<p>
										لطفا مشخصات را وارد کنید. این اطلاعات بر
										روی بیمه‌نامه شما درج خواهد شد
									</p>
									<div className="my-3">
										<TextField
											variant="outlined"
											color="primary"
											label="نام و نام خانوادگی"
											placeholder="نام و نام خانوادگی"
											name="name"
											inputRef={register({
												required: true
											})}
											error={!!errors.name}
											helperText={
												errors.name
													? 'لطفا نام و نام خانوادگی را تعیین کنید'
													: ''
											}
										/>
									</div>
									<div className="my-3">
										<TextField
											variant="outlined"
											color="primary"
											label="کد ملی"
											placeholder="کد ملی"
											name="national_code"
											inputRef={register({
												required: true,
												validate: value =>
													isValidNationalCode(value)
											})}
											error={!!errors.national_code}
											helperText={
												errors.national_code
													? getErrorMessage(
															errors.national_code,
															'کد ملی'
													  )
													: ''
											}
											// onChange={event =>
											// 	isValidNationalCode(
											// 		event.target.value
											// 	)
											// }
										/>
									</div>
									<div className="my-3">
										<Controller
											render={props => (
												<DatePicker
													labelFunc={date =>
														date
															? date.format(
																	'jYYYY/jMM/jDD'
															  )
															: ''
													}
													variant="inline"
													inputVariant="outlined"
													label="تاریخ تولد"
													value={props.value}
													onChange={date => {
														props.onChange(date)
													}}
													error={!!errors.birthday}
													helperText={
														errors.birthday
															? 'لطفا سال تولد را تعیین کنید'
															: ''
													}
													autoOk
												/>
											)}
											control={control}
											name="birthday"
											defaultValue={null}
											rules={{ required: true }}
										/>
									</div>
									<div className="my-3">
										<TextField
											variant="outlined"
											color="primary"
											label="شماره موبایل"
											placeholder="مثال: ۰۹۱۰۱۲۳۴۵۶۷"
											name="mobile"
											inputRef={register({
												required: true,
												pattern: mobileRegx
											})}
											error={!!errors.mobile}
											helperText={
												errors.mobile
													? getErrorMessage(
															errors.mobile,
															'شماره موبایل'
													  )
													: ''
											}
											onChange={event =>
												onSetNumberField(
													'mobile',
													event.target.value,
													11
												)
											}
										/>
									</div>
									<div className="my-3">
										<TextField
											variant="outlined"
											color="primary"
											label="تلفن ثابت"
											placeholder="مثال:‌ ۰۲۱۹۱۰۰۲۰۰۸"
											name="phone"
											inputRef={register({
												pattern: phoneRegex
											})}
											error={!!errors.phone}
											helperText={
												errors.phone
													? getErrorMessage(
															errors.phone,
															'تلفن ثابت'
													  )
													: ''
											}
											onChange={event =>
												onSetNumberField(
													'phone',
													event.target.value,
													11
												)
											}
										/>
									</div>
									<div className="my-3">
										<Controller
											as={
												<RadioGroup aria-label="gender">
													<FormControlLabel
														value="male"
														control={
															<Radio color="primary" />
														}
														label="مرد"
													/>
													<FormControlLabel
														value="female"
														control={
															<Radio color="primary" />
														}
														label="زن"
													/>
												</RadioGroup>
											}
											name="gender"
											control={control}
											rules={{
												required: true,
												validate: value =>
													value === 'male' ||
													value === 'female'
											}}
										/>
										{errors.gender && (
											<span className="text-danger">
												لطفا جنسیت را تعیین کنید
											</span>
										)}
									</div>
								</>
							</Collapse>
							<div className="py-3">
								لطفا آدرسی که میخواهید بر روی بیمه‌نامه درج شود
								را وارد کنید
							</div>
							<div className="row">
								<div className="col-xs-12 col-sm-6 col-md-4 mt-2 col-md-6">
									<Controller
										render={props => (
											<FormControl variant="outlined">
												<InputLabel>استان</InputLabel>
												<Select
													label="استان"
													value={props.value || ''}
													onChange={event => {
														props.onChange(
															event.target.value
														)
													}}
													renderValue={value =>
														value.name
													}
													error={errors.province}
												>
													{provinces.map(
														(option, index) => (
															<MenuItem
																value={option}
																key={index}
															>
																{option.name}
															</MenuItem>
														)
													)}
												</Select>
												{errors.province && (
													<FormHelperText
														error={errors.province}
													>
														لطفا استان را وارد کنید
													</FormHelperText>
												)}
											</FormControl>
										)}
										name="province"
										control={control}
										rules={{ required: true }}
									/>
								</div>
								<div className="col-xs-12 col-sm-6 col-md-4 mt-2 col-md-6">
									<Controller
										render={props => (
											<FormControl variant="outlined">
												<InputLabel>شهر</InputLabel>
												<Select
													label="شهر"
													value={props.value || ''}
													onChange={event => {
														props.onChange(
															event.target.value
														)
													}}
													renderValue={value =>
														value.name
													}
													error={errors.city}
													disabled={!selectedProvince}
												>
													{cities.map(
														(option, index) => (
															<MenuItem
																value={option}
																key={index}
															>
																{option.name}
															</MenuItem>
														)
													)}
												</Select>
												{errors.city && (
													<FormHelperText
														error={errors.city}
													>
														لطفا شهر را وارد کنید
													</FormHelperText>
												)}
											</FormControl>
										)}
										name="city"
										control={control}
										rules={{ required: true }}
									/>
								</div>
							</div>
							<div className="my-3">
								<TextField
									multiline
									variant="outlined"
									color="primary"
									label="آدرس دقیق"
									placeholder="آدرس دقیق"
									name="address"
									inputRef={register({
										required: true
									})}
									error={!!errors.address}
									helperText={
										errors.address
											? 'لطفا ادرس را تعیین کنید'
											: ''
									}
								/>
							</div>
						</div>

						<div className="mt-5">
							{addressFormLoading ? (
								<div className="w-100 text-center">
									<CircularProgress />
								</div>
							) : (
								<Button
									color="primary"
									variant="contained"
									type="submit"
									disabled={!buyer}
								>
									تایید و ادامه
								</Button>
							)}
						</div>
					</form>
				</div>
			</div>
		</div>
	)
}

export default OrderForm
