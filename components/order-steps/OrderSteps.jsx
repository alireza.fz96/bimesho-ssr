import React from 'react'
import Stepper from '@material-ui/core/Stepper'
import Step from '@material-ui/core/Step'
import StepLabel from '@material-ui/core/StepLabel'
import { useSelector } from 'react-redux'
import { makeStyles } from '@material-ui/core'
const useStyle = makeStyles({
	root: {
		'& .MuiPaper-root': {
			backgroundColor: 'unset'
		}
	}
})

const OrderSteps = ({ step }) => {
	const classes = useStyle()

	const steps = useSelector(state => state.coreData.orderSteps)

	return (
		<div className={classes.root}>
			<Stepper activeStep={step}>
				{steps.map(label => (
					<Step key={label}>
						<StepLabel>{label}</StepLabel>
					</Step>
				))}
			</Stepper>
		</div>
	)
}

export default OrderSteps
