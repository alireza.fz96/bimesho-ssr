import { Button, CircularProgress } from '@material-ui/core'
import React from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { setParam } from '../../../store/actions/healthInsuranceActions'
import { useRouter } from 'next/router'
import Actions from '../../actions/Actions'

const Applicants = () => {
	const state = useSelector(state => {
		return {
			applicants: state.healthInsurance.form.applicants,
			applicantAges: state.coreData.applicantAges
		}
	})
	const dispatch = useDispatch()

	const onAddApplicant = key => {
		const newApplicatns = { ...state.applicants }
		newApplicatns[key]++
		dispatch(setParam('applicants', newApplicatns))
	}

	const onSubtractApplicant = key => {
		const newApplicatns = { ...state.applicants }
		if (newApplicatns[key] > 0) {
			newApplicatns[key]--
			dispatch(setParam('applicants', newApplicatns))
		}
	}

	const router = useRouter()

	const onCheckEnqiury = () => {
		dispatch(setParam('done', true))
		setEnquiryLoading(true)
		router.push('/health-enquiry').finally(() => {
			setEnquiryLoading(false)
		})
	}

	const canCheck = () => {
		for (let age in state.applicants) {
			if (state.applicants[age] > 0) return true
		}
		return false
	}

	const [enquiryLoading, setEnquiryLoading] = React.useState(false)

	return (
		<div className="text-center">
			<h1 className="mb-4">به ازای هر فرد یک عدد اضافه کنید</h1>
			<Actions
				items={state.applicantAges}
				values={state.applicants}
				onAdd={onAddApplicant}
				onRemove={onSubtractApplicant}
			/>
			{canCheck() &&
				(enquiryLoading ? (
					<div className="w-100 text center mt-4">
						<CircularProgress />
					</div>
				) : (
					<Button
						color="primary"
						variant="contained"
						onClick={onCheckEnqiury}
						className="mt-4"
					>
						استعلام
					</Button>
				))}
		</div>
	)
}

export default Applicants
