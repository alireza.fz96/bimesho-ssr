import React from 'react'
import { useSelector, useDispatch } from 'react-redux'
import {
	nextSlide,
	setParam
} from '../../../store/actions/healthInsuranceActions'
import Select from '../../UI/Select/Select'

const BaseInsurance = () => {
	const state = useSelector(state => {
		const baseInsurance = state.healthInsurance.form.baseInsurance
		const baseInsurances = state.coreData.baseInsurances
		return {
			baseInsurance,
			baseInsurances
		}
	})
	const dispatch = useDispatch()

	const onSetParam = baseInsurance => {
		dispatch(setParam('baseInsurance', baseInsurance))
		dispatch(nextSlide())
	}

	return (
		<div className="input-wrapper">
			<Select
				label="بیمه پایه"
				id="base-insurance"
				changed={onSetParam}
				options={state.baseInsurances}
				value={state.baseInsurance}
				renderer={option => option.name}
			/>
		</div>
	)
}

export default BaseInsurance
