import React from 'react'
import { useDispatch, useSelector } from 'react-redux'
import {
	setCategory,
	nextSlide
} from '../../store/actions/thirdPartyInsuranceActions'
import { CircularProgress } from '@material-ui/core'
import { useCategories } from '../../hooks/vehicles'

const VehicleTypes = () => {
	const state = useSelector(state => state.thirdPartyInsurance.form.category)
	const dispatch = useDispatch()
	const { categories, isLoading } = useCategories()

	const onSetVehicleCategory = category => {
		dispatch(setCategory(category))
		dispatch(nextSlide())
	}

	return (
		<div className="grid-view">
			{isLoading && (
				<div className="d-flex justify-content-center w-100">
					<CircularProgress size={60} />
				</div>
			)}
			{!isLoading &&
				categories.map(vehicle => (
					<div
						key={vehicle.name}
						className="grid-view__wrapper col-4 col-md-4 col-lg-3 col-xl-3"
					>
						<div
							className={
								'grid-view__item ' +
								(state && vehicle.id === state.id
									? 'active'
									: '')
							}
							onClick={() => onSetVehicleCategory(vehicle)}
						>
							<img
								src={`/imgs/categories/${vehicle.icon}.svg`}
								alt={vehicle.name + 'logo'}
								className="grid-view__icon"
							/>
							<span className="grid-view__title">
								{vehicle.name}
							</span>
						</div>
					</div>
				))}
		</div>
	)
}

export default VehicleTypes
