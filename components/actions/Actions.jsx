import React from 'react'
import { Add, Remove } from '@material-ui/icons'

const Actions = ({ items, onAdd, onRemove,values }) => {
	return (
		<div className="input-wrapper actions">
			{items.map(item => (
				<div className="row mb-3" key={item.value}>
					<div className="col-6">
						<div className="actions__title">{item.name}</div>
					</div>
					<div className="col-6 d-flex">
						<div className="actions__btn">
							<Add onClick={() => onAdd(item.value)} />
						</div>
						<div className="actions__label">
							{values[item.value]}
						</div>
						<div
							className={[
								'actions__btn',
								values[item.value] === 0
									? 'actions__disabled'
									: ''
							].join(' ')}
						>
							<Remove onClick={() => onRemove(item.value)} />
						</div>
					</div>
				</div>
			))}
		</div>
	)
}

export default Actions
