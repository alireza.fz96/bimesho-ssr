import React from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { setParam } from '../../../store/actions/travelInsuranceActions'
import Select from '../../UI/Select/Select'
import { useRouter } from 'next/router'
import { Button, CircularProgress } from '@material-ui/core'

const PassengerAge = () => {
	const state = useSelector(state => {
		const age = state.travelInsurance.form.age
		const ages = state.coreData.ages
		return {
			age,
			ages
		}
	})
	const dispatch = useDispatch()

	const router = useRouter()

	const onSetParam = age => {
		dispatch(setParam('age', age))
		onCheckEnquiry()
	}

	const onCheckEnquiry = () => {
		dispatch(setParam('done', true))
		setEnquiryLoading(true)
		router.push('/travel-enquiry').finally(() => {
			setEnquiryLoading(false)
		})
	}

	const [enquiryLoading, setEnquiryLoading] = React.useState(false)

	return (
		<div className="input-wrapper">
			<Select
				label="سن مسافر"
				id="passenger-age"
				changed={onSetParam}
				options={state.ages}
				value={state.age}
				renderer={option => option.name}
			/>
			{enquiryLoading ? (
				<div className="w-100 text-center mt-4">
					<CircularProgress />
				</div>
			) : (
				<Button
					color="primary"
					variant="contained"
					onClick={onCheckEnquiry}
					className="mt-5"
					disabled={!state.age}
					fullWidth
				>
					استعلام
				</Button>
			)}
		</div>
	)
}

export default PassengerAge
