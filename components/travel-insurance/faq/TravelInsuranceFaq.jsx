import React from 'react'
import About from './sections/About'
import Questions from './sections/Questions'
import ConsultationRequest from '../../consultation-request/ConsultationRequest'
import FaqTabs from '../../faq-tabs/FaqTabs'
import TabPanel from '../../UI/TabPanel/TabPanel'

const TravelInsuranceFaq = () => {
	const [page, setPage] = React.useState(0)

	const changePage = (_, newValue) => {
		setPage(newValue)
	}

	return (
		<div className="faq container-fluid container-lg">
			<FaqTabs
				changePage={changePage}
				page={page}
				title="بیمه مسافرتی چیست ؟"
				header="درباره بیمه مسافرتی"
			/>
			<div className="contentBox">
				<TabPanel value={page} index={0}>
					<div className="about">
						<About value={page} index={0} />
					</div>
				</TabPanel>
				<TabPanel value={page} index={1}>
					<Questions />
				</TabPanel>
				<TabPanel value={page} index={2}>
					<ConsultationRequest />
				</TabPanel>
			</div>
		</div>
	)
}

export default TravelInsuranceFaq
