import React from 'react'
import { useSelector, useDispatch } from 'react-redux'
import Select from '../../UI/Select/Select'
import {
    nextSlide,
    setParam
} from '../../../store/actions/travelInsuranceActions'
import { Autocomplete } from '@material-ui/lab'
import { CircularProgress, TextField } from '@material-ui/core'
import { useCountries } from '../../../hooks/countries'

const Destination = () => {
    const state = useSelector(state => ({
        country: state.travelInsurance.form.country,
        stayingTime: state.travelInsurance.form.stayingTime,
        stayingTimes: state.coreData.stayingTimes
    }))
    const dispatch = useDispatch()

    const { countries, isLoading } = useCountries()

    const onSetParam = (field, value) => {
        const param = field === 'country' ? 'stayingTime' : 'country'
        dispatch(setParam(field, value, value && state[param]))
        if (value && state[param]) dispatch(nextSlide())
    }

    return (
        <div className="input-wrapper d-flex flex-column">
            <div className="mb-4">
                {isLoading ? (
                    <div className="text-center">
                        <CircularProgress />
                    </div>
                ) : (
                    <Autocomplete
                        options={countries}
                        getOptionLabel={option => option.title || ''}
                        value={state.country}
                        closeIcon={false}
                        onChange={(_, value) => onSetParam('country', value)}
                        renderInput={params => (
                            <TextField
                                {...params}
                                label="مقصد"
                                variant="outlined"
                                autoFocus
                            />
                        )}
                    />
                )}
            </div>
            <Select
                label="مدت اقامت"
                id="staying-time"
                changed={value => onSetParam('stayingTime', value)}
                options={state.stayingTimes}
                value={state.stayingTime || ''}
                renderer={value => value.name}
            />
        </div>
    )
}

export default Destination
