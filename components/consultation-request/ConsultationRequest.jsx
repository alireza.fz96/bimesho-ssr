import React from 'react'
import classes from './ConsultationRequest.module.scss'
import { TextField, Button } from '@material-ui/core'

const ConsultationRequest = () => {
	return (
		<div className={classes.consultation + ' row'}>
			<div
				className={
					classes.consultation__box +
					' col-12 ' +
					classes.consultation__mobile
				}
			>
				<div className={classes.consultation__popup}>
					<img
						src={'/imgs/svg/messageBoxWhite.svg'}
						alt="message_box"
						className={classes.consultation__messageBox}
					/>
				</div>
				<div className={classes.consultation__popup}>
					<img
						src={'/imgs/svg/messageBoxBule.svg'}
						alt="message_box"
						className={classes.consultation__messageBox}
					/>
					<p className={classes.consultation__text}>
						برای شروع گفتگو با مشاوران بیمه شو
						<br /> فیلد های زیر را پر کنید
					</p>
				</div>
			</div>
			<div className={classes.consultation__box + ' col-12'}>
				<div className={'row ' + classes.consultation__form}>
					<div className="col-md-6 col-lg-6 col-sm-12">
						<div className={classes.consultation__textMobile}>
							برای شروع گفتگو با مشاوران بیمه شو فیلد های زیر را
							پر کنید
						</div>
						<img
							src={'/imgs/svg/supporter.svg'}
							alt="supporter_logo"
							className={classes.consultation__mobile}
						/>
					</div>
					<div className="col-md-6 col-lg-6 col-sm-12">
						<div className="row">
							<div className="col-md-12 mt-5">
								<TextField
									label="نام"
									variant="outlined"
									fullWidth
								/>
							</div>
							<div className="col-md-12 mt-4">
								<TextField
									label="نام خانوادگی"
									variant="outlined"
									fullWidth
								/>
							</div>
							<div className="col-md-12 mt-4">
								<TextField
									label="شماره همراه"
									variant="outlined"
									fullWidth
								/>
							</div>
							<div className="col-md-12 mt-4">
								<TextField
									label="توضیحات"
									variant="outlined"
									multiline
									fullWidth
								/>
							</div>
							<div className="col-md-12 mt-4">
								<Button
									variant="contained"
									color="primary"
									fullWidth
								>
									تایید
								</Button>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div className="col-12 p-0">
				<div className={classes.consultation__footer}>
					<span>
						شماره تماس 24 ساعته بیمه شو برای راهنمایی و پاسخ سوالات
						شما
					</span>
					<span>
						<a
							href="tel:+982191002008"
							className={classes.consultation__phoneNumber}
						>
							021-91002008
						</a>
					</span>
				</div>
			</div>
		</div>
	)
}

export default ConsultationRequest
