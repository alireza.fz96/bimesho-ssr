import React from 'react'
import { Tabs, Tab, makeStyles } from '@material-ui/core'

const useStyles = makeStyles(theme => ({
	tabs: {
		width: '100%',
		'& .MuiTabs-flexContainer': {
			width: '100%',
			justifyContent: 'space-around'
		},
		'& .MuiTabs-root': {
			width: '100%'
		},
		'& .MuiTab-root': {
			[theme.breakpoints.down('xs')]: {
				padding: '0 5px',
				fontSize: 11
			}
		}
	}
}))

const FaqTabs = ({ page, changePage, title, header }) => {
	const styles = useStyles()

	return (
		<div className="header container-fluid">
			<div className="header__showBtn">
				<p>{title}</p>
			</div>
			<div className="row">
				<div className="header__titleBox">
					<div className={styles.tabs}>
						<Tabs
							value={page}
							onChange={changePage}
							indicatorColor="primary"
						>
							<Tab label={header} />
							<Tab label="سوالات متداول" />
							<Tab label="درخواست مشاوره" />
						</Tabs>
					</div>
				</div>
			</div>
		</div>
	)
}

export default FaqTabs
