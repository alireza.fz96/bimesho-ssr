import React from 'react'
import { Autocomplete } from '@material-ui/lab'
import { TextField, CircularProgress } from '@material-ui/core'
import { useSelector, useDispatch } from 'react-redux'
import {
    setModel as thirdSetModel,
    nextSlide as thirdNextSlide
} from '../../store/actions/thirdPartyInsuranceActions'
import {
    setModel as motorSetModel,
    nextSlide as motorNextSlide
} from '../../store/actions/motorInsuranceActions'
import {
    setModel as bodySetModel,
    nextSlide as bodyNextSlide
} from '../../store/actions/bodyInsuranceActions'
import { useModels } from '../../hooks/vehicles'

const VehicleModels = ({ bodyInsurance, motor }) => {
    const state = useSelector(state => {
        let model, brand
        if (bodyInsurance) {
            model = state.bodyInsurance.form.model
            brand = state.bodyInsurance.form.brand
        } else if (motor) {
            model = state.motorInsurance.form.model
            brand = state.motorInsurance.form.brand
        } else {
            model = state.thirdPartyInsurance.form.model
            brand = state.thirdPartyInsurance.form.brand
        }
        return {
            model,
            brand
        }
    })
    const dispatch = useDispatch()

    const { models, isLoading } = useModels(state.brand && state.brand.id)

    const onSetVehicleModel = model => {
        if (model) {
            dispatch(
                bodyInsurance
                    ? bodySetModel(model)
                    : motor
                    ? motorSetModel(model)
                    : thirdSetModel(model)
            )
            dispatch(
                bodyInsurance
                    ? bodyNextSlide()
                    : motor
                    ? motorNextSlide()
                    : thirdNextSlide()
            )
        }
    }

    return (
        <div className="input-wrapper">
            {isLoading && (
                <div className="d-flex justify-content-center w-100">
                    <CircularProgress size={60} />
                </div>
            )}
            {!isLoading && (
                <Autocomplete
                    options={models}
                    getOptionLabel={option => option.name || ''}
                    value={state.model}
                    closeIcon={false}
                    onChange={(_, value) => onSetVehicleModel(value)}
                    renderInput={params => (
                        <TextField
                            {...params}
                            label="انتخاب مدل"
                            variant="outlined"
                            autoFocus
                        />
                    )}
                />
            )}
        </div>
    )
}

export default VehicleModels
