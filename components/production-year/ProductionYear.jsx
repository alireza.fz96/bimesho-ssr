import React from 'react'
import { useSelector, useDispatch } from 'react-redux'
import {
	setProductionYear as thirdSetProductionYear,
	nextSlide as thirdNextSlide
} from '../../store/actions/thirdPartyInsuranceActions'
import {
	setProductionYear as motorSetProductionYear,
	nextSlide as motorNextSlide
} from '../../store/actions/motorInsuranceActions'
import {
	setProductionYear as bodySetProductionYear,
	nextSlide as bodyNextSlide,
	setDomesticProduction
} from '../../store/actions/bodyInsuranceActions'
import classes from './ProductionYear.module.scss'
import Select from '../UI/Select/Select'
import { Switch, makeStyles } from '@material-ui/core'

const useStyles = makeStyles({
	root: {
		width: '100%',
		marginBottom: 24,
		'& .MuiSwitch-root ': {
			display: 'flex !important',
			width: '100% !important',
			height: '50px !important',
			padding: '0 !important',
			borderRadius: 25,
			border: '1px solid #b3b8bc'
		},
		'& .MuiSwitch-switchBase': {
			width: '100%',
			padding: 0
		},
		'& .MuiSwitch-switchBase.Mui-checked': {
			transform: 'translateX(50%) !important'
		},
		'& .MuiSwitch-switchBase .MuiIconButton-label': {
			justifyContent: 'flex-start'
		},
		'& .MuiSwitch-thumb': {
			width: 'calc(50% - 16px) !important',
			height: '36px !important',
			margin: '6px 8px',
			boxShadow: 'none !important',
			borderRadius: '25px !important',
			backgroundColor: '#2659c1'
		},
		'& .MuiSwitch-track': {
			border: 'none !important',
			borderRadius: '25px!important',
			opacity: '1!important',
			backgroundColor: '#fff!important'
		},
		'& .MuiSwitch-colorPrimary.Mui-checked:hover': {
			backgroundColor: 'initial'
		},
		'& .MuiIconButton-root:hover ': {
			backgroundColor: 'initial'
		}
	}
})

const ProductionYear = ({ bodyInsurance, motor }) => {
	const styles = useStyles()
	const state = useSelector(state => {
		const productionYear = bodyInsurance
			? state.bodyInsurance.form.productionYear
			: motor
			? state.motorInsurance.form.productionYear
			: state.thirdPartyInsurance.form.productionYear
		const years = state.coreData.years
		return {
			productionYear,
			years,
			domesticProduction: state.bodyInsurance.form.domesticProduction
		}
	})
	const dispatch = useDispatch()

	const onSetProductionYear = year => {
		if (year) {
			dispatch(
				bodyInsurance
					? bodySetProductionYear(year)
					: motor
					? motorSetProductionYear(year)
					: thirdSetProductionYear(year)
			)
			dispatch(
				bodyInsurance
					? bodyNextSlide()
					: motor
					? motorNextSlide()
					: thirdNextSlide()
			)
		}
	}

	const renderer = year => (
		<>
			<span className={classes.year + ' ' + classes.miladi}>
				{year.miladi}
			</span>
			|<span className={classes.year}>{year.jalali}</span>
		</>
	)

	const toggle = () => {
		dispatch(setDomesticProduction(!state.domesticProduction))
    }
    
	return (
		<div className="input-wrapper">
			{bodyInsurance && (
				<div className={classes.switch} onClick={toggle}>
					<div className={classes.switch__wrapper}>
						<span
							className={[
								classes.switch__label,
								state.domesticProduction
									? classes.switch__disabled
									: ''
							].join(' ')}
						>
							تولید خارج
						</span>
					</div>
					<div className={styles.root}>
						<Switch
							checked={state.domesticProduction}
							onChange={toggle}
							color="primary"
						/>
					</div>
					<div className={classes.switch__wrapper}>
						<span
							className={[
								classes.switch__label,
								!state.domesticProduction
									? classes.switch__disabled
									: ''
							].join(' ')}
						>
							تولید داخل
						</span>
					</div>
				</div>
			)}
			<Select
				label={!motor ? 'سال ساخت وسیله نقلیه' : 'سال ساخت موتور'}
				id="production-year"
				changed={onSetProductionYear}
				options={state.years}
				value={state.productionYear}
				renderer={renderer}
			/>
		</div>
	)
}

export default ProductionYear
