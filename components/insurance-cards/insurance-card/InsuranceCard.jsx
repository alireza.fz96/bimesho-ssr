import React from 'react'
import Link from 'next/link'
import classes from './InsuranceCard.module.scss'

const InsuranceCard = ({ insurance }) => {
	return (
		<div className={classes.insuranceCard}>
			<Link href={insurance.link}>
				<div className="w-100">
					<img src={insurance.icon} alt={insurance.name + '_icon'} />
					<h2>{insurance.name}</h2>
				</div>
			</Link>
		</div>
	)
}

export default InsuranceCard
