import React from 'react'
import InsuranceCard from './insurance-card/InsuranceCard'
import classes from './InsuranceCards.module.scss'

const insurances = [
	{
		name: 'بیمه شخص ثالث',
		link: '/third-party-insurance',
		icon: '/imgs/svg/thirdparty-icon.svg'
	},
	{
		name: 'بیمه بدنه',
		link: '/body-insurance',
		icon: '/imgs/svg/body-icon.svg'
	},
	{
		name: 'بیمه مسافرتی',
		link: '/travel-insurance',
		icon: '/imgs/svg/travel-icon.svg'
	},
	{
		name: 'بیمه درمان تکمیلی',
		link: '/health-insurance',
		icon: '/imgs/svg/health-icon.svg'
	},
	{
		name: 'بیمه عمر',
		link: '/life-insurance',
		icon: '/imgs/svg/life-icon.svg'
	},
	{
		name: 'بیمه موتور سیکلت',
		link: '/motor-third-party-insurance',
		icon: '/imgs/svg/motor-icon.svg'
	},
	{
		name: 'بیمه آتش سوزی و زلزله',
		link: '/fire-insurance',
		icon: '/imgs/svg/fire-icon.svg'
	}
]

const InsuranceCards = () => {
	return (
		<>
			<div
				className={classes.insurancesBackground}
				style={{ backgroundImage: `url(${'/imgs/home-insurance-wrappers.jpg'})` }}
			></div>
			<div
				className={
					'row justify-content-sm-start justify-content-center ' +
					classes.insurances
				}
			>
				{insurances.map(insurece => (
					<div
						className="col-md-3 col-sm-4 col-6 d-flex align-items-center justify-content-center p-0 m-0"
						key={insurece.name}
					>
						<InsuranceCard insurance={insurece} />
					</div>
				))}
			</div>
		</>
	)
}

export default InsuranceCards
