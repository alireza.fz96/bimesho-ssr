import React from 'react'
import Button from '@material-ui/core/Button'
import Dialog from '@material-ui/core/Dialog'
import DialogActions from '@material-ui/core/DialogActions'
import DialogContent from '@material-ui/core/DialogContent'
import DialogContentText from '@material-ui/core/DialogContentText'
import Slide from '@material-ui/core/Slide'
import useMediaQuery from '@material-ui/core/useMediaQuery'
import { useTheme } from '@material-ui/core/styles'
import { Close } from '@material-ui/icons'
import AppBar from '@material-ui/core/AppBar'
import Toolbar from '@material-ui/core/Toolbar'
import Typography from '@material-ui/core/Typography'
import IconButton from '@material-ui/core/IconButton'
import classes from './OrderDialog.module.scss'
import { CircularProgress } from '@material-ui/core'

const Transition = React.forwardRef(function Transition(props, ref) {
	return <Slide direction="up" ref={ref} {...props} />
})

export default function OrderDialog({
	open,
	setOpen,
	values,
	onOrder,
	hasInstallments,
	loading
}) {
	const theme = useTheme()

	const fullScreen = useMediaQuery(theme.breakpoints.down('sm'))
	return (
		<Dialog
			open={open}
			TransitionComponent={Transition}
			keepMounted
			onClose={() => setOpen(false)}
			fullScreen={fullScreen}
		>
			<AppBar position="static">
				<Toolbar>
					<IconButton
						edge="start"
						color="inherit"
						aria-label="menu"
						onClick={() => setOpen(false)}
					>
						<Close />
					</IconButton>
					<Typography variant="h6">سفارش</Typography>
				</Toolbar>
			</AppBar>
			<DialogContent>
				<DialogContentText
					id="alert-dialog-slide-description"
					className="p-3 pl-sm-5 pr-sm-5 pt-2 pb-2"
					component="div"
				>
					<div className={'container ' + classes.dialog}>
						{values.map(value => (
							<>
								<div className="row justify-content-center">
									<div className={classes.title}>
										<p>{value.header}</p>
									</div>
								</div>
								{value.body.map(value => (
									<div
										className="row justify-content-between my-3"
										key={value.key}
									>
										<div className={classes.key}>
											{value.key}
										</div>
										<div className={classes.value}>
											{value.value}
										</div>
									</div>
								))}
							</>
						))}
						{hasInstallments && (
							<div
								className={
									'row justify-content-center ' +
									classes.footer
								}
							>
								<div className={classes.title}>
									<p>اقساط</p>
								</div>
								<p className={classes.red + ' my-2 w-100'}>
									قیمت اقساط: 4,366,118
								</p>
								<p>
									سفارش اقساطی تنها در شهر
									<span className={classes.red}> تهران </span>
									امکان پذیر است.
								</p>
								<p>
									بیمه‌نامه شما بعد از دریافت تصاویر چک‌ها
									صادر می‌شود.
								</p>
								<p>
									لطفا قبل از صدور چک، منتظر تماس کارشناسان ما
									باشید.
								</p>
								<p className={classes.red}>
									چک های ارائه شده، حتما باید دارای آرم صیاد
									باشد.
								</p>
							</div>
						)}
					</div>
				</DialogContentText>
			</DialogContent>
			<DialogActions>
				{loading ? (
					<div className="text-center w-100">
						<CircularProgress />
					</div>
				) : (
					<div className="text-center w-100">
						<Button
							onClick={() => onOrder('cash')}
							color="primary"
							variant="contained"
						>
							سفارش نقدی
						</Button>
						{hasInstallments && (
							<Button
								color="secondary"
								variant="contained"
								className="mr-2"
								onClick={() => onOrder('installments')}
							>
								سفارش اقساطی
							</Button>
						)}
					</div>
				)}
			</DialogActions>
		</Dialog>
	)
}
