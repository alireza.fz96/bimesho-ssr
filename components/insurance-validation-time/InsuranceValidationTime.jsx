import React from 'react'
import { DatePicker } from '@material-ui/pickers'
import { useSelector, useDispatch } from 'react-redux'
import {
    setInsuranceStart,
    setInsuranceEnd,
    nextSlide
} from '../../store/actions/thirdPartyInsuranceActions'
import {
    setInsuranceStart as motorSetInsuranceStart,
    setInsuranceEnd as motorSetInsuranceEnd,
    nextSlide as motorNextSlide
} from '../../store/actions/motorInsuranceActions'

const InsuranceValidationTime = ({ motor }) => {
    const state = useSelector(state => {
        const { insuranceStart, insuranceEnd } = motor
            ? state.motorInsurance.form
            : state.thirdPartyInsurance.form
        return {
            start: insuranceStart,
            end: insuranceEnd
        }
    })

    const [errorMessage, setErrorMessage] = React.useState('')

    const dispatch = useDispatch()

    const onSetInsuranceStart = start => {
        dispatch(
            motor ? motorSetInsuranceStart(start) : setInsuranceStart(start)
        )
        state.end && setError(start, state.end)
    }

    const onSetInsuranceEnd = end => {
        dispatch(motor ? motorSetInsuranceEnd(end) : setInsuranceEnd(end))
        setError(state.start, end)
    }

    const onSubmit = () => {
        dispatch(motor ? motorNextSlide() : nextSlide())
    }

    const setError = (start, end) => {
        if (start.isAfter && start.isAfter(end, 'day'))
            setErrorMessage({ type: 'smaller' })
        else if (start.isSame && start.isSame(end, 'day'))
            setErrorMessage({ type: 'notEqual' })
        else if (end.diff && end.diff(start, 'day') > 366)
            setErrorMessage({ type: 'limit' })
        else {
            setErrorMessage('')
            onSubmit()
        }
    }

    const getErrorMessage = () => {
        if (errorMessage) {
            switch (errorMessage.type) {
                case 'notEqual':
                    return 'تاریخ شروع و انقضای بیمه نامه نمیتواند یکی باشد'
                case 'smaller':
                    return 'تاریخ شروع بیمه نامه نمیتواند بعد از تاریخ انقضا باشد'
                case 'limit':
                    return 'انقضای بیمه نامه نمیتواند بیشتر از یک سال باشد'
                default:
                    return ''
            }
        } else return ''
    }

    return (
        <div className="d-flex flex-column align-items-center">
            <div className="input-wrapper">
                <DatePicker
                    value={state.start}
                    labelFunc={date =>
                        date ? date.format('jYYYY/jMM/jDD') : ''
                    }
                    onChange={date => {
                        onSetInsuranceStart(date)
                    }}
                    variant="inline"
                    inputVariant="outlined"
                    label="تاریخ شروع بیمه نامه قبلی"
                    error={!!errorMessage}
                    helperText={getErrorMessage()}
                    autoOk
                />
            </div>
            <div className="input-wrapper mt-4">
                <DatePicker
                    value={state.end}
                    labelFunc={date =>
                        date ? date.format('jYYYY/jMM/jDD') : ''
                    }
                    onChange={date => {
                        onSetInsuranceEnd(date)
                    }}
                    variant="inline"
                    inputVariant="outlined"
                    label="تاریخ پایان بیمه نامه قبلی"
                    error={!!errorMessage}
                    helperText={getErrorMessage()}
                    autoOk
                />
            </div>
        </div>
    )
}

export default InsuranceValidationTime
