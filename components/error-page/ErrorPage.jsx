import React from 'react'
import { initial as bodyInitial } from '../../store/actions/bodyInsuranceActions'
import { initial as motorInitial } from '../../store/actions/motorInsuranceActions'
import { initial as thirdInitial } from '../../store/actions/thirdPartyInsuranceActions'
import { reset as travelReset } from '../../store/actions/travelInsuranceActions'
import { reset as fireReset } from '../../store/actions/fireInsuranceActions'
import { reset as lifeReset } from '../../store/actions/lifeInsuranceActions'
import { reset as healthReset } from '../../store/actions/healthInsuranceActions'
import { logout } from '../../store/actions/userActions'
import { useDispatch } from 'react-redux'
import { resetPorduct } from '../../store/actions/productActions'

const ErrorPage = (error, resetErrorBoundary) => {
	const dispatch = useDispatch()

	React.useEffect(() => {
		dispatch(dispatch => {
			dispatch(bodyInitial())
			dispatch(thirdInitial())
			dispatch(motorInitial())
			dispatch(travelReset())
			dispatch(fireReset())
			dispatch(lifeReset())
			dispatch(healthReset())
			dispatch(logout())
			dispatch(resetPorduct())
		})
	}, [])

	return (
		<div role="alert">
			<div className="text-center my-5">
				<p>
					خطایی پیش آمده است :( <br /> لطفا دوباره تلاش کنید
				</p>
				<pre className="my-4">{error.message}</pre>
				<a href="/" style={{ color: 'blue' }}>
					صفحه اصلی
				</a>
			</div>
		</div>
	)
}

export default ErrorPage
