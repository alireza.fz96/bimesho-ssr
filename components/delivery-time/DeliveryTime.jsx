import React from 'react'
import {
	Tabs,
	Tab,
	RadioGroup,
	FormControlLabel,
	Radio,
	makeStyles
} from '@material-ui/core'
import TabPanel from '../UI/TabPanel/TabPanel'
import { Controller } from 'react-hook-form'
import classes from './DeliveryTime.module.scss'

const useStyle = makeStyles({
	root: {
		'& .MuiFormControlLabel-root': {
			margin: 0
		},
		'& .MuiFormControlLabel-label': {
			fontSize: 14
		},
		'& .MuiTabs-indicator': {
			height: 3
		},
		'& .MuiTabs-scroller': {
			border: '1px solid #257ffc',
			borderRadius: '20px 20px 0 0'
		}
	}
})

const DeliveryTime = ({
	control,
	error,
	initialValue,
	changed,
	initialIndex = 0
}) => {
	const styles = useStyle()
	const [page, setPage] = React.useState(initialIndex)

	const changePage = (event, newValue) => {
		setPage(newValue)
	}

	return (
		<div className={styles.root}>
			<Tabs
				value={page}
				onChange={changePage}
				indicatorColor="primary"
				scrollButtons="auto"
				textColor="primary"
				variant="scrollable"
			>
				<Tab label="شنبه" />
				<Tab label="یکشنبه" />
				<Tab label="دوشنبه" />
				<Tab label="سه شنبه" />
			</Tabs>
			<TabPanel value={page} index={0}>
				<Controller
					as={
						<RadioGroup aria-label="gender">
							<FormControlLabel
								value="saturday-15-19"
								className={classes.time}
								control={<Radio color="primary" />}
								label="ساعت ۱۵-۱۹ - هزینه: رایگان"
								onChange={() =>
									changed({
										value: 'saturday-15-19',
										index: page
									})
								}
							/>
							<FormControlLabel
								className={classes.time}
								value="saturday-18-22"
								control={<Radio color="primary" />}
								label="ساعت ۱۸-۲۲ - هزینه: رایگان"
								onChange={() =>
									changed({
										value: 'saturday-18-22',
										index: page
									})
								}
							/>
						</RadioGroup>
					}
					name="deliveryTime"
					defaultValue={initialValue}
					control={control}
					rules={{ required: true }}
				/>
			</TabPanel>
			<TabPanel value={page} index={1}>
				<Controller
					as={
						<RadioGroup aria-label="gender">
							<FormControlLabel
								className={classes.time}
								value="sunday-15-19"
								control={<Radio color="primary" />}
								label="ساعت ۱۵-۱۹ - هزینه: رایگان"
								onChange={() =>
									changed({
										value: 'sunday-15-19',
										index: page
									})
								}
							/>
							<FormControlLabel
								className={classes.time}
								value="sunday-18-22"
								control={<Radio color="primary" />}
								label="ساعت ۱۸-۲۲ - هزینه: رایگان"
								onChange={() =>
									changed({
										value: 'sunday-18-22',
										index: page
									})
								}
							/>
						</RadioGroup>
					}
					name="deliveryTime"
					control={control}
					defaultValue={initialValue}
					rules={{ required: true }}
				/>
			</TabPanel>
			<TabPanel value={page} index={2}>
				<Controller
					as={
						<RadioGroup aria-label="gender">
							<FormControlLabel
								className={classes.time}
								value="monday-15-19"
								control={<Radio color="primary" />}
								label="ساعت ۱۵-۱۹ - هزینه: رایگان"
								onChange={() =>
									changed({
										value: 'monday-15-19',
										index: page
									})
								}
							/>
							<FormControlLabel
								className={classes.time}
								value="monday-18-22"
								control={<Radio color="primary" />}
								label="ساعت ۱۸-۲۲ - هزینه: رایگان"
								onChange={() =>
									changed({
										value: 'monday-18-22',
										index: page
									})
								}
							/>
						</RadioGroup>
					}
					name="deliveryTime"
					control={control}
					defaultValue={initialValue}
					rules={{ required: true }}
				/>
			</TabPanel>
			<TabPanel value={page} index={3}>
				<Controller
					as={
						<RadioGroup aria-label="gender">
							<FormControlLabel
								className={classes.time}
								value="tuesday-15-19"
								control={<Radio color="primary" />}
								label="ساعت ۱۵-۱۹ - هزینه: رایگان"
								onChange={() =>
									changed({
										value: 'tuesday-15-19',
										index: page
									})
								}
							/>
							<FormControlLabel
								className={classes.time}
								value="tuesday-18-22"
								control={<Radio color="primary" />}
								label="ساعت ۱۸-۲۲ - هزینه: رایگان"
								onChange={() =>
									changed({
										value: 'tuesday-18-22',
										index: page
									})
								}
							/>
						</RadioGroup>
					}
					name="deliveryTime"
					control={control}
					defaultValue={initialValue}
					rules={{ required: true }}
				/>
			</TabPanel>
			{error && (
				<div className="text-danger mt-3">
					لطفا زمان تحویل را تعیین کنید
				</div>
			)}
		</div>
	)
}

export default DeliveryTime
