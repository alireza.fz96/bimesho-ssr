import React from 'react'
import { withStyles, makeStyles } from '@material-ui/core/styles'
import Menu from '@material-ui/core/Menu'
import MenuItem from '@material-ui/core/MenuItem'
import ListItemText from '@material-ui/core/ListItemText'

const useStyle = makeStyles({
	btn: {
		fontWeight: 400,
		margin: '0 15px',
		fontSize: 18,
		color: '#53575b',
		cursor: 'pointer',
		'&:hover': {
			color: '#007bff'
		}
	}
})

const StyledMenu = withStyles({
	paper: {
		border: '1px solid #d3d4d5',
		maxHeight: 300
	}
})(props => (
	<Menu
		elevation={0}
		getContentAnchorEl={null}
		anchorOrigin={{
			vertical: 'bottom',
			horizontal: 'center'
		}}
		transformOrigin={{
			vertical: 'top',
			horizontal: 'center'
		}}
		{...props}
	/>
))

export default function CustomizedMenus({
	items,
	anchorEl,
	handleClick,
	handleClose,
	onClick,
	label,
	getLabel
}) {
	const classes = useStyle()
	return (
		<div className="d-flex align-items-center">
			<span onClick={handleClick} className={classes.btn}>
				{label}
			</span>
			<StyledMenu
				anchorEl={anchorEl}
				open={Boolean(anchorEl)}
				onClose={handleClose}
			>
				{items.map(item => (
					<MenuItem
						key={item.id || item}
						onClick={() => onClick(item)}
					>
						<ListItemText
							primary={getLabel(item)}
							style={{ minWidth: 150 }}
						/>
					</MenuItem>
				))}
			</StyledMenu>
		</div>
	)
}
