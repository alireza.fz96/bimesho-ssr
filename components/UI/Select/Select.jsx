import React from 'react'
import InputLabel from '@material-ui/core/InputLabel'
import MenuItem from '@material-ui/core/MenuItem'
import FormControl from '@material-ui/core/FormControl'
import Select from '@material-ui/core/Select'

const CustomSelect = ({ label, id, value, changed, options, renderer }) => {
	const getOption = (item, options) => {
		return options.find(option => option.name === item.name)
	}
	return (
		<FormControl variant="outlined">
			<InputLabel id={id + '-label'}>{label}</InputLabel>
			<Select
				labelId={id + '-label'}
				id="demo-simple-select"
				label={label}
				value={(value && getOption(value, options)) || ''}
				onChange={event => {
					changed(event.target.value)
				}}
				renderValue={value => renderer(value)}
			>
				{options.map((option, index) => (
					<MenuItem value={option} key={index}>
						{renderer(option)}
					</MenuItem>
				))}
			</Select>
		</FormControl>
	)
}

export default CustomSelect
