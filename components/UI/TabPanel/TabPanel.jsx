import React from 'react'

export default function TabPanel(props) {
	const { children, value, index, ...other } = props

	return (
		<div className="row">
			<div
				role="tabpanel"
				hidden={value !== index}
				{...other}
				className="content col-12"
			>
				{value === index && children}
			</div>
		</div>
	)
}
