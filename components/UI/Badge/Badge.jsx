import React from 'react'
import classes from './Badge.module.scss'

const Badge = ({ children }) => {
	return <span className={classes.badge}>{children}</span>
}

export default Badge
