import React from 'react'
import NProgress from 'nprogress'

const LazyProgressBar = () => {
	React.useEffect(() => {
		NProgress.start()

		return () => {
			NProgress.done()
		}
	})

	return ''
}

export default LazyProgressBar
