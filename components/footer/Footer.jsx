import React from 'react'
import classes from './Footer.module.scss'
import {
	Twitter,
	Facebook,
	LinkedIn,
	Instagram,
	Telegram
} from '@material-ui/icons'
import Link from 'next/link'

const Footer = () => {
	const onOpenEnamad = () => {
		window.open(
			'https://trustseal.enamad.ir/?id=148070&Code=NolsfoOTMQvq2IueOpcb',
			'Popup',
			'toolbar=no, location=no, statusbar=no, menubar=no, scrollbars=1, resizable=0, width=580, height=600, top=30'
		)
	}

	return (
		<div className="container-fluid container-lg">
			<div className={classes.footer}>
				<div className={classes.footer__content}>
					<div
						className={
							classes.footer__section +
							' ' +
							classes['footer__section--social']
						}
					>
						<div className={classes.footer__logo}>
							<img src={'/logo.png'} alt="logo" />
						</div>
						<div className={classes.footer__socialIcons}>
							<a
								target="blank"
								href="https://www.facebook.com/sharer/sharer.php?u=https://bimesho.com"
							>
								<div className={classes.footer__icon}>
									<Facebook />
								</div>
							</a>
							<a
								target="blank"
								href="https://twitter.com/intent/tweet?text=%D8%AA%D8%AE%D9%81%DB%8C%D9%81%20%D8%A7%D8%B3%D8%AA%D8%AB%D9%86%D8%A7%DB%8C%DB%8C%20%D8%AE%D8%B1%DB%8C%D8%AF%20%D8%A8%DB%8C%D9%85%D9%87%20%D8%AF%D8%B1%20%D8%A8%DB%8C%D9%85%D9%87%20%D8%B4%D9%88-%D9%87%D9%85%DB%8C%D9%86%20%D8%A7%D9%84%D8%A7%D9%86%20%D8%AB%D8%A8%D8%AA%20%D9%86%D8%A7%D9%85%20%DA%A9%D9%86%20%D9%88%20%D9%BE%D9%88%D8%B1%D8%B3%D8%A7%D9%86%D8%AA%20%D8%AE%D9%88%D8%AF%20%D8%B1%D8%A7%20%D8%AF%D8%B1%DB%8C%D8%A7%D9%81%D8%AA%20%DA%A9%D9%86%DB%8C%D8%AF."
							>
								<div className={classes.footer__icon}>
									<Twitter />
								</div>
							</a>
							<a
								target="blank"
								href="http://www.linkedin.com/shareArticle?mini=true&url=https://bimesho.com&title=%D8%B3%DB%8C%D8%B3%D8%AA%D9%85%20%DA%A9%D8%B3%D8%A8%20%D8%AF%D8%B1%D8%A2%D9%85%D8%AF%20%D9%88%20%D8%A8%D8%A7%D8%B2%D8%A7%D8%B1%DB%8C%D8%A7%D8%A8%DB%8C%20%D8%A8%DB%8C%D9%85%D9%87%20%D8%B4%D9%88&summary=%D8%AA%D8%AE%D9%81%DB%8C%D9%81%20%D8%A7%D8%B3%D8%AA%D8%AB%D9%86%D8%A7%DB%8C%DB%8C%20%D8%AE%D8%B1%DB%8C%D8%AF%20%D8%A8%DB%8C%D9%85%D9%87%20%D8%AF%D8%B1%20%D8%A8%DB%8C%D9%85%D9%87%20%D8%B4%D9%88-%D9%87%D9%85%DB%8C%D9%86%20%D8%A7%D9%84%D8%A7%D9%86%20%D8%AB%D8%A8%D8%AA%20%D9%86%D8%A7%D9%85%20%DA%A9%D9%86%20%D9%88%20%D9%BE%D9%88%D8%B1%D8%B3%D8%A7%D9%86%D8%AA%20%D8%AE%D9%88%D8%AF%20%D8%B1%D8%A7%20%D8%AF%D8%B1%DB%8C%D8%A7%D9%81%D8%AA%20%DA%A9%D9%86%DB%8C%D8%AF."
							>
								<div className={classes.footer__icon}>
									<LinkedIn />
								</div>
							</a>
							<a target="blank" href="https://bimesho.com">
								<div className={classes.footer__icon}>
									<Instagram />
								</div>
							</a>
							<a
								target="blank"
								href="https://t.me/share/url?url=https://bimesho.com&text=%D8%AE%D8%B1%DB%8C%D8%AF%20%D8%A8%DB%8C%D9%85%D9%87%20%D8%AF%D8%B1%20%D8%B3%D8%A7%D9%85%D8%A7%D9%86%D9%87%20%D8%AE%D8%B1%DB%8C%D8%AF%20%D8%A2%D9%86%D9%84%D8%A7%DB%8C%D9%86%20%D8%A8%DB%8C%D9%85%D9%87%20%D8%8C%20%D8%A8%DB%8C%D9%85%D9%87%20%D8%B4%D9%88%20-%20%D8%A8%D8%B1%D8%A7%DB%8C%20%D8%A7%D8%B3%D8%AA%D9%81%D8%A7%D8%AF%D9%87%20%D8%A7%D8%B2%20%D8%AA%D8%AE%D9%81%DB%8C%D9%81%D8%A7%D8%AA%20%D9%88%DB%8C%DA%98%D9%87%20%DA%A9%D9%84%DB%8C%DA%A9%20%DA%A9%D9%86%DB%8C%D8%AF."
							>
								<div className={classes.footer__icon}>
									<Telegram />
								</div>
							</a>
						</div>
					</div>

					<div
						className={
							classes.footer__section +
							' ' +
							classes['footer__section--nav']
						}
					>
						<ul className={classes.footer__navList}>
							<li className={classes.footer__navItem}>
								<Link href="/">خانه</Link>
							</li>
							<li className={classes.footer__navItem}>
								<a href="/">همکاری با بیمه شو</a>
							</li>
							<li className={classes.footer__navItem}>
								<a href="/">فرصت های شغلی</a>
							</li>
						</ul>
						<ul className={classes.footer__navList}>
							<li className={classes.footer__navItem}>
								<Link href="/about-us">درباره بیمه شو</Link>
							</li>
							<li className={classes.footer__navItem}>
								<Link href="/contact-us">تماس با بیمه شو</Link>
							</li>
							<li className={classes.footer__navItem}>
								<a href="/">شرایط و قوانین</a>
							</li>
						</ul>
					</div>

					<div
						className={
							classes.footer__section +
							' ' +
							classes['footer__section--certifactions']
						}
					>
						<div className={classes.footer__certification}>
							<a href="/">
								<img
									src={'/imgs/logo-samandehi.png'}
									alt="samandehi_logo"
									className="img-fluid"
								/>
							</a>
						</div>
						<div
							className={
								classes.footer__certification +
								' ' +
								classes['footer__certification--enamad']
							}
						>
							<a href="/">
								<img
									src={'/imgs/logo-enamad.png'}
									alt="enamad_logo"
									className="img-fluid"
									onClick={onOpenEnamad}
								/>
							</a>
						</div>
					</div>
				</div>
				<div className={classes.footer__tags}>
					<div className={classes.footer__tag}>#بیمه شو</div>
					<div className={classes.footer__tag}>#bimesho</div>
				</div>
			</div>
			<div className="row">
				<div className="col-12">
					<p className={classes.footer__copyRight}>
						کلیه حقوق این سایت متعلق به شرکت بیمه شو میباشد
					</p>
					<p className={classes.footer__version}>
						V1.0.0 ©{new Date().getFullYear()} bimesho.com
					</p>
				</div>
			</div>
		</div>
	)
}

export default Footer
