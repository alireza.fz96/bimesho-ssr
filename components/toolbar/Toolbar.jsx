import React from 'react'
import classes from './Toolbar.module.scss'
import { Button, IconButton } from '@material-ui/core'
import NavigationItems from './navigation-items/NavigationItems.jsx'
import Drawer from '../drawer/Drawer'
import Menu from '@material-ui/icons/Menu'
import {
	ContactSupport,
	Twitter,
	Facebook,
	LinkedIn,
	Instagram,
	Telegram,
	Person,
	ExitToApp
} from '@material-ui/icons'
import Link from 'next/link'
import { useRouter } from 'next/router'
import { useDispatch, useSelector } from 'react-redux'
import { logout } from '../../store/actions/userActions'

const Toolbar = () => {
	const [open, setOpen] = React.useState(false)

	const toggleDrawer = () => {
		setOpen(open => !open)
	}
	const state = useSelector(state => state.user)
	const dispatch = useDispatch()

	const router = useRouter()

	const onLogout = () => {
		dispatch(logout())
		router.replace('/')
	}

	return (
		<div className={'container-fluid container-lg ' + classes.header}>
			<div className="d-md-flex d-none align-item-center justify-content-between pt-3 px-4">
				<div className={classes.social}>
					<a
						href="https://twitter.com/intent/tweet?text=%D8%AA%D8%AE%D9%81%DB%8C%D9%81%20%D8%A7%D8%B3%D8%AA%D8%AB%D9%86%D8%A7%DB%8C%DB%8C%20%D8%AE%D8%B1%DB%8C%D8%AF%20%D8%A8%DB%8C%D9%85%D9%87%20%D8%AF%D8%B1%20%D8%A8%DB%8C%D9%85%D9%87%20%D8%B4%D9%88-%D9%87%D9%85%DB%8C%D9%86%20%D8%A7%D9%84%D8%A7%D9%86%20%D8%AB%D8%A8%D8%AA%20%D9%86%D8%A7%D9%85%20%DA%A9%D9%86%20%D9%88%20%D9%BE%D9%88%D8%B1%D8%B3%D8%A7%D9%86%D8%AA%20%D8%AE%D9%88%D8%AF%20%D8%B1%D8%A7%20%D8%AF%D8%B1%DB%8C%D8%A7%D9%81%D8%AA%20%DA%A9%D9%86%DB%8C%D8%AF."
						className="px-2"
						target="blank"
					>
						<Twitter fontSize="small" />
					</a>
					<a
						href="https://www.facebook.com/sharer/sharer.php?u=https://bimesho.com"
						className="px-2"
						target="blank"
					>
						<Facebook fontSize="small" />
					</a>
					<a href="https://bimesho.com" className="px-2">
						<Instagram fontSize="small" />
					</a>
					<a
						href="http://www.linkedin.com/shareArticle?mini=true&url=https://bimesho.com&title=%D8%B3%DB%8C%D8%B3%D8%AA%D9%85%20%DA%A9%D8%B3%D8%A8%20%D8%AF%D8%B1%D8%A2%D9%85%D8%AF%20%D9%88%20%D8%A8%D8%A7%D8%B2%D8%A7%D8%B1%DB%8C%D8%A7%D8%A8%DB%8C%20%D8%A8%DB%8C%D9%85%D9%87%20%D8%B4%D9%88&summary=%D8%AA%D8%AE%D9%81%DB%8C%D9%81%20%D8%A7%D8%B3%D8%AA%D8%AB%D9%86%D8%A7%DB%8C%DB%8C%20%D8%AE%D8%B1%DB%8C%D8%AF%20%D8%A8%DB%8C%D9%85%D9%87%20%D8%AF%D8%B1%20%D8%A8%DB%8C%D9%85%D9%87%20%D8%B4%D9%88-%D9%87%D9%85%DB%8C%D9%86%20%D8%A7%D9%84%D8%A7%D9%86%20%D8%AB%D8%A8%D8%AA%20%D9%86%D8%A7%D9%85%20%DA%A9%D9%86%20%D9%88%20%D9%BE%D9%88%D8%B1%D8%B3%D8%A7%D9%86%D8%AA%20%D8%AE%D9%88%D8%AF%20%D8%B1%D8%A7%20%D8%AF%D8%B1%DB%8C%D8%A7%D9%81%D8%AA%20%DA%A9%D9%86%DB%8C%D8%AF."
						className="px-2"
						target="blank"
					>
						<LinkedIn fontSize="small" />
					</a>
					<a
						target="blank"
						className="px-2"
						href="https://t.me/share/url?url=https://bimesho.com&text=%D8%AE%D8%B1%DB%8C%D8%AF%20%D8%A8%DB%8C%D9%85%D9%87%20%D8%AF%D8%B1%20%D8%B3%D8%A7%D9%85%D8%A7%D9%86%D9%87%20%D8%AE%D8%B1%DB%8C%D8%AF%20%D8%A2%D9%86%D9%84%D8%A7%DB%8C%D9%86%20%D8%A8%DB%8C%D9%85%D9%87%20%D8%8C%20%D8%A8%DB%8C%D9%85%D9%87%20%D8%B4%D9%88%20-%20%D8%A8%D8%B1%D8%A7%DB%8C%20%D8%A7%D8%B3%D8%AA%D9%81%D8%A7%D8%AF%D9%87%20%D8%A7%D8%B2%20%D8%AA%D8%AE%D9%81%DB%8C%D9%81%D8%A7%D8%AA%20%D9%88%DB%8C%DA%98%D9%87%20%DA%A9%D9%84%DB%8C%DA%A9%20%DA%A9%D9%86%DB%8C%D8%AF."
					>
						<Telegram />
					</a>
				</div>
				<div
					className={classes.support}
					style={{
						color:
							router.pathname === '/'
								? 'white'
								: 'rgb(0, 60, 182)'
					}}
				>
					<a href="tel:+982191002008">021-91002008</a>
					<ContactSupport />
				</div>
			</div>
			<div className={classes.toolbar}>
				<div className={classes.toolbar__right}>
					<div className={classes.logo}>
						<Link href="/">
							<img
								src={'/logo.png'}
								alt="logo"
								className="img-fluid"
							/>
						</Link>
					</div>
					<NavigationItems />
				</div>
				<div>
					{!state.user ? (
						<Link href="/login">
							<Button color="primary" variant="contained">
								ورود / عضویت
							</Button>
						</Link>
					) : (
						<div>
							<div className="row align-items-center">
								<Person />
								<span className="ml-1 mr-2">
									{state.user.data && state.user.data.mobile}
								</span>
								<IconButton onClick={onLogout}>
									<ExitToApp />
								</IconButton>
							</div>
						</div>
					)}

					<Drawer open={open} toggleDrawer={toggleDrawer} />
				</div>
			</div>
			<div className={classes.toolbar__mobile}>
				<div onClick={toggleDrawer} className={classes.icon}>
					<Menu className={classes.menu} />
				</div>
				<div className={classes.logo}>
					<Link href="/">
						<img
							src={'/logo.png'}
							alt="logo"
							className="img-fluid"
							style={{ cursor: 'pointer' }}
						/>
					</Link>
				</div>
				<div className={classes.icon}>
					<ContactSupport />
				</div>
			</div>
		</div>
	)
}

export default Toolbar
