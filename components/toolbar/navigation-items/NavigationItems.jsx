import React from 'react'
import { useSelector } from 'react-redux'
import Menu from '../../UI/Menu/Menu'
import { useRouter } from 'next/router'

const insurances = [
	{ name: 'بیمه شخص ثالث', link: '/third-party-insurance' },
	{ name: 'بیمه بدنه', link: '/body-insurance' },
	{ name: 'بیمه مسافرتی', link: '/travel-insurance' },
	{ name: 'بیمه درمان تکمیلی', link: '/health-insurance' },
	{ name: 'بیمه عمر', link: '/life-insurance' },
	{ name: 'بیمه آتش سوزی و زلزله', link: '/fire-insurance' },
	{ name: 'بیمه موتور سیکلت', link: '/motor-third-party-insurance' }
]

export default function CustomizedMenus() {
	const [partnersAnchorEl, setPartnersAnchorEl] = React.useState(null)

	const handlePartnersClick = event => {
		setPartnersAnchorEl(event.currentTarget)
	}

	const handlePartnersClose = () => {
		setPartnersAnchorEl(null)
	}

	const [insurancesAnchorEl, setInsurancesAnchorEl] = React.useState(null)

	const handleInsurancesClick = event => {
		setInsurancesAnchorEl(event.currentTarget)
	}

	const handleInsurancesClose = () => {
		setInsurancesAnchorEl(null)
	}

	const companiesList = useSelector(state => state.coreData.companiesList)

	const router = useRouter()

	const onClickInsurance = item => {
		handleInsurancesClose()
		router.push(item.link)
	}

	return (
		<div className="d-flex align-items-center">
			<Menu
				anchorEl={insurancesAnchorEl}
				handleClick={handleInsurancesClick}
				handleClose={handleInsurancesClose}
				items={insurances}
				label="انواع بیمه ها"
				onClick={onClickInsurance}
				getLabel={item => item.name}
			/>
			<Menu
				anchorEl={partnersAnchorEl}
				handleClick={handlePartnersClick}
				handleClose={handlePartnersClose}
				items={companiesList}
				label="بیمه های همکار"
				onClick={handlePartnersClose}
				getLabel={item => item.name}
			/>
		</div>
	)
}
