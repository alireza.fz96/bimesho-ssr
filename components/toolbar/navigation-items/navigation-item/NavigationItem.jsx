import React from 'react'
import { NavLink } from 'react-router-dom'
import Link from 'next/link'
import classes from './NavigationItem.module.scss'

const NavigationItem = ({ link, children }) => {
	return (
		<div className={classes.navItem}>
			<NavLink href={link}>{children}</NavLink>
		</div>
	)
}

export default NavigationItem
