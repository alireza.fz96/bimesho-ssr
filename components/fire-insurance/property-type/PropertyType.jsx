import React from 'react'
import { useSelector, useDispatch } from 'react-redux'
import {
	nextSlide,
	setBuildingType,
	setPropertyType
} from '../../../store/actions/fireInsuranceActions'
import Select from '../../UI/Select/Select'

const DamageCount = () => {
	const state = useSelector(state => {
		const { propertyType, buildingType } = state.fireInsurance.form
		const { propertyTypes, buildingTypes } = state.coreData
		return {
			propertyType,
			buildingType,
			propertyTypes,
			buildingTypes
		}
	})

	const dispatch = useDispatch()

	const onSetPropertyType = type => {
		dispatch(setPropertyType(type))
		if (state.buildingType) dispatch(nextSlide())
	}

	const onSetBuildingType = type => {
		dispatch(setBuildingType(type))
		if (state.propertyType) dispatch(nextSlide())
	}

	return (
		<div className="input-wrapper d-flex flex-column">
			<div className="mb-4">
				<Select
					label="نوع ملک"
					id="property-type"
					changed={onSetPropertyType}
					options={state.propertyTypes}
					value={state.propertyType || ''}
					renderer={value => value.name}
				/>
			</div>
			<Select
				label="نوع سازه"
				id="building-type"
				changed={onSetBuildingType}
				options={state.buildingTypes}
				value={state.buildingType || ''}
				renderer={value => value.name}
			/>
		</div>
	)
}

export default DamageCount
