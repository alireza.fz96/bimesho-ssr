import { Button, CircularProgress, TextField } from '@material-ui/core'
import React from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { setDone, setValue } from '../../../store/actions/fireInsuranceActions'
import { useRouter } from 'next/router'
import {
	fromSeparetedNumber,
	separateNumbers
} from '../../../utilities/utilities'

const UnitValue = () => {
	const state = useSelector(state => {
		const { value } = state.fireInsurance.form
		return {
			value
		}
	})

	const dispatch = useDispatch()

	const router = useRouter()

	const onSetValue = value => {
		dispatch(setValue(value))
	}

	const onNavigate = () => {
		dispatch(setDone())
		setEnquiryLoading(true)
		router.push('/fire-enquiry').finally(() => {
			setEnquiryLoading(false)
		})
	}

	const [enquiryLoading, setEnquiryLoading] = React.useState(false)

	return (
		<div className="input-wrapper d-flex flex-column">
			<TextField
				label="ارزش لوازم خانگی (تومان)"
				placeholder="ارزش لوازم خانگی (تومان)"
				value={separateNumbers(state.value) || ''}
				onChange={event => {
					const newValue = fromSeparetedNumber(event.target.value)
					if (+newValue || !newValue) onSetValue(newValue)
				}}
				variant="outlined"
			/>

			{state.value && (
				<div className="mt-5">
					{enquiryLoading ? (
						<div className="w-100 text-center">
							<CircularProgress />
						</div>
					) : (
						<Button
							fullWidth
							variant="contained"
							color="primary"
							onClick={onNavigate}
						>
							استعلام
						</Button>
					)}
				</div>
			)}
		</div>
	)
}

export default UnitValue
