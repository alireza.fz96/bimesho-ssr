import { Button, TextField } from '@material-ui/core'
import React from 'react'
import { useSelector, useDispatch } from 'react-redux'
import {
	nextSlide,
	setUnitCount,
	setUnitMeter
} from '../../../store/actions/fireInsuranceActions'
import { toEnglishDigit } from '../../../utilities/utilities'

const UnitCount = () => {
	const state = useSelector(state => {
		const { unitCount, unitMeter } = state.fireInsurance.form
		return {
			unitCount,
			unitMeter
		}
	})

	const dispatch = useDispatch()

	const onSetUnitCount = count => {
		dispatch(setUnitCount(count))
	}

	const onSetUnitMeter = meter => {
		dispatch(setUnitMeter(meter))
	}

	const onNextSlide = () => {
		dispatch(nextSlide())
	}

	return (
		<div className="input-wrapper d-flex flex-column">
			<div className="mb-4">
				<TextField
					label="تعداد واحد"
					placeholder="تعداد واحد"
					value={state.unitCount || ''}
					onChange={event => {
						const newValue = toEnglishDigit(event.target.value)
						if (+newValue || !newValue) onSetUnitCount(newValue)
					}}
					variant="outlined"
				/>
			</div>
			<TextField
				label="متراژ واحد"
				placeholder="متراژ واحد"
				value={state.unitMeter || ''}
				onChange={event => {
					const newValue = toEnglishDigit(event.target.value)
					if (+newValue || !newValue) onSetUnitMeter(newValue)
				}}
				variant="outlined"
			/>

			{state.unitCount && state.unitMeter && (
				<div className="mt-5">
					<Button
						fullWidth
						variant="contained"
						color="primary"
						onClick={onNextSlide}
					>
						ادامه
					</Button>
				</div>
			)}
		</div>
	)
}

export default UnitCount
