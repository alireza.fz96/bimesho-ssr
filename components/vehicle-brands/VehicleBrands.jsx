import React from 'react'
import { TextField, InputAdornment, CircularProgress } from '@material-ui/core'
import {
    setBrand as thirdSetBrand,
    nextSlide as thirdNextSlide
} from '../../store/actions/thirdPartyInsuranceActions'
import {
    setBrand as motorSetBrand,
    nextSlide as motorNextSlide
} from '../../store/actions/motorInsuranceActions'
import {
    nextSlide as bodyNextSlide,
    setBrand as bodySetBrand
} from '../../store/actions/bodyInsuranceActions'
import { useDispatch, useSelector } from 'react-redux'
import { useBrands } from '../../hooks/vehicles'

const VehicleBrands = ({ bodyInsurance, motor }) => {
    const state = useSelector(state => {
        let category, brand
        if (bodyInsurance) {
            category = state.bodyInsurance.form.category
            brand = state.bodyInsurance.form.brand
        } else if (motor) {
			category = state.motorInsurance.form.category
            brand = state.motorInsurance.form.brand
        } else {
            category = state.thirdPartyInsurance.form.category
            brand = state.thirdPartyInsurance.form.brand
        }
        return {
            category,
            brand
        }
    })
    const dispatch = useDispatch()

    const { brands, isLoading } = useBrands(state.category && state.category.id)

    const [searchParam, setSearchParam] = React.useState('')

    const filterBrands = () => {
        const newBrands = brands.filter(brand =>
            brand.name.includes(searchParam)
        )
        return newBrands
    }

    const onSetVehicleBrand = brand => {
        dispatch(
            bodyInsurance
                ? bodySetBrand(brand)
                : motor
                ? motorSetBrand(brand)
                : thirdSetBrand(brand)
        )
        dispatch(
            bodyInsurance
                ? bodyNextSlide()
                : motor
                ? motorNextSlide()
                : thirdNextSlide()
        )
    }

    return (
        <div>
            <div className="mb-3">
                <TextField
                    placeholder={motor ? 'برند موتور' : 'برند وسیله نقلیه'}
                    variant="outlined"
                    onChange={event => setSearchParam(event.target.value)}
                    InputProps={{
                        startAdornment: (
                            <InputAdornment position="start">
                                <img src={'/imgs/svg/search-icon.svg'} alt="search_icon" />
                            </InputAdornment>
                        )
                    }}
                />
            </div>
            <div className="grid-view__scroll">
                <div className="grid-view">
                    {isLoading && (
                        <div className="d-flex justify-content-center w-100 mt-4">
                            <CircularProgress size={60} />
                        </div>
                    )}
                    {!isLoading &&
                        filterBrands().map(brandItem => (
                            <div
                                key={brandItem.name}
                                className="grid-view__wrapper col-4 col-md-4 col-lg-3 col-xl-3"
                            >
                                <div
                                    className={
                                        'grid-view__item ' +
                                        (state.brand &&
                                        brandItem.id === state.brand.id
                                            ? 'active'
                                            : '')
                                    }
                                    onClick={() => onSetVehicleBrand(brandItem)}
                                >
                                    {state.category.name !== 'موتور سیکلت' && (
                                        <img
                                            src={`https://bimesho.com/images/carbase/${brandItem.icon}.png`}
                                            alt={brandItem.name + 'logo'}
                                            className="grid-view__img"
                                        />
                                    )}
                                    <span className="grid-view__title">
                                        {brandItem.name}
                                    </span>
                                </div>
                            </div>
                        ))}
                </div>
            </div>
        </div>
    )
}

export default VehicleBrands
