import React from 'react'

const About = () => {
	return (
		<>
			<p>
				با توجه به شرایط اقتصادی موجود در کشورمان، همه افراد به دنبال
				ایجاد یک پس‌انداز مناسب برای دوران بازنشستگی و همچنین تأمین
				نیازهای خانواده خود در سال‌های آینده هستند.
				<br />
				هزینه‌های سنگینی مثل هزینه تحصیل، ازدواج و راه اندازی کسب و کار
				فرزندان و در کنار آن مخارج بالای زندگی در دوران بازنشستگی، اهمیت
				داشتن یک پشتوانه مالی محکم را بیش از پیش مشخص می‌کند. اما
				مهم‌ترین سوال این است که چه نوع سرمایه‌گذاری برای ما مناسب خواهد
				بود؟ سپرده‌گذاری در بانک، بورس یا بیمه عمر؟
			</p>

			<h2>بیمه عمر چیست و چه فایده‌ای دارد؟</h2>

			<p>
				بیمه عمر یکی از محبوب‌ترین و رایج‌ترین بیمه‌ها در جهان است. این
				بیمه یک سرمایه‌گذاری امن و سودآور به همراه پوشش‌های بیمه‌ای
				گسترده محسوب می‌شود که ریسک زندگی افراد را در مورد آینده و خطرات
				احتمالی آن به شرکت بیمه منتقل می‌کند.
				<br />
				در واقع افراد دوراندیش و آینده نگر با هر توان مالی، با پس‌انداز
				مبلغی به صورت ماهیانه، سه ماهه، شش ماهه و یا سالانه با عنوان
				حق‌بیمه، علاوه بر یک اندوخته مالی مطمئن برای آینده، از امکانات و
				پوشش‌‌های بیمه عمر هم برخوردار خواهند شد.
			</p>

			<h2>شرایط بیمه عمر چیست؟</h2>

			<h2>محدوده سنی:</h2>
			<p>
				برای کودکان از بدو تولد و برای بزرگسالان تا ۷۰ سالگی بیمه عمر
				صادر می‌شود. البته به نسبت افزایش سن، حق بیمه هم افزایش پیدا
				خواهد کرد.
			</p>

			<h2>مدت قرارداد:</h2>

			<p>
				هر فرد به مدت حداقل 5 سال و حداکثر 30 سال میتواند تحت پوشش بیمه
				عمر قرار بگیرد که هر چه مدت قرارداد افزایش پیدا کند، سرمایه
				بیشتری به بیمه‌شده تعلق خواهد گرفت.
			</p>

			<h2>مبلغ حق بیمه:</h2>
			<p>
				در بیمه عمر حق بیمه ثابت و مشخصی وجود ندارد و هر شخصی می‌تواند
				با توجه به شرایط مالی خود، مبلغی را به عنوان حق بیمه انتخاب کند
				و آن را به شکل ماهانه، سه ماهه، شش ماهه و یک ساله پرداخت نماید.
				حداقل مبلغ ماهانه برای شروع بیمه عمر، 50 هزار تومان است، اما اگر
				می‌خواهید با هدف یک سرمایه‌گذاری سودآور بیمه عمر را خریداری
				کنید، بهتر است حق بیمه ماهانه خود را حداقل یک میلیون و دویست
				هزار تومان در نظر بگیرید. البته بهترین روش برای پرداخت حق بیمه
				عمر، به صورت سالانه است.
			</p>

			<h2>مزیت بیمه عمر چیست؟</h2>

			<h2>سرمایه‌گذاری به همراه سود تضمینی و سود مشارکت</h2>
			<p>
				سود اصلی و ثابت در بیمه عمر، سود تضمینی است که مقدار آن توسط
				بیمه مرکزی اعلام می‌شود و تمام شرکت‌های بیمه موظف به پرداخت آن
				به بیمه‌شدگان خود هستند. بر اساس آخرین مصوبه بیمه مرکزی، نرخ
				تضمینی سود سالانه بیمه عمر برای دو سال اول 16%، برای دو سال دوم
				13%، و مابقی سال‌ها 10% است.
				<br />
				اما سود مشارکت به شرکت بیمه انتخابی و فعالیت‌های اقتصادی آن
				بستگی دارد. به این معنی که شرکت‌های بیمه با استفاده از حق
				بیمه‌های بیمه عمر خود در فعالیت‌های اقتصادی مختلف سرمایه‌گذاری
				می‌کنند و بیمه‌شدگان بیمه عمر در سود حاصل از این سرمایه‌گذاری‌ها
				شریک خواهند شد. به همین دلیل سود مشارکت در منافع بیمه‌های عمر در
				شرکت‌های مختلف بیمه و حتی در سال‌های مختلف، متفاوت است.
			</p>

			<h2>پرداخت اندوخته بیمه عمر به صورت یکجا یا مستمری</h2>
			<p>
				در صورت حیات بیمه‌شده و در پایان مدت بیمه‌نامه، شرکت‌های بیمه
				اندوخته بیمه عمر را به اضافه مجموع سودهای تعلق گرفته به آن
				پرداخت خواهند کرد. بیمه‌شده‌ها می‌توانند سرمایه بیمه عمر خود را
				به صورت یکجا یا مستمری دریافت کنند.
			</p>

			<h2>دریافت وام</h2>
			<p>
				یکی از مزایای بیمه عمر، امکان دریافت وام از اندوخته بیمه‌نامه
				است. به این معنی که بیمه‌شده‌ها می‌توانند بدون نیاز به تشریفات
				اداری، ضامن یا وثیقه، حداکثر تا 90 درصد اندوخته خود را به صورت
				وام دریافت کنند. در این حالت نیازی به بازخرید کردن بیمه عمر نیست
				و پوشش‌های این بیمه هم ادامه پیدا خواهد کرد.
				<br />
				امکان دریافت وام از بیمه عمر، پس از گذشت 6 ماه تا 2 سال (بسته به
				نوع شرکت انتخابی) وجود دارد و نرخ سود این وام، حداکثر 4 درصد
				بیشتر از سود تضمینی متعلق به بیمه‌نامه عمر است.
			</p>

			<h2>سپرده گذاری</h2>
			<p>
				بر اساس قوانین بیمه مرکزی، بیمه‌گذار می‌تواند به جز پرداخت حق
				بیمه، مبالغی را هم به عنوان سپرده در بیمه عمر خود قرار دهد و از
				سود آن استفاده کند. همچنین امکان برداشت سپرده بیمه عمر در هر
				زمان وجود دارد، بدون اینکه بیمه‌شده نیاز به بازخرید یا توقف
				بیمه‌نامه داشته باشد.
			</p>

			<h2>پوشش بیماری‌‌های خاص</h2>
			<p>
				یکی دیگر از مزایای مهم بیمه عمر این است که بیمه‌شده برای درمان
				بیماری‌های خاص تحت پوشش بیمه قرار خواهد گرفت. منظور از
				بیماری‌های خاص هم سرطان، سکته قلبی، سکته مغزی، جراحی عروق قلبی
				(کرونر) و پیوند اعضای اصلی بدن است.
				<br />
				البته این پوشش مشروط به این است که بیمه‌شده قبل از ۶۰ سالگی و
				بعد از شروع بیمه عمر خود به این بیماری‌ها مبتلا شده باشد.
			</p>

			<h2>پوشش نقص عضو و از کار افتادگی</h2>
			<p>
				در صورت بروز حادثه و ازکار افتادگی کامل و دائم، شخص بیمه‌شده از
				پرداخت حق‌بیمه معاف می‌شود و یا در صورت نقص عضو، می‌تواند
				تا&nbsp;200%&nbsp;سرمایه فوت خود را ( بسته به نوع شرکت بیمه)
				دریافت کند.
			</p>

			<h2>پوشش‌‌های فوت طبیعی و فوت بر اثر حادثه</h2>
			<p>
				بیمه‌شده‌ها با پرداخت اولین قسط از بیمه‌نامه خود مشمول این پوشش
				می‌شوند و از این طریق، دیگر نگران آینده‌ی خانواده‌ خود در صورت
				بروز حادثه و فوت، نخواهند بود.
				<br />
				در زمان عقد قرارداد بیمه عمر، شخص بیمه‌شده می‌تواند تا ۱، ۲ و یا
				۳ برابر سرمایه فوت خود را به عنوان پوشش فوت بر اثر حادثه انتخاب
				کند.
				<br />
				درصورت فوت هم شرکت بیمه سرمایه بیمه عمر را به اضافه اندوخته
				تشکیل شده تا زمان فوت، به ذی‌نفعان (که در قرارداد بیمه ذکر شده)
				پرداخت می‌کند.
			</p>

			<h2>پوشش تکمیلی هزینه‌های پزشکی</h2>
			<p>
				در بسیاری از بیمه‌های عمر، هزینه‌های پزشکی ناشی از حادثه به
				بیمه‌شده پرداخت خواهد شد و این پوشش معمولا یک سقف حداکثری دارد.
			</p>

			<h2>مشخص کردن ‌ذی‌نفع قرارداد بیمه</h2>
			<p>
				از دیگر مزایا و شرایط بیمه عمر می‌توان به این نکته اشاره‌کرد که،
				بیمه‌گذار حق دارد ‌ذی‌نفع قرارداد را خود مشخص کند و در صورت
				تمایل هر سال آن را تغییر دهد.
			</p>

			<h2>انعطاف پذیری</h2>
			<p>
				شخص می‌تواند شرایط بیمه‌نامه خود را از نظر نحوه‌ی پرداخت، میزان
				مبلغ پرداختی و پرداخت حق‌بیمه در بازه‌های زمانی دلخواه مانند هر
				ماه، هر سه ماه، هر شش ماه و سالی یکبار تعیین کند.
			</p>

			<div>
				<h2>بیمه عمر و تشکیل سرمایه سامان</h2>
			</div>

			<p>
				بیمه سامان یکی از شرکت‌های موفق در زمینه صدور بیمه عمر است.
				<br />
				بیمه سامان علاوه بر ارائه پوشش‌های اصلی مانند پوشش‌های نقص‌عضو و
				ازکارافتادگی، بیماری‌های خاص و...به تازگی پوشش‌های تأمین
				هزینه‌های بیماری‌های خاص و سرطان را تا سقف&nbsp;
				<strong>500 میلیون تومان</strong> ، تأمین هزینه‌های بیمارستانی
				را تا سقف&nbsp;<strong>20&nbsp;میلیون تومان</strong>، جبران
				خسارت‌های وارد شده به ساختمان‌ها و تاسیسات در مقابل آتش‌سوزی،
				صاعقه، انفجار، سیل، سرقت و مسئولیت مالی را تا سقف&nbsp;
				<strong>200 &nbsp;میلیون تومان</strong> و پوشش جدید سرقت تلفن
				همراه، تبلت، لپ تاپ را تا سقف&nbsp;
				<strong>20 &nbsp;میلیون تومان</strong>
				به بیمه‌نامه خود اضافه کرده است.
			</p>

			<div>
				<h2>بیمه عمر و تامین آتیه پاسارگاد</h2>
			</div>

			<p>
				بر اساس آمار بیمه مرکزی، بیمه پاسارگاد در حال حاضر پرفروش‌ترین
				شرکت بیمه از نظر میزان فروش بیمه عمر است. بیمه عمر پاسارگاد در
				دو رشته بیمه عمر و تأمین آتیه و بیمه عمر به شرط فوت ارائه
				می‌شود.
				<br />
				از جمله پوشش‌های ویژه‌ای که شرکت بیمه پاسارگاد به بیمه‌گذاران
				خود ارائه می‌دهد، پوشش آتش‌سوزی منزل مسکونی تا سقف&nbsp;500
				&nbsp;میلیون تومان سرمایه و ارائه پوشش بیمه تکمیلی است.
			</p>

			<div>
				<h2>بیمه عمر ایران</h2>
			</div>

			<p>
				بیمه ایران به عنوان تنها شرکت بیمه دولتی در کشور، توانسته رتبه
				دوم فروش بیمه عمر را کسب کند.
				<br />
				جدیدترین و کاربردی‌ترین طرح بیمه عمر شرکت ایران طرح "بیمه مان"
				است و موارد زیر را می‌توان به عنوان ویژگی‌های مهم این طرح بیمه
				عمر نام برد.
			</p>

			<div>
				<h2>تفاوت‌‌های سرمایه‌گذاری در بانک و خرید بیمه عمر</h2>
			</div>

			<div>
				یکی از بهترین روش‌‌های سرمایه‌گذاری بلندمدت، خرید بیمه عمر است و
				در مقایسه با سرمایه‌گذاری در بانک مزایای بسیاری دارد.
				<br />
				در این بخش قصد داریم تا تفاوت‌‌های خرید بیمه عمر و سپرده‌گذاری
				در بانک را به صورت خلاصه توضیح دهیم:
				<ul>
					<li>
						اگر قصد شما سرمایه‌گذاری کوتاه‌مدت است، سپرده‌گذاری در
						بانک‌ها به نظر جذاب‌تر می‌آید، اما باید در ابتدا مبلغ
						بسیاری را در بانک سپرده‌گذاری کنید تا سود قابل ‌توجهی را
						بدست بیاورید. ولی در مورد خرید بیمه عمر ، کافی‌ست تنها
						یک حق‌بیمه را پرداخت کنید تا تحت پوشش بیمه‌ای نیز قرار
						بگیرید؛ در صورتی که بانک فاقد این پوشش‌می‌باشد.
					</li>
					<li>
						در حساب‌ها و سپرده‌گذاری‌‌های بلندمدت بانکی، امکان
						برداشت پول قبل از اتمام قرارداد وجود ندارد، اما در بیمه
						عمر امکان برداشت اندوخته (وام بدون ضامن) طی مدت قرارداد
						وجود دارد.
					</li>
					<li>
						یکی از تفاوت‌‌های قابل توجهی که بین سپرده‌گذاری در بانک
						و خرید بیمه عمر مطرح است، متغیر و ثابت بودن نرخ سود
						پرداختی به اندوخته شما است. در مورد سپرده‌گذاری در
						بانک‌ها، نرخ سود بانکی تحت شرایط متفاوت متغیر است، اما
						در مورد بیمه عمر نرخ سود تا پایان، روندی مشخص و ثابت
						دارد و تغییری نمی‌کند.
					</li>
					<li>
						تفاوت عمده دیگر در حق برداشت طلبکاران است. در صورتی که
						پس‌اندازکننده، یک سپرده در بانک داشته باشد، طبق قانون،
						در صورت از کار افتادگی او و بروز مشکلات دیگر، طلبکاران
						می‌توانند از محل سپرده‌‌های بانکی، طلب خود را وصول کنند.
						در بیمه عمر چنین برداشتی امکان پذیر نیست.
					</li>
					<li>
						یک تفاوت مهم دیگر نیز وجود دارد، تا زمانی که روند
						پس‌انداز در بانک‌ها ادامه و یا سرمایه‌ها در اختیار آن‌ها
						قرار دارد، به پس‌انداز‌کننده سود پرداخت می‌کنند و در
						صورت از کارافتادگی کلی پس‌اندازکننده و کاهش درآمد او،
						روند پس‌انداز کردن متوقف می‌شود؛ اما بیمه عمر از آنجایی
						که علاوه بر پس‌انداز و سرمایه‌گذاری جنبه پوشش‌دهی ریسک
						هم دارد در صورت از کارافتادگی دائم و کلی، بیمه‌شده از
						پرداخت حق‌بیمه معاف می‌شود و شرکت بیمه ملزم به اجرای
						تعهدات مندرج در بیمه‌نامه است.
					</li>
				</ul>
			</div>

			<div>
				<h2>محاسبه بیمه عمر</h2>
			</div>

			<p>
				نرخ بیمه عمر مقدار مشخصی ندارد و با توجه به عواملی از جمله سن،
				جنسیت، وضعیت جهانی، تاریخچه سلامت خانواده، شغل و حرفه، انتخاب
				پوشش‌های متفاوت و تعیین میزان حق‌بیمه پرداختی به دلخواه شخص
				بیمه‌گذار، قابل تعیین است.
			</p>

			<div>
				<h2>فسخ قرارداد بیمه عمر</h2>
			</div>

			<p>
				بیمه‌گر می‌تواند در هر زمان که بخواهد این بیمه‌نامه را فسخ کند و
				در تاریخ فسخ می‌تواند ارزش باز خریدی بیمه‌نامه خود را دریافت
				کند.
			</p>

			<div>
				<h2>هماهنگی بیمه عمر با تورم</h2>
			</div>

			<div>
				سپرده‌‌‌های بانکی با گذشت زمان و در اثر تورم ارزش کمتری پیدا
				می‌کنند، به دلیل همین ویژگی، هماهنگی با تورم در بیمه‌نامه عمر
				ایجاد شده است که به شیوه‌‌‌های زیر امکان پذیر است:
				<ul>
					<li>
						اولین راهکار افزایش ‌‌حق‌بیمه متناسب با درصد تورم بین ۵
						تا ۲5 درصد هر سال نسبت به سال قبل و متناسب با افزایش
						سرمایه فوت و اندوخته است.
					</li>
					<li>
						راهکار بعدی افزایش ‌‌حق‌بیمه با درصدی بیشتر از درصد
						افزایش سرمایه فوت است.
					</li>
					<li>
						افزایش ‌‌حق‌بیمه و ثابت نگه‌داشتن سرمایه فوت نیز راهکار
						دیگری برای هماهنگی با تورم است.
					</li>
				</ul>
			</div>

			<div>
				<h2>بهترین بیمه عمر</h2>
			</div>

			<p>
				در انتخاب بهترین بیمه عمر هدف بیمه‌گذار از خرید این بیمه بسیار
				مهم است. افراد بیمه‌گذار می‌توانند هدف‌های متفاوتی ازجمله اهمیت
				پوشش‌های این بیمه به تنهایی، سرمایه‌گذاری بلندمدت از طریق خرید
				بیمه عمر، نحوه و پرداخت خسارت به موقع هر شرکت بیمه، میزان
				توانگری مالی شرکت‌های بیمه و ... داشته باشند و به طبع بهترین
				بیمه عمر با توجه به هدف هر فرد، متفاوت است. که در این راه
				می‌توانید از متخصصین شرکت "بیمه شو" کمک بگیرید.
				<br />
				از آنجایی که بیمه شو از طریق شرکت‌های مختلف بیمه عمر را ارائه
				می‌دهد، مشاوران بیمه شو با آگاهی از خواسته‌ها و شرایط شما،
				بهترین بیمه را به صورت بی طرفانه پیشنهاد می‌کنند و به تمام
				پرسش‌ها و ابهامات شما با دقت و حوصله پاسخ خواهند داد.
			</p>
		</>
	)
}

export default About
