import React from 'react'
import { useSelector, useDispatch } from 'react-redux'
import {
	nextSlide,
	setParam
} from '../../../store/actions/lifeInsuranceActions'
import Select from '../../UI/Select/Select'

const DamageCount = () => {
	const state = useSelector(state => {
		const { paymentMethod, insuranceRight } = state.lifeInsurance.form
		const { insuranceRights, paymentMethods } = state.coreData
		return {
			paymentMethod,
			insuranceRight,
			paymentMethods,
			insuranceRights
		}
	})
	const dispatch = useDispatch()

	const onSetParam = (field, value) => {
		dispatch(setParam(field, value))
		const param =
			field === 'insuranceRight' ? 'paymentMethod' : 'insuranceRight'
		if (state[param] && value) dispatch(nextSlide())
	}

	return (
		<div className="input-wrapper d-flex flex-column">
			<div className="mb-4">
				<Select
					label="حق بیمه"
					id="insurance-right"
					changed={value => onSetParam('insuranceRight', value)}
					options={state.insuranceRights}
					value={state.insuranceRight || ''}
					renderer={value => value.name}
				/>
			</div>
			<Select
				label="نحوه ی پرداخت"
				id="payment method"
				changed={value => onSetParam('paymentMethod', value)}
				options={state.paymentMethods}
				value={state.paymentMethod || ''}
				renderer={value => value.name}
			/>
		</div>
	)
}

export default DamageCount
