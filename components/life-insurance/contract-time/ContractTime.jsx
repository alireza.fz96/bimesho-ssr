import { Button, CircularProgress } from '@material-ui/core'
import React from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { setParam } from '../../../store/actions/lifeInsuranceActions'
import Select from '../../UI/Select/Select'
import { useRouter } from 'next/router'

const ContractTime = () => {
	const state = useSelector(state => {
		const { contractTime, birthday } = state.lifeInsurance.form
		const { contractTimes, years } = state.coreData
		return {
			contractTime,
			contractTimes,
			years,
			birthday
		}
	})
	const dispatch = useDispatch()

	const onSetParam = (field, value) => {
		dispatch(setParam(field, value))
		const otherField =
			field === 'contractTime' ? 'birthday' : 'contractTime'
		if (state[otherField]) onCheckEnquiry()
	}

	const router = useRouter()

	const onCheckEnquiry = () => {
		dispatch(setParam('done', true))
		setEnquiryLoading(true)
		router.push('/life-enquiry').finally(() => {
			setEnquiryLoading(false)
		})
	}

	const canCheck = () => {
		return state.contractTime && state.birthday
	}

	const renderer = year => (
		<>
			<span className="year miladi">{year.miladi}</span>|
			<span className="year">{year.jalali}</span>
		</>
	)

	const [enquiryLoading, setEnquiryLoading] = React.useState(false)

	return (
		<div className="input-wrapper d-flex flex-column">
			<div className="mb-4">
				<Select
					label="طول مدت قرارداد"
					id="contract-time"
					changed={value => onSetParam('contractTime', value)}
					options={state.contractTimes}
					value={state.contractTime || ''}
					renderer={value => value.name}
				/>
			</div>
			<Select
				label="سال تولد"
				id="birthday"
				changed={value => onSetParam('birthday', value)}
				options={state.years}
				value={state.birthday || ''}
				renderer={renderer}
			/>
			{enquiryLoading ? (
				<div className="w-100 text-center mt-4">
					<CircularProgress />
				</div>
			) : (
				<Button
					color="primary"
					variant="contained"
					onClick={onCheckEnquiry}
					className="mt-5"
					disabled={!canCheck()}
				>
					استعلام
				</Button>
			)}
		</div>
	)
}

export default ContractTime
