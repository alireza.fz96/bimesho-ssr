import React from 'react'
import { useSelector, useDispatch } from 'react-redux'
import {
    setInsuranceDiscount,
    nextSlide,
    setDriverAccidentDiscount
} from '../../store/actions/thirdPartyInsuranceActions'
import {
    setInsuranceDiscount as motorSetInsuranceDiscount,
    nextSlide as motorNextSlide,
    setDriverAccidentDiscount as motorSetDriverAccidentDiscount
} from '../../store/actions/motorInsuranceActions'
import Select from '../UI/Select/Select'
import classes from './InsuranceDiscount.module.scss'
import { Dialog } from '@material-ui/core'

const InsuranceDiscount = ({ motor }) => {
    const state = useSelector(state => {
        const { driverAccidentDamageDiscount, insuranceDiscount } = motor
            ? state.motorInsurance.form
            : state.thirdPartyInsurance.form
        const {
            driverAccidentDamageDiscounts,
            insuranceDiscounts
        } = state.coreData

        return {
            driverAccidentDamageDiscount,
            driverAccidentDamageDiscounts,
            insuranceDiscount,
            insuranceDiscounts
        }
    })
    const dispatch = useDispatch()

    const onSetInsuranceDiscount = discount => {
        dispatch(
            motor
                ? motorSetInsuranceDiscount(discount)
                : setInsuranceDiscount(discount)
        )
        if (state.driverAccidentDamageDiscount)
            dispatch(motor ? motorNextSlide() : nextSlide())
    }

    const onSetDriverAccidentDiscount = discount => {
        dispatch(
            motor
                ? motorSetDriverAccidentDiscount(discount)
                : setDriverAccidentDiscount(discount)
        )
        if (state.insuranceDiscount)
            dispatch(motor ? motorNextSlide() : nextSlide())
    }

    const [open, setOpen] = React.useState(false)

    const onOpenHelp = () => {
        setOpen(true)
    }

    const handleClose = () => {
        setOpen(false)
    }

    return (
        <div className="input-wrapper">
            <div className="help-input">
                <Select
                    id="prev-insurance-discount"
                    value={state.insuranceDiscount || ''}
                    changed={onSetInsuranceDiscount}
                    label="تخفیف شخص ثالث"
                    options={state.insuranceDiscounts}
                    renderer={option => option.name}
                ></Select>
                <div className="extra-badge" onClick={onOpenHelp}>
                    راهنما
                </div>
            </div>
            <div className="mt-4">
                <div className="help-input">
                    <Select
                        label="تخفیف حوادث راننده"
                        id="driver-accident-discount"
                        changed={onSetDriverAccidentDiscount}
                        options={state.driverAccidentDamageDiscounts}
                        value={state.driverAccidentDamageDiscount || ''}
                        renderer={value => value.name}
                    />
                    <div className="extra-badge" onClick={onOpenHelp}>
                        راهنما
                    </div>
                </div>
            </div>
            <Dialog onClose={handleClose} open={open}>
                <img
                    src={'/imgs/help/1.jpg'}
                    alt="help_img"
                    onClick={handleClose}
                    className="img-fluid"
                />
            </Dialog>
            <div className={classes.messageBox + ' mt-4'}>
                <p className="mb-3">
                    درصد تخفیف خود را مطابق
                    <span style={{ color: '#00dadb' }}> راهنما و عینا </span>
                    از روی بیمه‌نامه وارد کنید
                </p>
                <p>
                    به تخفیف درج‌شده در بیمه‌نامه عددی را اضافه نکنید؛ تخفیف سال
                    جاری به صورت خودکار محاسبه می‌شود
                </p>
            </div>
        </div>
    )
}

export default InsuranceDiscount
