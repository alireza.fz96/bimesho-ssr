import React from 'react'
import { useSelector, useDispatch } from 'react-redux'
import {
	setFinanceDamageCount,
	setLifeDamageCount
} from '../../../store/actions/thirdPartyInsuranceActions'
import Select from '../../UI/Select/Select'

const DamageCount = () => {
	const state = useSelector(state => {
		const {
			financeDamageCount,
			lifeDamageCount
		} = state.thirdPartyInsurance.form
		const { lifeDamageCounts, financeDamageCounts } = state.coreData
		return {
			finance: financeDamageCount,
			life: lifeDamageCount,
			financeDamageCounts,
			lifeDamageCounts
		}
	})
	const dispatch = useDispatch()

	const onSetLifeDamageCount = count => {
		dispatch(setLifeDamageCount(count, state.finance))
	}

	const onSetFinanceDamageCount = count => {
		dispatch(setFinanceDamageCount(count, state.life))
	}

	return (
		<div className="input-wrapper d-flex flex-column">
			<div className="mb-4">
				<Select
					label="تعداد خسارات جانی"
					id="life-damge-counts"
					changed={onSetLifeDamageCount}
					options={state.lifeDamageCounts}
					value={state.life || ''}
					renderer={value => value.name}
				/>
			</div>
			<Select
				label="تعداد خسارات مالی"
				id="finance-damge-counts"
				changed={onSetFinanceDamageCount}
				options={state.financeDamageCounts}
				value={state.finance || ''}
				renderer={value => value.name}
			/>
		</div>
	)
}

export default DamageCount
