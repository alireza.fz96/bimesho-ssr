import React from 'react'
import classes from './ThirdPartyFilter.module.scss'
import { Slider, makeStyles } from '@material-ui/core'
import { Add, Remove } from '@material-ui/icons'

const marks = [
    {
        value: 11
    },
    {
        value: 15
    },
    {
        value: 20
    },
    {
        value: 22
    },
    {
        value: 30
    },
    {
        value: 34
    },
    {
        value: 40
    },
    {
        value: 50
    },
    {
        value: 55
    },
    {
        value: 90
    },
    {
        value: 140
    },
    {
        value: 180
    },
    {
        value: 220
    }
]

const useStyle = makeStyles(theme => ({
    root: {
        flex: '1',
        '& .MuiSlider-root': {
            display: 'flex',
            alignItems: 'center',
            padding: '0',
            height: '100%'
        },
        '& .MuiSlider-rail': {
            backgroundColor: '#d3e5fe',
            opacity: 1,
            top: 0,
            height: '100%'
        },
        '& .MuiSlider-track': {
            backgroundColor: '#2659c1',
            transition: '0.2s linear',
            top: 0,
            height: '100%'
        },
        '& .MuiSlider-thumb': {
            position: 'relative',
            top: 2,
            width: 35,
            height: '100%',
            borderRadius: '5px 0 0 5px',
            transition: 'left 0.2s linear',
            zIndex: 2,
            boxShadow: 'none',
            backgroundColor: '#2659c1',
            transform: 'translateX(-10px)',
            '&::after,&::before': {
                content: '""',
                display: 'inline-block',
                position: 'absolute',
                backgroundColor: '#fff',
                width: 3,
                height: 3,
                borderRadius: '50%',
                right: 10
            },
            '&::after': {
                top: '50%',
                left: 22,
                bottom: 0,
                transform: 'translateY(-7px)'
            },
            '&::before': {
                top: '50%',
                transform: 'translate(0)',
                boxShadow: '0 7px 0 0 #fff'
            }
        },
        '& .MuiSlider-mark': { backgroundColor: 'transparent !important' }
    }
}))

const ThirdPartyFilter = ({ changed, motor }) => {
    const [coverMoney, setCoverMoney] = React.useState(11)

    const findValue = () => {
        return marks.findIndex(m => m.value === coverMoney)
    }

    const onAddCoverMoney = () => {
        if (coverMoney < 220) {
            const newValue = marks[findValue() + 1].value
            setCoverMoney(newValue)
            changed(newValue)
        }
    }

    const onSubtractCoverMoney = () => {
        if (coverMoney > 11) {
            const newValue = marks[findValue() - 1].value
            setCoverMoney(newValue)
            changed(newValue)
        }
    }

    const styles = useStyle()

    return (
        <div className={classes.cover}>
            <div className={classes.cover__body}>
                <div className={classes.cover__title}>تغییر پوشش مالی</div>
                <div className={classes.cover__icons}>
                    <div className="mt-2">
                        <img
                            src={motor ? '/imgs/svg/motor-icon.svg' : '/imgs/svg/car-ic.svg'}
                            alt={'car_image'}
                            className={
                                classes.cover__icon + ' ' + motor
                                    ? classes.cover__motor
                                    : ''
                            }
                        />
                    </div>
                </div>
            </div>
            <div className={classes.cover__range}>
                <div className="mb-3 mb-xs-0">
                    <div className={classes.rangeInput}>
                        <div
                            className={
                                classes.rangeInput__offset +
                                ' ' +
                                classes.rangeInput__offset__right
                            }
                        >
                            <Remove
                                style={{
                                    cursor: 'pointer'
                                }}
                                onClick={onSubtractCoverMoney}
                            />
                        </div>
                        <div className={styles.root}>
                            <Slider
                                value={coverMoney}
                                onChange={(_, value) =>
                                    value >= 11 && setCoverMoney(value)
                                }
                                onChangeCommitted={(_, value) => changed(value)}
                                max={220}
                                min={0}
                                step={null}
                                marks={marks}
                            />
                        </div>
                        <div
                            className={
                                classes.rangeInput__offset +
                                ' ' +
                                classes.rangeInput__offset__left
                            }
                        >
                            <Add
                                style={{
                                    cursor: 'pointer'
                                }}
                                onClick={onAddCoverMoney}
                            />
                        </div>
                    </div>
                </div>
            </div>
            <div className={classes.cover__info}>
                <p className="mb-3">تعهد مالی: {coverMoney} میلیون تومان</p>
                <div className=" d-none d-xl-block">
                    <p className="mb-3">تعهد جانی: 440 میلیون تومان </p>
                    <p>تعهد راننده: 330 میلیون تومان </p>
                </div>
            </div>
        </div>
    )
}

export default ThirdPartyFilter
