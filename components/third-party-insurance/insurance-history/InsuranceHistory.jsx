import { useState } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import RadioGroup from '@material-ui/core/RadioGroup'
import FormControlLabel from '@material-ui/core/FormControlLabel'
import Radio from '@material-ui/core/Radio'
import {
	setDatePrice,
	setPrevCompany,
	nextSlide,
	done
} from '../../../store/actions/thirdPartyInsuranceActions'
import {
	setDatePrice as motorSetDatePrice,
	setPrevCompany as motorSetPrevCompany,
	nextSlide as motorNextSlide,
	done as motorDone
} from '../../../store/actions/motorInsuranceActions'
import { Button, CircularProgress } from '@material-ui/core'
import { useRouter } from 'next/router'
import { DatePicker } from '@material-ui/pickers'

const InsuranceHistory = ({ motor }) => {
	const router = useRouter()

	const state = useSelector(state => {
		return {
			companiesList: state.coreData.companiesList,
			datePrice: motor
				? state.motorInsurance.form.datePrice
				: state.thirdPartyInsurance.form.datePrice,
			prevCompany: motor
				? state.motorInsurance.form.prevCompany
				: state.thirdPartyInsurance.form.prevCompany
		}
	})
	const dispatch = useDispatch()

	const onSetDatePrice = date => {
		dispatch(motor ? motorSetDatePrice(date) : setDatePrice(date))
	}

	const onSetPrevCompany = prevCompany => {
		if (prevCompany == '0') setHasPrevInsurance(true)
		else if (prevCompany === '120' || prevCompany === '110') {
			setHasPrevInsurance(false)
		}
		dispatch(
			motor
				? motorSetPrevCompany(prevCompany)
				: setPrevCompany(prevCompany)
		)
		if (
			prevCompany !== '120' &&
			prevCompany !== '110' &&
			prevCompany !== '0'
		)
			dispatch(motor ? motorNextSlide() : nextSlide())
		if (prevCompany === '120') onCheckEnquiry()
	}

	const [enquiryLoading, setEnquiryLoading] = useState(false)

	const onCheckEnquiry = () => {
		dispatch(motor ? motorDone() : done())
		setEnquiryLoading(true)
		router
			.push(motor ? '/motor-enquiry' : '/third-party-enquiry')
			.finally(() => {
				setEnquiryLoading(false)
			})
	}

	const [hasPrevInsurance, setHasPrevInsurance] = React.useState(false)

	return (
		<div style={{ overflowX: 'hidden' }} className="px-3">
			<RadioGroup
				name="gender1"
				value={state.prevCompany}
				onChange={(event, value) => onSetPrevCompany(value)}
			>
				<FormControlLabel
					value="120"
					control={<Radio color="primary" />}
					label="فاقد بیمه نامه"
				/>
				<FormControlLabel
					value="110"
					control={<Radio color="primary" />}
					label={motor ? 'موتور صفر کیلومتر' : 'خودرو صفر کیلومتر'}
				/>
				<FormControlLabel
					value="0"
					control={
						<Radio color="primary" checked={hasPrevInsurance} />
					}
					label="دارای بیمه نامه قبلی از‌"
				/>
			</RadioGroup>
			{state.prevCompany === '110' && (
				<div className="input-wrapper my-4">
					<DatePicker
						value={state.datePrice}
						labelFunc={date =>
							date ? date.format('jYYYY/jMM/jDD') : ''
						}
						onChange={date => {
							onSetDatePrice(date)
						}}
						variant="inline"
						inputVariant="outlined"
						label={
							motor ? 'تاریخ ترخیص موتور' : 'تاریخ ترخیص خودرو'
						}
						autoOk
					/>
				</div>
			)}
			{(state.prevCompany === '110' || state.prevCompany === '120') && (
				<div className="input-wrapper mt-5">
					{enquiryLoading ? (
						<div
							className="text-center w-100"
							style={{ overflow: 'hidden' }}
						>
							<CircularProgress />
						</div>
					) : (
						<Button
							color="primary"
							variant="contained"
							onClick={() => onCheckEnquiry()}
							disabled={
								state.prevCompany === '110' && !state.datePrice
							}
							fullWidth
						>
							استعلام
						</Button>
					)}
				</div>
			)}
			{hasPrevInsurance && (
				<div className="grid-view__scroll">
					<div className="grid-view">
						{state.companiesList.map(company => (
							<div
								key={company.name}
								className="grid-view__wrapper col-4 col-md-4 col-lg-3 col-xl-3"
							>
								<div
									className={
										'grid-view__item ' +
										(state.prevCompany &&
										company.id === state.prevCompany
											? 'active'
											: '')
									}
									onClick={() => onSetPrevCompany(company.id)}
								>
									<img
										src={`/imgs/insurance-companies/${company.id}.png`}
										alt={company.name + 'logo'}
										className="grid-view__img"
									/>
									<span className="grid-view__title">
										{company.name}
									</span>
								</div>
							</div>
						))}
					</div>
				</div>
			)}
		</div>
	)
}

export default InsuranceHistory
