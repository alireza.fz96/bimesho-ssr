import React from 'react'
import { useSelector, useDispatch } from 'react-redux'
import {
    setDriverAccidentCount,
    setDriverAccidentDiscount,
    done
} from '../../../store/actions/thirdPartyInsuranceActions'
import { Button } from '@material-ui/core'
import { useRouter } from 'next/router'
import Select from '../../UI/Select/Select'

const DamageCount = () => {
    let router = useRouter()

    const state = useSelector(state => {
        const {
            driverAccidentDamageDiscount,
            driverAccidentDamageCount
        } = state.thirdPartyInsurance.form
        const {
            driverAccidentDamageCounts,
            driverAccidentDamageDiscounts
        } = state.coreData
        return {
            driverAccidentDamageCounts,
            driverAccidentDamageDiscounts,
            count: driverAccidentDamageCount,
            discount: driverAccidentDamageDiscount
        }
    })
    const dispatch = useDispatch()

    const onSetDriverAccidentCount = count => {
        dispatch(setDriverAccidentCount(count))
        next(count && state.discount)
    }

    const onSetDriverAccidentDiscount = discount => {
        dispatch(setDriverAccidentDiscount(discount))
        next(discount && state.count)
    }

    const navigateToEnquiry = () => {
        dispatch(done())
        router.push(motor ? '/motor-enquiry' : '/third-party-enquiry')
    }

    const next = shouldNavigate => {
        shouldNavigate && navigateToEnquiry()
    }

    return (
        <div className="input-wrapper d-flex flex-column ">
            <div className="mb-4">
                <Select
                    label="درصد تخفیف حوادث راننده"
                    id="driver-accident-discount"
                    changed={onSetDriverAccidentDiscount}
                    options={state.driverAccidentDamageDiscounts}
                    value={state.discount || ''}
                    renderer={value => value.name}
                />
            </div>
            {state.count && state.discount && (
                <Button
                    color="primary"
                    onClick={navigateToEnquiry}
                    variant="contained"
                >
                    استعلام قیمت
                </Button>
            )}
        </div>
    )
}

export default DamageCount
