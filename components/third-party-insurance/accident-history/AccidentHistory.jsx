import React, { useState } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import {
	setFinanceDamageCount,
	setLifeDamageCount,
	setDriverAccidentCount,
	done,
	setNoAccident
} from '../../../store/actions/thirdPartyInsuranceActions'
import {
	setFinanceDamageCount as motorSetFinanceDamageCount,
	setLifeDamageCount as motorSetLifeDamageCount,
	setDriverAccidentCount as motorSetDriverAccidentCount,
	done as motorDone,
	setNoAccident as motorSetNoAccident
} from '../../../store/actions/motorInsuranceActions'
import Select from '../../UI/Select/Select'
import RadioGroup from '@material-ui/core/RadioGroup'
import FormControlLabel from '@material-ui/core/FormControlLabel'
import Radio from '@material-ui/core/Radio'
import { Button, CircularProgress, Dialog } from '@material-ui/core'
import { useRouter } from 'next/router'

const AccidentHistory = ({ motor }) => {
	const state = useSelector(state => {
		const {
			financeDamageCount,
			lifeDamageCount,
			driverAccidentDamageCount
		} = motor ? state.motorInsurance.form : state.thirdPartyInsurance.form
		const {
			lifeDamageCounts,
			financeDamageCounts,
			driverAccidentDamageCounts
		} = state.coreData
		return {
			finance: financeDamageCount,
			life: lifeDamageCount,
			financeDamageCounts,
			lifeDamageCounts,
			driverAccidentDamageCount,
			driverAccidentDamageCounts
		}
	})
	const dispatch = useDispatch()

	const onSetLifeDamageCount = count => {
		dispatch(
			motor ? motorSetLifeDamageCount(count) : setLifeDamageCount(count)
		)
		if (count && state.finance && state.driverAccidentDamageCount)
			onSubmit()
	}

	const onSetFinanceDamageCount = count => {
		dispatch(
			motor
				? motorSetFinanceDamageCount(count)
				: setFinanceDamageCount(count)
		)
		if (count && state.life && state.driverAccidentDamageCount) onSubmit()
	}

	const onSetDriverAccidentCount = count => {
		dispatch(
			motor
				? motorSetDriverAccidentCount(count)
				: setDriverAccidentCount(count)
		)
		if (count && state.finance && state.life) onSubmit()
	}

	const router = useRouter()

	const [enquiyLoading, setEnquiryLoading] = useState(false)

	const onSubmit = () => {
		dispatch(motor ? motorDone() : done())
		setEnquiryLoading(true)
		router
			.push(motor ? '/motor-enquiry' : '/third-party-enquiry')
			.finally(() => setEnquiryLoading(false))
	}

	const isValid =
		state.finance && state.driverAccidentDamageCount && state.life

	const [hasDamage, setHasDamage] = React.useState('')

	const [dialog, setDialog] = React.useState({
		open: false,
		content: null
	})

	const onOpenHelp = content => {
		setDialog({ open: true, content })
	}

	const handleClose = () => {
		setDialog({ open: false, content: null })
	}

	const onNoAccident = () => {
		dispatch(motor ? motorSetNoAccident() : setNoAccident())
		onSubmit()
	}

	return (
		<div className="px-3">
			<RadioGroup
				name="gender1"
				value={hasDamage}
				onChange={(event, value) => {
					setHasDamage(value)
					if (value === 'no') onNoAccident()
				}}
			>
				<FormControlLabel
					value="no"
					control={<Radio color="primary" />}
					label="بدون خسارت"
				/>
				<FormControlLabel
					value="yes"
					control={<Radio color="primary" />}
					label="دارای خسارت"
				/>
			</RadioGroup>
			{hasDamage === 'yes' && (
				<div className="input-wrapper d-flex flex-column mt-3">
					<div className="mb-4">
						<div className="help-input">
							<Select
								label="خسارت مالی"
								id="finance-damge-counts"
								changed={onSetFinanceDamageCount}
								options={state.financeDamageCounts}
								value={state.finance || ''}
								renderer={value => value.name}
							/>
							<div
								className="extra-badge"
								onClick={() =>
									onOpenHelp(
										<p className="p-3">
											تعداد دفعاتی که خسارت مالی شخص ثالث
											را از بیمه‌نامه قبلی خود دریافت
											کرده‌اید.
										</p>
									)
								}
							>
								راهنما
							</div>
						</div>
					</div>
					<div className="mb-4">
						<div className="help-input">
							<Select
								label="خسارت جانی"
								id="life-damge-counts"
								changed={onSetLifeDamageCount}
								options={state.lifeDamageCounts}
								value={state.life || ''}
								renderer={value => value.name}
							/>
							<div
								className="extra-badge"
								onClick={() =>
									onOpenHelp(
										<p className="p-3">
											تعداد دفعاتی که خسارت جانی شخص ثالث
											(شامل دیه جرح، دیه فوت و دیه نقص
											عضو) را از بیمه‌نامه قبلی خود دریافت
											کرده‌اید.
										</p>
									)
								}
							>
								راهنما
							</div>
						</div>
					</div>
					<div className="mb-4">
						<div className="help-input">
							<Select
								label="خسارت حوادث راننده"
								id="driver-accident-count"
								changed={onSetDriverAccidentCount}
								options={state.driverAccidentDamageCounts}
								value={state.driverAccidentDamageCount || ''}
								renderer={value => value.name}
							/>
							<div
								className="extra-badge"
								onClick={() =>
									onOpenHelp(
										<p className="p-3">
											تعداد دفعاتی که خسارت جانی راننده
											خودرو (شامل دیه جرح، دیه فوت و دیه
											نقص عضو) را از بیمه‌نامه قبلی خود
											دریافت کرده‌اید.
										</p>
									)
								}
							>
								راهنما
							</div>
						</div>
					</div>
					<div className="mt-4">
						{enquiyLoading ? (
							<div className="w-100 text-center">
								<CircularProgress />
							</div>
						) : (
							<Button
								color="primary"
								variant="contained"
								disabled={!isValid}
								fullWidth
								onClick={onSubmit}
							>
								استعلام
							</Button>
						)}
					</div>
				</div>
			)}
			<Dialog onClose={handleClose} open={dialog.open}>
				<div onClick={handleClose}>{dialog.content}</div>
			</Dialog>
		</div>
	)
}

export default AccidentHistory
