import React from 'react'
import {
	Drawer,
	Button,
	Divider,
	List,
	ListItem,
	ListItemText,
	ListItemIcon,
	Collapse,
	makeStyles,
	IconButton
} from '@material-ui/core'
import classes from './Drawer.module.scss'
import Link from 'next/link'
import { ExpandLess, ExpandMore, Person, ExitToApp } from '@material-ui/icons'
import { useSelector, useDispatch } from 'react-redux'
import { useRouter } from 'next/router'
import { logout } from '../../store/actions/userActions'

const useStyles = makeStyles(theme => ({
	nested: {
		paddingLeft: theme.spacing(5)
	}
}))

const insurancesList = [
	{
		text: 'بیمه شخص ثالث',
		link: '/third-party-insurance',
		icon: '/imgs/insurances/third.svg'
	},
	{
		text: 'بیمه بدنه',
		link: '/body-insurance',
		icon: '/imgs/insurances/body.svg'
	},
	{
		text: 'بیمه مسافرتی',
		link: '/travel-insurance',
		icon: '/imgs/insurances/travel.svg'
	},
	{
		text: 'بیمه درمان تکمیلی',
		link: '/health-insurance',
		icon: '/imgs/insurances/health.svg'
	},
	{
		text: 'بیمه عمر',
		link: '/life-insurance',
		icon: '/imgs/insurances/life.svg'
	},
	{
		text: 'بیمه آتش سوزی و زلزله',
		link: '/fire-insurance',
		icon: '/imgs/insurances/fire.svg'
	},
	{
		text: 'بیمه موتور سیکلت',
		link: '/motor-third-party-insurance',
		icon: '/imgs/insurances/motor.svg'
	}
]

const SideDrawer = ({ open, toggleDrawer }) => {
	const styles = useStyles()

	const [collapse, setCollapse] = React.useState(false)

	const handleCollapse = () => {
		setCollapse(collapse => !collapse)
	}

	const dispatch = useDispatch()

	const router = useRouter()

	const onLogout = () => {
		dispatch(logout())
		router.replace('/')
	}

	const state = useSelector(state => ({
		companiesList: state.coreData.companiesList,
		user: state.user.user
	}))

	return (
		<div>
			<Drawer anchor="left" open={open} onClose={toggleDrawer}>
				<div className={classes.drawer}>
					<div className={classes.drawer__header}>
						<div>
							<Link href="/">
								<img
									src={'/logo.png'}
									alt="logo"
									onClick={toggleDrawer}
									style={{ cursor: 'pointer' }}
								/>
							</Link>
						</div>
						{!state.user ? (
							<Link href="/login">
								<Button
									color="primary"
									variant="contained"
									onClick={toggleDrawer}
								>
									ورود / عضویت
								</Button>
							</Link>
						) : (
							<div className="row align-items-center justify-content-center w-100 pr-4">
								<Person />
								<span className="ml-1 mr-2">
									{state.user.data && state.user.data.mobile}
								</span>
								<IconButton onClick={onLogout}>
									<ExitToApp />
								</IconButton>
							</div>
						)}
					</div>
					<Divider />
					<div className={classes.drawer__content}>
						<List>
							{insurancesList.map(insurance => (
								<Link
									href={insurance.link}
									key={insurance.text}
								>
									<ListItem button onClick={toggleDrawer}>
										<ListItemIcon>
											<img
												src={insurance.icon}
												alt={insurance.text}
												className={classes.drawer__logo}
											/>
										</ListItemIcon>
										<ListItemText>
											{insurance.text}
										</ListItemText>
									</ListItem>
								</Link>
							))}
						</List>
						<Divider />
						<List>
							<ListItem button onClick={handleCollapse}>
								<ListItemIcon>
									<img
										src="/imgs/insurances/partner.svg"
										alt="partner_logo"
										className={classes.drawer__logo}
									/>
								</ListItemIcon>
								<ListItemText>بیمه های همکار</ListItemText>
								{collapse ? <ExpandLess /> : <ExpandMore />}
							</ListItem>
							<Collapse in={collapse}>
								{state.companiesList.map(company => (
									<ListItem
										button
										className={styles.nested}
										key={company.id}
									>
										<ListItemText>
											{company.name}
										</ListItemText>
									</ListItem>
								))}
							</Collapse>
						</List>
					</div>
				</div>
			</Drawer>
		</div>
	)
}

export default SideDrawer
