import React from 'react'
import { makeStyles } from '@material-ui/core/styles'
import Accordion from '@material-ui/core/Accordion'
import AccordionSummary from '@material-ui/core/AccordionSummary'
import AccordionDetails from '@material-ui/core/AccordionDetails'
import Typography from '@material-ui/core/Typography'
import ExpandMoreIcon from '@material-ui/icons/ExpandMore'

const useStyles = makeStyles(theme => ({
	root: {
		width: '100%',
		'& .MuiAccordion-rounded:first-child, .MuiAccordion-rounded:last-child': {
			borderRadius: 18
		},
		'& .MuiAccordion-root:before': {
			opacity: 0
		}
	},
	heading: {
		color: 'white',
		fontSize: 14
	},
	main: {
		backgroundColor: '#b3b8bc',
		borderRadius: 18,
		marginBottom: '1rem',
		transition: 'background-color .35s',
		'&.Mui-expanded': {
			backgroundColor: '#2659c1'
		},
		boxShadow: 'none'
	},
	icon: {
		color: 'white'
	},
	text: {
		backgroundColor: 'white'
	}
}))

const Questions = ({ questions }) => {
	const classes = useStyles()

	return (
		<div className={classes.root}>
			{questions.map(question => (
				<Accordion className={classes.main} key={question.question}>
					<AccordionSummary
						expandIcon={<ExpandMoreIcon className={classes.icon} />}
					>
						<Typography className={classes.heading}>
							{question.question}
						</Typography>
					</AccordionSummary>
					<AccordionDetails className={classes.text}>
						<Typography>
							<div className="text-result">{question.answer}</div>
						</Typography>
					</AccordionDetails>
				</Accordion>
			))}
		</div>
	)
}

export default Questions
