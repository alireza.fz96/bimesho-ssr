import React from 'react'
import FormGroup from '@material-ui/core/FormGroup'
import FormControlLabel from '@material-ui/core/FormControlLabel'
import Checkbox from '@material-ui/core/Checkbox'
import StickyLayout from '../../../layout/sticky-layout/StickyLayout'
import { makeStyles } from '@material-ui/core/styles'

const useStyles = makeStyles(theme => ({
	root: {
		'& .MuiTypography-body1': {
			fontSize: 13
		}
	}
}))

const InsurancesFilter = ({ values, changed }) => {
	const styles = useStyles()

	const fields = (
		<FormGroup className={styles.root}>
			{values.map(value => (
				<FormControlLabel
					key={value.name}
					control={
						<Checkbox
							checked={value.value}
							onChange={event =>
								changed(value.name, event.target.checked)
							}
							name={value.name}
							color="primary"
						/>
					}
					label={value.label}
				/>
			))}
		</FormGroup>
	)

	return (
		<>
			<div className="d-none d-xl-block">{fields}</div>
			<StickyLayout title="پوشش های اضافی">{fields}</StickyLayout>
		</>
	)
}

export default InsurancesFilter
