import React, { useState, useEffect } from 'react'
import {
	Button,
	CircularProgress,
	FormControl,
	InputLabel,
	InputAdornment,
	OutlinedInput,
	makeStyles,
	FormHelperText,
	IconButton
} from '@material-ui/core'
import {
	discountCode as checkDiscountCode,
	removeCode
} from '../../api/userApi'
import { useSelector, useDispatch } from 'react-redux'
import { setDiscountCode as setDCode } from '../../store/actions/productActions'
import CloseIcon from '@material-ui/icons/Close'
import { CheckCircle } from '@material-ui/icons'
import { toEnglishDigit } from '../../utilities/utilities'

const useStyle = makeStyles({
	root: {
		'& .MuiFormControl-root': {
			width: '100%'
		},
		'& .MuiOutlinedInput-root ': {
			borderRadius: 100
		},
		'& .MuiButton-contained': {
			boxShadow: 'none',
			borderRadius: 100
		},
		'& .MuiOutlinedInput-adornedEnd': {
			paddingRight: 8
		},
		'& input + fieldset': {
			borderColor: 'green',
			borderWidth: 2
		}
	},
	hasCode: {
		display: 'flex',
		alignItems: 'center',
		fontSize: 13,
		color: 'green'
	}
})

const DiscountCode = ({ toggleLoading = () => {} }) => {
	const state = useSelector(state => state.product.discountCode)
	const invoice = useSelector(state => state.product.invoice)
	const dispatch = useDispatch()

	const styles = useStyle()
	const [discountCode, setDiscountcode] = useState(
		(state && state.code) || ''
	)

	const [checkDiscount, setCheckDiscount] = useState({
		error: '',
		loading: ''
	})

	useEffect(() => {
		if (state) {
			toggleLoading()
			setCheckDiscount({ loading: true, error: '' })
			checkDiscountCode(state.code, invoice && invoice.invoice_id)
				.then(res => {
					if (res.data.message !== 'Code Success') {
						setDiscountcode('')
						dispatch(setDCode(null))
					}
					toggleLoading()
					setCheckDiscount({
						loading: false,
						error: ''
					})
				})
				.catch(() => {
					setDiscountcode('')
					setDCode(null)
					toggleLoading()
					setCheckDiscount({
						loading: false,
						error: ''
					})
				})
		}
	}, [])

	const onCheckCode = () => {
		toggleLoading()
		setCheckDiscount({ loading: true, error: '' })
		checkDiscountCode(discountCode, invoice && invoice.invoice_id)
			.then(res => {
				let error
				switch (res.data.message) {
					case 'does not exist code':
						error = 'کد تخفیف وجود ندارد'
						break
					case 'Expired Date':
						error = 'تاریخ مصرف کد گذشته است'
						break
					case 'Code Success':
						dispatch(setDCode({ ...res.data, code: discountCode }))
						error = ''
						break
					case 'does not exist Invoice':
						error = 'شماره فاکتور وجود ندارد'
						break
					case 'Number of uses expired':
						error = 'کد بیش از حد مجاز استفاده شده است'
						break
					default:
						error = 'خطای ناشناخته ای پیش آمد'
				}
				toggleLoading()
				setCheckDiscount({
					loading: false,
					error
				})
			})
			.catch(error => {
				toggleLoading()
				setCheckDiscount({
					error: 'خطای ناشناخته ای پیش آمد',
					loading: false
				})
			})
	}

	const onSetDiscountCode = event => {
		const value = toEnglishDigit(event.target.value)
		setDiscountcode(value)
		if (!value) setCheckDiscount({ error: '', loading: false })
	}

	const onRemoveDiscountCode = () => {
		toggleLoading()
		setCheckDiscount({ loading: true, error: '' })
		removeCode(discountCode)
			.then(() => {
				dispatch(setDCode(null))
				setDiscountcode('')
				toggleLoading()
				setCheckDiscount({
					error: '',
					loading: false
				})
			})
			.catch(error => {
				toggleLoading()
				setCheckDiscount({
					error: 'خطای ناشناخته ای پیش آمد',
					loading: false
				})
			})
	}

	return (
		<div className={styles.root}>
			<FormControl variant="outlined" error={checkDiscount.error}>
				<InputLabel htmlFor="standard-adornment-password">
					کد تخفیف
				</InputLabel>
				<OutlinedInput
					value={discountCode}
					onChange={onSetDiscountCode}
					label="کد تخفیف"
					disabled={checkDiscount.loading}
					readOnly={!!state}
					endAdornment={
						<InputAdornment position="end">
							{discountCode &&
								(checkDiscount.loading ? (
									<CircularProgress size={25} />
								) : state ? (
									<IconButton onClick={onRemoveDiscountCode}>
										<CloseIcon />
									</IconButton>
								) : (
									<Button
										variant="contained"
										color="primary"
										onClick={onCheckCode}
									>
										اعمال
									</Button>
								))}
						</InputAdornment>
					}
				/>
				{checkDiscount.error && (
					<FormHelperText>{checkDiscount.error}</FormHelperText>
				)}
			</FormControl>
			{state && (
				<div className={styles.hasCode + ' mt-3'}>
					<CheckCircle />
					<p className="mr-1">
						مقدار تخفیف:‌ {Number(state.discount).toLocaleString()}{' '}
						تومان
					</p>
				</div>
			)}
		</div>
	)
}

export default DiscountCode
