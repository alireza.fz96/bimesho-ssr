import React from 'react'
import classes from './InsuranceDetails.module.scss'
import { MoreHoriz, Star, Timer } from '@material-ui/icons'
import {
	Button,
	CircularProgress,
	Collapse,
	IconButton
} from '@material-ui/core'

const InsuranceDetails = ({
	price,
	insurance,
	onClick,
	details,
	sticky,
	hasInstallments
}) => {
	const renderStars = () => {
		const stars = []
		for (let i = 0; i < power; i++) stars.push(i)
		return stars
	}

	const { branches, customer_rate, power, delay, title, id } = insurance

	const [showDetails, setShowDetails] = React.useState(false)

	const toggleShow = () => {
		setShowDetails(s => !s)
	}

	return (
		<>
			<div className={classes.card + ' d-none d-md-block'}>
				{sticky && <div className={classes.card__sticky}>{sticky}</div>}
				<div
					className={classes.card__content}
					onClick={() => setShowDetails(s => !s)}
				>
					{hasInstallments && (
						<div className={classes.card__badge}>
							امکان پرداخت اقساطی
						</div>
					)}
					<div className={classes.card__body}>
						<div className={classes.card__item}>
							<div className={classes.card__value}>
								<img
									src={`/imgs/insurance-companies/${id}.png`}
									alt="logo"
									className={classes.card__logo}
								/>
							</div>
						</div>
						<div className={classes.card__item}>
							<div className={classes.card__value}>
								<span className={classes.card__price}>
									{price &&
										Number(
											+price.toFixed()
										).toLocaleString()}{' '}
									تومان
								</span>
							</div>
						</div>
						<div className={classes.card__item}>
							<div className={classes.card__value}>
								<div className={classes.card__delay}>
									<CircularProgress
										variant="static"
										value={(delay / 20) * 100}
									/>
									<div className={classes.card__time}>
										<Timer fontSize="small" />
									</div>
								</div>
							</div>
						</div>
						<div className={classes.card__item}>
							<div className={classes.card__value}>
								<div className="d-flex">
									{renderStars().map(v => (
										<Star key={v} fontSize="small" />
									))}
								</div>
							</div>
						</div>
						<div className={classes.card__item}>
							<div className={classes.card__value}>
								{customer_rate}
							</div>
						</div>
						<div className={classes.card__item}>
							<div className={classes.card__value}>
								{branches}
							</div>
						</div>
						<div className={classes.card__item}>
							{details && (
								<IconButton>
									<MoreHoriz />
								</IconButton>
							)}
							<Button
								color="primary"
								variant="contained"
								onClick={() => onClick({ title, price })}
							>
								سفارش
							</Button>
						</div>
					</div>
				</div>
				{details && (
					<Collapse in={showDetails}>
						<div className={classes.card__collapse}>{details}</div>
					</Collapse>
				)}
			</div>
			<div
				className={classes.mobileCard + ' d-md-none'}
				onClick={toggleShow}
			>
				<div className={classes.card__body}>
					<div className={classes.card__item}>
						<img
							src={`/imgs/insurance-companies/${id}.png`}
							alt="logo"
							className={classes.card__logo}
						/>
					</div>
					<div className={classes.card__item}>
						<span className={classes.card__price}>
							{price && Number(+price.toFixed()).toLocaleString()}
							تومان
						</span>
					</div>
					<div className={classes.card__item}>
						<Button
							color="primary"
							variant="contained"
							onClick={() => onClick({ title, price })}
						>
							سفارش
						</Button>
					</div>
				</div>
				<Collapse in={showDetails}>
					<div className="px-3">
						{details}
						<div className={classes.mobileCard__info}>
							<div className={classes.mobileCard__item}>
								<div className={classes.mobileCard__label}>
									تعداد شعب
								</div>
								<div
									className={
										classes.mobileCard__value + ' pl-2'
									}
								>
									{branches}
								</div>
							</div>
							<div className={classes.mobileCard__item}>
								<div className={classes.mobileCard__label}>
									رضایت
								</div>
								<div
									className={
										classes.mobileCard__value + ' pl-2'
									}
								>
									{customer_rate}
								</div>
							</div>
							<div className={classes.mobileCard__item}>
								<div className={classes.mobileCard__label}>
									توان گری
								</div>
								<div
									className={
										classes.mobileCard__value + ' pl-2'
									}
								>
									<div className="d-flex">
										{renderStars()}
									</div>
								</div>
							</div>
							<div className={classes.mobileCard__item}>
								<div className={classes.mobileCard__label}>
									سرعت پاسخگویی
								</div>
								<div className={classes.mobileCard__value}>
									<div className={classes.card__delay}>
										<CircularProgress
											variant="static"
											value={(delay / 20) * 100}
										/>
										<div className={classes.card__time}>
											<Timer fontSize="small" />
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</Collapse>
				<div className={classes.mobileCard__badge}>
					<div className={!hasInstallments ? 'text-center' : ''}>
						<MoreHoriz fontSize="small" />
					</div>
					{hasInstallments && <span>امکان پرداخت اقساطی</span>}
				</div>
			</div>
		</>
	)
}

export default InsuranceDetails
