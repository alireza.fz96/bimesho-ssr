import React from 'react'
import Dialog from '@material-ui/core/Dialog'
import DialogContent from '@material-ui/core/DialogContent'
import DialogContentText from '@material-ui/core/DialogContentText'
import Slide from '@material-ui/core/Slide'
import useMediaQuery from '@material-ui/core/useMediaQuery'
import { useTheme } from '@material-ui/core/styles'
import { Close } from '@material-ui/icons'
import AppBar from '@material-ui/core/AppBar'
import Toolbar from '@material-ui/core/Toolbar'
import Typography from '@material-ui/core/Typography'
import IconButton from '@material-ui/core/IconButton'

const Transition = React.forwardRef(function Transition(props, ref) {
	return <Slide direction="up" ref={ref} {...props} />
})

export default function ErrorDialog({ close, error, open }) {
	const theme = useTheme()

	const fullScreen = useMediaQuery(theme.breakpoints.down('sm'))
	return (
		<Dialog
			open={open}
			TransitionComponent={Transition}
			keepMounted
			onClose={close}
			fullScreen={fullScreen}
		>
			<AppBar position="static">
				<Toolbar>
					<IconButton
						edge="start"
						color="inherit"
						aria-label="menu"
						onClick={close}
					>
						<Close />
					</IconButton>
					<Typography variant="h6">خطا</Typography>
				</Toolbar>
			</AppBar>
			<DialogContent>
				<DialogContentText
					id="alert-dialog-slide-description"
					className="p-3 pl-sm-5 pr-sm-5 pt-2 pb-2"
					component="div"
				>
					{error}
				</DialogContentText>
			</DialogContent>
		</Dialog>
	)
}
