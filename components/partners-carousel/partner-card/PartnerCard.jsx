import React from 'react'
import classes from './PartnerCard.module.scss'

const PartnerCard = ({ partner }) => {
	return (
		<div className={classes.partner}>
			<img
				src={`/imgs/insurance-companies/${partner.id}.png`}
				alt={partner.name + '_logo'}
				className={classes.partner__image}
			/>
			<h3 className={classes.partner__name}>{partner.name}</h3>
		</div>
	)
}

export default PartnerCard
