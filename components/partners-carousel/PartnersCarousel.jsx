import React from 'react'
import Slider from 'react-slick'
import classes from './PartnersCarousel.module.scss'
import { NavigateNext, NavigateBefore } from '@material-ui/icons'
import PartnerCard from './partner-card/PartnerCard'
import { useSelector } from 'react-redux'

const PartnersCarousel = () => {
	var settings = {
		dots: false,
		infinite: true,
		speed: 400,
		slidesToScroll: 1,
		slidesToShow: 4,
		arrows: false,
		autoplay: true,
		autoplaySpeed: 4000,
		responsive: [
			{
				breakpoint: 992,
				settings: {
					slidesToShow: 2
				}
			},
			{
				breakpoint: 576,
				settings: {
					slidesToShow: 1
				}
			}
		]
	}

	const sliderRef = React.useRef()

	const next = () => {
		sliderRef.current.slickNext()
	}

	const previous = () => {
		sliderRef.current.slickPrev()
	}

	const companiesList = useSelector(state => state.coreData.companiesList)
	return (
		<div className={classes.partners}>
			<Slider {...settings} ref={sliderRef}>
				{companiesList.map(company => (
					<div
						key={company.id}
						className="py-4"
						style={{ paddingRight: 0, paddingLeft: 0 }}
					>
						<PartnerCard partner={company} />
					</div>
				))}
			</Slider>
			<div className={classes.partners__controls}>
				<div className={classes.partners__controlItem} onClick={next}>
					<NavigateNext />
				</div>
				<div
					className={classes.partners__controlItem}
					onClick={previous}
				>
					<NavigateBefore />
				</div>
			</div>
		</div>
	)
}

export default PartnersCarousel
